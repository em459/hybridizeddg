\documentclass[12pt]{article}
\usepackage[cm]{fullpage}
\usepackage{amsmath,amssymb}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problem specification}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Function space and dof-vector}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let $V$ be an $n$-dimensional function space with basis $\{\psi_j\}_{j=1}^n$ i.e. for every function $u\in V$ we have that
\begin{equation}
  u(x) = \sum_{j=1}^n U_j \psi_j(x)
\label{eqn:fine_expansion}
\end{equation}
We call the vector $\vec{U}\in\mathbb{R}^n$ defined in Eq. (\ref{eqn:fine_expansion}) the \textit{dof-vector} of the function $u\in V$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Weak form}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Consider the weak form of a PDE, which we express in terms of a bilinear form $a(\cdot,\cdot):V\times V\rightarrow \mathbb{R}$ and a linear 1-form $b(\cdot):V\rightarrow \mathbb{R}$ as
\begin{equation}
  a(v,u) = b(v) \qquad\text{for all $v\in V$}\label{eqn:weak_form}.
\end{equation}
The goal is to find the function $u\in V$ which solves Eq. (\ref{eqn:weak_form}).

An example is the weak form of the Poisson equation $-\Delta u=f$ in $\Omega$, for which we have\footnote{Of course, we also need to specify boundary conditions, but they are not relevant for the argument here.}
\begin{xalignat}{2}
a(v,u) &= \int_\Omega \nabla u(x)\cdot \nabla v(x)\;dx, &
b(v) &= \int_\Omega f(x)v(x)\;dx.
\end{xalignat}
For the following we only assume that we have an equation of the form in Eq. (\ref{eqn:weak_form}) and do not specify the construction of $a(\cdot,\cdot)$ and $b(\cdot)$ further. The only thing that matters is that those forms are (bi-) linear.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dual functions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Note that $b$ is an element of the dual space $V'$ and that we can define a vector $\vec{B}\in\mathbb{R}^n$ as
\begin{equation}
  B_i := b(\psi_j)\qquad\text{for all $j=1,\dots,n$}.
\end{equation}
This is \textit{not} the same as the dof-vector $\vec{\Phi}_b$ of the Riesz-representer $\phi_b\in V$ of $b\in V'$, which (assuming we have a scalar product $\langle\cdot,\cdot\rangle$ on $V$) is defined as
\begin{equation}
b(v) = \langle \phi_b,v\rangle\qquad\text{for all $v\in V$}\label{eqn:Riesz}.
\end{equation}
More specifically, given the expansion 
\begin{equation}
\phi_b(x)=\sum_{j=1}^n (\Phi_b)_j\psi_j(x)
\end{equation}
of the Riesz representer $\phi_b$, we have $\vec{B}=M\vec{\Phi}_b$ with the (symmetric) $n\times n$ mass-matrix $M$ defined by $M_{ij}=\langle\psi_i,\psi_j \rangle$; this can be seen by setting $v = \psi_i$ in Eq. (\ref{eqn:Riesz}).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Intergrid operators}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let $V^{(c)}$ be the $n^{(c)}$-dimensional coarse level function space with basis $\{\psi^{(c)}\}_{j=1}^{n^{(c)}}$, i.e. every function $u^{(c)}\in V^{(c)}$ can be written as
\begin{equation}
u^{(c)}(x) = \sum_{j=1}^{n^{(c)}} U^{(c)}_j \psi^{(c)}_j(x)
\label{eqn:coarse_expansion}
\end{equation}
Although this is often the case, here we do not assume that $V^{(c)}\subset V$. In fact, for the hybridized space $V=\Lambda_h$ of all trace functions defined on the skeleton of the mesh, this is not the case, since the coarse space $V^{(c)}=P_{1,h}$ consists of all piecewise linear functions on the entire mesh.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Prolongation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Assume further that we have a prolongation between the function spaces $\pi: V^{(c)}\rightarrow V$ such that $\pi u^{(c)} = u$ for all $u^{(c)}\in V^{(c)}$ (we write $\pi u^{(c)}$ instead of $\pi(u^{(c)})$ to denote that $\pi$ is a linear map). Given the expansions of $u$ and $u^{(c)}$ in Eqs. (\ref{eqn:fine_expansion}) and (\ref{eqn:coarse_expansion}), the map $\pi$ induces the linear transformation $P$ (represented by an $n\times n^{(c)}$ matrix) between the corresponding dof-vectors
\begin{xalignat}{2}
\pi u^{(c)} &= u & \Leftrightarrow \qquad P\vec{U}^{(c)} = \vec{U}
\label{eqn:prolongation}
\end{xalignat}
Using the expansion of $u^{(c)}$ and $u$ it is easy to work out the entries of $P$ explicitly as
\begin{equation}
P = M^{-1} C \qquad \text{with $M_{ij}=\langle\psi_i,\psi_j\rangle$ and $C_{ij}=\langle \psi_i,\pi\psi^{(c)}_j\rangle$}.
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Restriction}\label{sec:restriction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We now seek the coarse-level equivalent of Eq. (\ref{eqn:weak_form}), i.e. we look for a 2-form $a^{(c)}(\cdot,\cdot):V^{(c)}\times V^{(c)}\rightarrow \mathbb{R}$ and a 1-form $b^{(c)}(\cdot):V^{(c)}\rightarrow\mathbb{R}$ such that
\begin{equation}
  a^{(c)}(u^{(c)},v^{(c)}) = b^{(c)}(v^{(c)})\qquad\text{for all $v^{(c)}\in V^{(c)}$} \label{eqn:coarse_equation}
\end{equation}
Note that $b^{(c)}\in {V^{(c)}}'$ is an element of the dual space ${V^{(c)}}'$. Given the prolongation $\pi:V^{(c)}\rightarrow V$, one way of defining those forms is via
\begin{xalignat}{2}
a^{(c)}(v^{(c)},u^{(c)}) &:= a(\pi v^{(c)},\pi u^{(c)}), &
b^{(c)}(v^{(c)}) &:= b(\pi v^{(c)})\qquad\text{for all $u^{(c)},v^{(c)}\in V^{(c)}$}
\end{xalignat}
Looking first at the linear form $b^{(c)}$, this defines the \textit{restriction} $\rho:V'\rightarrow {V^{(c)}}'$ as a map between the dual spaces with $b^{(c)} = \rho b = b\circ \pi$ (as for the prolongation, we write $\rho u$ instead of $\rho(u)$ since $\rho$ is a linear map).

Now let $\vec{B}^{(c)}$ be defined as $B^{(c)}_j=b^{(c)}(\psi_j^{(c)})$. The restriction $\rho$ (as a map between dual function spaces) induces a linear transformation between the vectors $\vec{B}$ and $\vec{B}^{(c)}$ as
\begin{xalignat}{2}
b^{(c)} &= \rho b & \Leftrightarrow \qquad \vec{B}^{(c)}&=R\vec{B}.
\end{xalignat}
To work out the elements of the $n^{(c)}\times n$ restriction matrix $R$, consider $B_i^{(c)} = b^{(c)}(\psi_i^{(c)})$ and note that $\pi\psi^{(c)}_i = \sum_{j=1}^{n} P_{ji}\psi_j$, which is a special case of Eq. (\ref{eqn:prolongation}) with $u^{(c)} = \psi_i^{(c)}$ $\Leftrightarrow$ $U^{(c)}_k = \delta_{ik}$. The linearity of $b$ gives
\begin{equation}
B_i^{(c)} = b^{(c)}(\psi_i^{(c)}) = b(\pi\psi_i^{(c)}) = b(\sum_{j=1}^n P_{ji}\psi_j) = \sum_{j=1}^n P_{ji} b(\psi_j) = \sum_{j=1}^n P_{ji}B_j
\end{equation}
and hence $R=P^T=C^TM^{-1}$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Restriction of primal functions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We stress again that the restriction operator $\rho:V'\rightarrow {V^{(c)}}'$ maps between dual function spaces. We can use it to construct a restriction operator $\rho^*:V\rightarrow V^{(c)}$ between the function spaces themselves as follows:

Assume that we have a function $w(x)=\sum_{j=1}^n W_j\psi_j(x)$ and want to know what the corresponding restricted function $\rho^* w(x) = w^{(c)}(x)= \sum_{j=1}^{n^{(c)}}W^{(c)}_j\psi^{(c)}_j(x)$ is. For this, note that there exist linear forms $w^*\in V'$ and $w^{(c)*}\in {V^{(c)}}'$ such that $w=\phi_{w^*}$ is the Riesz representer of $w^*$ and $w^{(c)}=\psi_{w^{(c)*}}$ is the Riesz representer of $w^{(c)*}$. Further, we then know how to restrict the vector $\vec{W}^*$ defined by $w^*(\phi_j)=W^*_j$ to obtain the vector $\vec{W}^{(c)*}$ defined by $w^{(c)*}(\psi^{(c)}_j) = W^{(c)*}_j$, namely:
\begin{equation}
\vec{W}^{(c)*} = P^T \vec{W}^*.
\end{equation}
Since $\vec{W}^{*}=M\vec{W}$ and $\vec{W}^{(c)*}=M^{(c)}\vec{W}^{(c)}$ this implies that
\begin{equation}
\vec{W}^{(c)} = R^* \vec{W}\qquad\text{with $R^*=\left(M^{(c)}\right)^{-1}P^T M$}
\end{equation}
where $M$ and $M^{(c)}$ are the mass matrices. The transformation of the dof-vectors representing $w$ and $w^{(c)}$ defines the mapping $\rho^*$ between the functions themselves.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Matrix equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Defining the $n\times n$ system matrix $A$ as
\begin{equation}
  A_{ij} = a(\psi_i,\psi_j)
\end{equation}
allows writing down a matrix equation for the dof-vector $\vec{U}$.
Setting $v=\psi_i$ in Eq. (\ref{eqn:weak_form}) we find using linearity
\begin{equation}
\begin{aligned}
B_i = b(\psi_i) &= a(\psi_i,u) = a(\psi_i,\sum_{j=1}^n U_j \psi_j) = \sum_{j=1}^n U_j a(\psi_i,\psi_j) = \sum_{j=1}^n A_{ij}U_j\\
\Leftrightarrow\qquad A\vec{U} &= \vec{B}
\end{aligned}
\end{equation}
The corresponding coarse level equation can be written down by setting $v^{(c)}=\psi_i^{(c)}$ in Eq. (\ref{eqn:coarse_equation})
\begin{equation}
\vec{B}^{(c)} = A^{(c)}\vec{U}^{(c)}
\end{equation}
The coarse level matrix is given by the Galerkin product, using the bilinearity of $a(\cdot,\cdot)$
\begin{equation}
\begin{aligned}
A^{(c)}_{ij} &= a^{(c)}(\psi^{(c)}_i,\psi^{(c)}_j) = a(\pi\psi^{(c)}_i,\pi\psi^{(c)}_j) = a(\sum_{k=1}^n P_{ki}\psi_k,\sum_{\ell=1}^n P_{\ell j}\psi_\ell)\\
& \qquad =\;\; \sum_{k=1}^n\sum_{\ell=1}^n  P_{ki}P_{\ell j} a(\psi_k,\psi_\ell) = \sum_{k=1}^n \sum_{\ell=1}^n P_{ki}P_{\ell j} A_{k\ell}\\
\Leftrightarrow\qquad A^{(c)} &= P^T A P = R A P
\end{aligned}
\end{equation}
\end{document}