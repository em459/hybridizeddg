DG shallow water solver
=======================

Summary
-------

This repository contains code for solving the linearised shallow water equations.

Repository Layout
-----------------

All source code is contained in the directory ```src```. 

The subdirectory ```swehdg``` contains a Firedrake implementation of a spatial DG discretisation on an icosahedral sphere grid with semi-implicit IMEX timestepping. Both a native-DG discretisation and a hybridized DG implementation based on [Bui-Thanh, 2016](https://doi.org/10.1137/16M1057243)  (see also [Bui-Thanh, 2015](https://doi.org/10.1016/j.jcp.2015.04.009)) are implemented. The hybridized version uses the SLATE language as described in [Gibson et al.](https://arxiv.org/abs/1802.00303).

Inside the ```swehdg``` there are several subdirectories:
* ```core``` contains the central classes, such as the DG and HDG discretisations of the shallow water equations and the timesteppers
* ```auxilliary``` contains some helper code, which can be used for example for constructing suitable test cases

---

**To use the Firedrake swehdg code**
*DO NOT* manually set your Python path environment variable.
`swehdg` is now installable as a package.
If you wish to continue editing the package, install using
```
cd ${ROOT}
pip install -e .
```
where `${ROOT}` is the directory into which the git repository has been cloned.
If you just want to run the code, without editing the internals, just omit the `-e` flag.

Once this is set up, it is possible to import the code as follows:
```
from swehdg.core.shallow_water import *
from swehdg.auxilliary.solution_looper import *
```

---

The main scripts for running the code are in the ```src/driver``` directory. Those are used to obtain results and carry out performance measurements

All tests are contained in the subdirectory ```src/tests```, they can be run with ```py.test```.

The directory ```src/analysis``` contains some code for theoretical analysis. 



For more details on the maths and to generate Sphinx-documentation, run ```make html``` in the ```src``` directory to generate html documentation in the subdirectory ```build/html```.

The subdirectory ```doc``` contains some documentation of the mathematics.
