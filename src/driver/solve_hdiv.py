from firedrake import *
import numpy as np
from swehdg.core.periodic_square import PeriodicUnitSquareMeshHierarchy

'''Example code for solving the problem

  - gamma*grad(div(u)) + u = f

for u in H(div). This is the problem which has to be solved on the coarse
level if the Lax-Friedrichs flux is used.
The weak formulation of the problem with a RT(1) discretisation is solved with
a two-level geometric multigrid method with PCpatch smoothers and an exact
solver on the coarse level.
'''

# Number of cells in each direction for fine mesh
n = 16
# Number of coarsenings (nref=1 for the two-level method) 
nref = 1
# Strength of second order term
gamma = 1.0

pmh = PeriodicUnitSquareMeshHierarchy(n,nref)
mesh = pmh.mesh() 

# Construct RT(1) function space
V = FunctionSpace(mesh, 'RT', 1)

# Build right hand side
X = SpatialCoordinate(mesh)
f = Function(V).project(as_vector((sin(2.*np.pi*X[0]),sin(2.*np.pi*X[1]))))

# Test- and trial-functions in RT(1) space 
u_test = TestFunction(V)
u_trial = TrialFunction(V)

# Construct two-form and right hand side
a = (inner(u_test,u_trial) + Constant(gamma)*div(u_test)*div(u_trial))*dx
L = inner(u_test,f)*dx

u = Function(V)
lvp = LinearVariationalProblem(a,L,u)

# Use PCpatch smoother?
use_pc_patch = True

# Relative tolerance on residual
rtol = 1.0E-12
# Smoothing factor
richardson_scaling = 0.5

if (use_pc_patch):
    param = {'ksp_type':'cg',
             'mat_type':'aij',
             'pc_type': 'mg',
             'pc_mg_type': 'full',
             'pc_mg_cycles': 'v',
             'mg_levels_ksp_type': 'richardson',
             'mg_levels_ksp_norm_type': 'unpreconditioned',
             'mg_levels_ksp_richardson_scale': richardson_scaling,
             'mg_levels_ksp_max_it': 1,
             'mg_levels_ksp_convergence_test': 'skip',
             'mg_levels_pc_type': 'python',
             'mg_levels_pc_python_type': 'firedrake.PatchPC',
             'mg_levels_patch_pc_patch_save_operators': True,
             'mg_levels_patch_pc_patch_partition_of_unity': False,
             'mg_levels_patch_pc_patch_construct_type': 'star',
             'mg_levels_patch_pc_patch_construct_dim': 0,
             'mg_levels_patch_pc_patch_sub_mat_type': 'seqdense',
             'mg_levels_patch_pc_patch_precompute_element_tensors': True,
             'mg_levels_patch_sub_ksp_type': 'preonly',
             'mg_levels_patch_sub_pc_type': 'lu',
             'mg_coarse_pc_type': 'python',
             'mg_coarse_pc_python_type': 'firedrake.AssembledPC',
             'mg_coarse_assembled_pc_type': 'lu',
             'mg_coarse_assembled_pc_factor_mat_solver_type': 'mumps',
             'ksp_monitor_true_residual':None,
             'ksp_rtol':1.E-12}
else:
    param = {'ksp_type':'cg',
             'pc_type':'gamg',
             'ksp_monitor_true_residual':True,
             'ksp_rtol':1.E-12}
    
lvs = LinearVariationalSolver(lvp,solver_parameters=param)

# Set intergrid operators for multigrid
prolongation = EmbeddedDGTransfer(V.ufl_element())
lvs.set_transfer_operators(dmhooks.transfer_operators(V, prolong=prolongation.prolong, inject=prolongation.inject, restrict=prolongation.restrict))

# Solve problem
lvs.solve()
