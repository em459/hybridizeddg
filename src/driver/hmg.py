from firedrake import *
from matplotlib import pyplot as plt


degree = 1

mesh = UnitSquareMesh(64, 64, quadrilateral=False)
RT = FunctionSpace(mesh, "RT", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = RT * DG
sigma, u = TrialFunctions(W)
tau, v = TestFunctions(W)

# Define the source function
f = Function(DG)
x, y = SpatialCoordinate(mesh)
f.interpolate((1+8*pi*pi)*sin(x*pi*2)*sin(y*pi*2))


def p1_callback():
    P1 = FunctionSpace(mesh, "CG", 1)
    q = TrialFunction(P1)
    r = TestFunction(P1)
    return inner(q, r)*dx + inner(grad(q), grad(r))*dx


def get_p1_space():
    return FunctionSpace(mesh, "CG", 1)



# Define the variational forms
a = (dot(sigma, tau) - div(tau) * u + u * v + v * div(sigma)) * dx
L = f * v * dx

w = Function(W)
params = {'mat_type': 'matfree',
          'ksp_type': 'preonly',
          'pc_type': 'python',
          'pc_python_type': 'firedrake.HybridizationPC',
          'hybridization': {'ksp_type': 'cg',
                            # 'ksp_view': None,
                            'ksp_monitor_true_residual': None,
                            'mat_type': 'matfree',
                            'pc_type': 'python',
                            'pc_python_type': 'firedrake.GTMGPC',
                            'gt': {'mat_type': 'aij',
                                   'mg_levels': {'ksp_type': 'chebyshev',
                                                 'pc_type': 'sor',
                                                 'ksp_max_it': 2},
                                   'mg_coarse': {"ksp_type": "preonly",
                                                 "pc_type": "mg",
                                                 "pc_mg_cycles": 'v',
                                                 "mg_levels": {"ksp_type": "chebyshev",
                                                               "ksp_max_it": 2,
                                                               "pc_type": "sor"}}}}}

appctx = {"get_coarse_operator": p1_callback,
          "get_coarse_space": get_p1_space}

lvp = LinearVariationalProblem(a, L, w)
linear_solver = LinearVariationalSolver(lvp, solver_parameters=params,
                                        appctx=appctx)
linear_solver.solve()

sigma_h, u_h = w.split()

plot(u_h)
plt.show()
