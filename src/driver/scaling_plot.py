import argparse
import os

import matplotlib.pyplot as plt
import pickle as pkl
import swehdg.auxilliary.plotting as stdplot
import swehdg.auxilliary.results_data as resdat

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--directory',
                        default='.',
                        type=str,
                        help='directory to save or retrieve results from')
    args, unknown = parser.parse_known_args()

    directory = args.directory

    # Read in data
    raw_data, kind = resdat.read_scaling_results_data(directory)
    # Add time per timestep field
    raw_data['tperts'] = raw_data['runtime']/raw_data['num_ts']
    # Get all methods
    all_methods = resdat.get_methods(raw_data)
    # Re-arrange data
    data = raw_data.groupby(['method', 'degree', 'procs']).mean()
    # Get all required degrees
    select_degree = list(set(data.index.get_level_values('degree')))
    
    # Plot figure
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches((4, 4))
    DPI = 300
    
    if kind=='WEAK':
        stdplot.weak_scaling_plot(ax, data,
                            select_method=all_methods,
                            select_degree=select_degree,
                            legends=True)
    else:
        stdplot.strong_scaling_plot(ax, data,
                            select_method=all_methods,
                            select_degree=select_degree,
                            legends=True)

    plt_name = os.path.join(directory, kind + '.png')
    fig.savefig(plt_name, bbox_inches='tight', dpi=DPI)

    with open(os.path.join(directory, 'scaling_plot.pickle'), 'wb') as fh:
        pkl.dump(fig, fh)
