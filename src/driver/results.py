import sys
import csv
import argparse

import numpy as np

from swehdg.auxilliary.solution_looper import *
from swehdg.auxilliary.balanced_vortex import *
from swehdg.core.timestepper import *

parameters["pyop2_options"]["lazy_evaluation"] = False

def generate_list(args):
    '''
    Generates a list of parameters based on command line options
    '''
    refinement = list(set(args.refinement))
    if args.degree is not None:
        degree = list(set(args.degree))
    else:
        degree = [0,1,2]
    non_linear = args.nonlinear
    bathymetry = args.bathymetry
    gtmg = args.gtmg

    # Set fixed parameters to be the "defaults"
    fixed_param = default_param()
    # mesh = gen_mesh(fixed_param) Can't generate meshes until the end now
    # fixed_param['mesh'] = mesh
    fixed_param['timestep_parameter'] = args.ts_param
    if args.timestepper == 'Explicit':
        fixed_param['timestepper'] = TimeStepperTheta
        fixed_param['timestep_parameter'] = 0.0
        fixed_param['alpha'] = 0.5 # This had to be changed from 1 to get stability for high order
    elif 'RKDG' in args.timestepper:
        fixed_param['timestepper'] = globals()['TimeStepper' + args.timestepper]
        fixed_param['alpha'] = 1 # 1/10 size of implicit step
    else:
        fixed_param['timestepper'] = globals()['TimeStepper' + args.timestepper]
    fixed_param['total_time'] = 0.5
    fixed_param['flux'] = args.flux
    fixed_param['hybridised'] = False
    fixed_param['linear'] = not non_linear
    fixed_param['gtmg'] = gtmg
    fixed_param['mg_type'] = args.mg_type
    fixed_param['save_output'] = args.save_output
    fixed_param['solver_rtol'] = args.solver_rtol

    base_list = [fixed_param]

    base_list = vary_param(base_list, 'refinement', refinement)
    base_list = vary_param(base_list, 'degree', degree)
    if (args.timestepper != 'Explicit') and ('RKDG' not in args.timestepper):
        if args.vary_mg:
            hybridised_list = vary_param(base_list, 'hybridised', True)
            hybridised_list = vary_param(hybridised_list, 'gtmg', [False, True])
            if args.hybridised_only:
                base_list = hybridised_list
            else:
                base_list += hybridised_list
        elif args.hybridised_only:
            base_list = vary_param(base_list, 'hybridised', True)
        else:
            base_list = vary_param(base_list, 'hybridised', [False, True])

    ## Initial conditions and mesh for all configurations:
    # Vortex setup
    SW_param = ShallowWaterDGParam()
    f_over_cg = 2.0*SW_param.omega/SW_param.cg
    dphi = 0.1

    vortex = BalancedVortex(dphi, f_over_cg, non_linear=non_linear, bathymetry=bathymetry)
    for configuration in base_list:
        mesh = gen_mesh(configuration)
        configuration['mesh'] = mesh
        configuration['initial_condition_phi'] = vortex.phis_ufl(mesh)
        if bathymetry:
            configuration['bathymetry'] = vortex.bath_ufl(mesh)
        configuration['initial_condition_u'] = vortex.u_ufl(mesh)

    return base_list

def warmup(args):
    '''
    Perform two timesteps for each paremeter to ensure the kernels are build
    and this compilation time isn't included in the runtime
    '''
    fprint('WARMUP')
    # Generate list of parameters
    base_list = generate_list(args)
    # Loop over list
    for count, item in enumerate(base_list):
        fprint(count, end=': ')
        try:
            rt = simulation_warmup(item)
            fprint('DONE in', rt, 'seconds')
        except Exception as err:
            fprint('ERROR')
            store_error = err
            rt = -999
            import pdb; pdb.set_trace()

    return 0

def run(args):
    '''
    Run through the all simulations listed at command line
    '''
    # Generate list of parameters
    base_list = generate_list(args)

    # Extract args
    directory = args.directory
    directory += '/'

    # Make directory for results (only on rank 0)
    if (COMM_WORLD.rank == 0):
        try:
            os.makedirs(directory)
        except Exception as e:
            print('Directory already exists, if you want to overwrite the tests try rerun')
            raise

    # IMPORTANT
    out = sys.stdout

    # Create CSV to hold results (only on rank 0)
    if (COMM_WORLD.rank == 0):
        fh = open(directory + 'results.csv', 'w', newline='')
        fieldnames = [  'refinement', 'degree', 'hybridised',
                        'ndof_cell', 'ndof_facet', 'runtime',
                        'avg_iter', 'timestepper', 'num_ts',
                        'linear', 'bathymetry', 'multigrid',
                        'phi_error', 'u_error']
        writer = csv.DictWriter(fh, fieldnames=fieldnames)
        writer.writeheader()

    # Loop over list
    for count, item in enumerate(base_list):
        filename = 'simulation' + str(count)

        fprint(count, end=': ')
        if (COMM_WORLD.rank == 0):
            meta_data(directory + filename, item)

        # Run simulation
        try:
            q, rt, sw, tsname = simulation(directory + filename, item)
            fprint('DONE')
        except Exception as err:
            sys.stdout = out
            fprint('ERROR')
            store_error = err
            rt = -999
            if (COMM_WORLD.rank == 0):
                with open(directory + filename + '.output', 'a') as errorfh:
                    errorfh.write(str(err.args))
            # Next iteration (don't write to CSV)
            continue

        # Save details of result
        output_dict = {}
        output_dict['refinement'] = sw._nrefine
        output_dict['degree'] = sw._degree
        output_dict['hybridised'] = item['hybridised']
        output_dict['ndof_cell'] = sw.ncell * sw.ndof_per_cell
        if item['hybridised']:
            output_dict['ndof_facet'] = sw.nfacet * sw.ndof_per_facet
        else:
            output_dict['ndof_facet'] = 0
        output_dict['runtime'] = rt
        output_dict['timestepper'] = tsname
        if (args.timestepper != 'Explicit')  and ('RKDG' not in args.timestepper):
            for monitor in sw._ksp_monitors:
                if 'DG' in monitor.label:
                    break
            output_dict['avg_iter'] = np.average(monitor.niter)
            output_dict['num_ts'] = len(monitor.niter)
        else:
            output_dict['avg_iter'] = 0.0
            dt = item['alpha']*item['rho']*sw.h/(2.0*item['degree']+1)/sw.cg
            output_dict['num_ts'] = np.int(np.ceil(item['total_time']/dt))
        output_dict['linear'] = item['linear']
        output_dict['bathymetry'] = True if item['bathymetry'] is not None else False
        if item['hybridised']:
            if item['gtmg'] == True:
                output_dict['multigrid'] = 'GTMG+' + item['mg_type']
            else:
                output_dict['multigrid'] = 'AMG'
        else:
            output_dict['multigrid'] = 'AMG'
        # Calculate error
        try:
            phi_ic = item['initial_condition_phi']
            u_ic = item['initial_condition_u']
            u_final, phi_final = q.split()[0:2]
            phi_error = np.sqrt(assemble((phi_final - phi_ic)*(phi_final - phi_ic)*dx))
            u_error = np.sqrt(assemble(inner(u_final - u_ic, u_final - u_ic)*dx))
        except:
            phi_error = np.nan
            u_error = np.nan

        output_dict['phi_error'] = phi_error
        output_dict['u_error'] = u_error

        if (COMM_WORLD.rank == 0):
            writer.writerow(output_dict)
            fh.flush()

    if (COMM_WORLD.rank == 0):
        fh.close()

    return sw

def rerun(args):
    '''
    Currently not implemented
    '''
    # TODO: Implement this
    directory = args.directory

    if args.refinement is not None:
        refinement = list(set(args.refinement))
        print(refinement)
    if args.degree is not None:
        degree = list(set(args.degree))
        print(degree)

    if os.path.isdir(directory):
        print('Directory', directory, 'exists')
    else:
        print('Directory', directory, 'does not exist')
    pass

def plot(args):
    '''
    Plot the results found in a given directory, see code for specific plots
    '''
    directory = args.directory
    legends = not args.no_legend

    # Imports
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    from pandas import Series, DataFrame, read_csv
    import pickle as pkl
    import swehdg.auxilliary.plotting as stdplot
    import swehdg.auxilliary.results_data as resdat

    # Read in data
    raw_data = resdat.read_results_data(directory)
    # Add time per degree of freedom field
    raw_data['tperdof'] = raw_data['runtime']/(raw_data['ndof']*raw_data['num_ts'])
    # Get all methods
    timestep_methods = resdat.get_methods(raw_data)
    # Re-arrange data
    data = raw_data.groupby(['method', 'refinement', 'degree']).mean()
    ndof_data = raw_data.groupby(['method', 'ndof_cell', 'degree']).mean()

    # Get all required degrees
    if args.degree is not None:
        select_degree = list(set(args.degree))
    else:
        select_degree = list(set(data.index.get_level_values('degree')))

    # Global figure size
    fig_size = (4, 4)

    ######
    # PLOT 1 runtime vs. refinement
    ######
    fig1, ax1 = plt.subplots(1, 1)
    fig1.set_size_inches(fig_size)

    ax1 = stdplot.plot1(ax1, data, select_method=timestep_methods, select_degree=select_degree, legends=legends)

    ref_plot = os.path.join(directory, 'runtime_refine.pdf')
    fig1.savefig(ref_plot, bbox_inches='tight')

    ######
    # PLOT 2 runtime vs. ndofs
    ######
    fig2, ax2 = plt.subplots(1, 1)
    fig2.set_size_inches(fig_size)

    ax2 = stdplot.plot2(ax2, ndof_data, select_method=timestep_methods, select_degree=select_degree, legends=legends)

    ndof_plot = os.path.join(directory, 'runtime_ndof.pdf')
    fig2.savefig(ndof_plot, bbox_inches='tight')

    ######
    # PLOT 3 single timestep runtime per dof vs. ndof
    ######
    fig3, ax3 = plt.subplots(1, 1)
    fig3.set_size_inches(fig_size)

    ax3 = stdplot.plot3(ax3, ndof_data, select_method=timestep_methods, select_degree=select_degree, legends=legends)

    ndof_plot = os.path.join(directory, 'runtime-ndof-ntime_ndof.pdf')
    fig3.savefig(ndof_plot, bbox_inches='tight')

    # REMOVE EXPLICIT HERE
    try:
        timestep_methods.remove('Explicit')
    except:
        pass

    ######
    # PLOT 4 iteration vs. ndofs
    ######
    fig4, ax4 = plt.subplots(1, 1)
    fig4.set_size_inches(fig_size)

    ax4 = stdplot.plot4(ax4, ndof_data, select_method=timestep_methods, select_degree=select_degree, legends=legends)

    ndof_plot = os.path.join(directory, 'iter_ndof.pdf')
    fig4.savefig(ndof_plot, bbox_inches='tight')

    ######
    # PLOT 5 speedup vs. refinement
    ######
    try:
        fig5, ax5 = plt.subplots(1, 1)
        fig5.set_size_inches(fig_size)

        ax5 = stdplot.plot5(ax5, data, select_method=timestep_methods, select_degree=select_degree, legends=legends)

        ndof_plot = os.path.join(directory, 'speedup_ndof.pdf')
        fig5.savefig(ndof_plot, bbox_inches='tight')
    except:
        print('ERROR: Speedup plot failed (this may be expected)')

    with open(os.path.join(directory, 'plots.pickle'), 'wb') as fh:
        pkl.dump([fig1, fig2, fig3, fig4, fig5], fh)


if __name__ == '__main__':
    # Parse command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('verb',
                        choices=['warmup', 'run','rerun', 'plot'],
                        help='warmup, run, rerun or plot results')
    parser.add_argument('--directory',
                        type=str,
                        help='directory to save or retrieve results from')
    parser.add_argument('--refinement',
                        type=int,
                        nargs='+',
                        default=[3,4],
                        help='refinements to run')
    parser.add_argument('--degree',
                        type=int,
                        nargs='+',
                        default=None, # Has different defaults for plots and runs
                        help='degrees to run')
    parser.add_argument('--flux',
                        choices=['upwind', 'Lax-Friedrichs'],
                        default='Lax-Friedrichs',
                        help='choice of flux')
    parser.add_argument('--timestepper',
                        choices=[   'Explicit',
                                    'Theta',
                                    'IMEXTheta',
                                    'IMEXThreeStage',
                                    'IMEXARS2_2_3_2',
                                    'IMEXARS3_4_4_3',
                                    'RKDG2',
                                    'RKDG3',
                                    'IMEXRKDG2',
                                    'IMEXRKDG3'],
                        default='IMEXTheta',
                        help='choose timestepping routine (theta=0.5 by default)')
    parser.add_argument('--ts_param',
                        type=float,
                        default=0.5,
                        help='timestepping parameter (theta)')
    parser.add_argument('--hybridised_only',
                        action='store_true',
                        help='do not run any DG, only HDG')
    parser.add_argument('--nonlinear',
                        action='store_true',
                        help='solve non-linear problem')
    parser.add_argument('--bathymetry',
                        action='store_true',
                        help='solve problem with bathymetry')
    mg_group = parser.add_mutually_exclusive_group()
    mg_group.add_argument('--gtmg',
                        action='store_true',
                        default=False,
                        help='Use the non-nested multigrid')
    mg_group.add_argument('--vary_mg',
                        action="store_true",
                        help='Runs tests with and without --gtmg')
    parser.add_argument('--mg_type',
                        choices=['AMG', 'GMG'],
                        default='AMG',
                        help='Choice of coarse space multigrid')
    parser.add_argument('--save_output',
                        action='store_true',
                        help='Store vtk for timesteps')
    parser.add_argument('--no_legend',
                        action='store_true',
                        help='Do not draw legends when plotting')
    parser.add_argument('--solver_rtol',
                        type=float,
                        default=1E-8,
                        help='relative tolerance to solve to')
    args, unknown = parser.parse_known_args()

    call = locals()[args.verb]
    sw = call(args)
