import numpy as np
import matplotlib.pyplot as plt
from firedrake import *
from firedrake.petsc import PETSc

## Extrct the PETSc matrix
def get_PETSC_matrix(form):
    operator = assemble(form).M.handle
    return operator

## Convert to numpy matrix
def get_np_matrix(form):
    L = get_PETSC_matrix(form)
    
    size = L.getSize()
    Lij = PETSc.Mat()
    L.convert('aij', Lij)

    Lnp = np.array(Lij.getValues(range(size[0]), range(size[1])))
    return Lnp

## Gen number of dofs in cell and facet blocks
def get_sizes(N, degree, quads=False):
    # Count quads differently to tets
    if quads:
        sides = 4
        ncells = int(N*(degree + 1)*(degree + 1))
        nfacets = int(0.5*sides*N*(degree + 1))
    else:
        sides = 3
        ncells = int(N*(degree + 1)*(degree + 2))
        nfacets = int(0.5*sides*N*(degree + 1))
        
    return sides, ncells, nfacets

## Swap phi and U fields
def swap_phiu(L, N, dimension, degree, hybridized=0, quads=False):
    
    d = dimension
    _, ncells, __ = get_sizes(N, degree, quads)
    
    # Field indices i old j new
    u_i = slice(0, d*ncells)
    phi_i = slice(d*ncells, (d+1)*ncells)
    phi_j = slice(0, ncells)
    u_j = slice(ncells, (d+1)*ncells)
    
    # Shuffle columns
    tempmat = np.zeros_like(L)
    tempmat[:,:] = L[:,:]
    tempmat[:,phi_j] = L[:,phi_i]
    tempmat[:,u_j] = L[:,u_i]
    
    # Shuffle rows
    L[:,:] = tempmat[:,:]
    L[phi_j,:] = tempmat[phi_i,:]
    L[u_j,:] = tempmat[u_i,:]
    
    return L

def sp_check(L, N, dimension, degree, quads=False):
    d = dimension
    _, ncells, __ = get_sizes(N, degree, quads)
    q_i = slice(0, (d+1)*ncells)
    l_i = slice((d+1)*ncells, None)
    
    S = L[l_i,l_i] - L[l_i,q_i]@np.linalg.inv(L[q_i,q_i])@L[q_i,l_i]
    fig, ax = plt.subplots(1,1)
    plot_mat(ax, S)
    ax.set_axis_on()
    print(L.shape, S.shape)
    plt.show()
    

## Plot matrix sparsity pattern
def plot_mat(ax, A, **params):
    from matplotlib.colors import ListedColormap
    # Mask A
    Am = np.ma.masked_values(A, 0, atol=1e-12)
    
    # Set new colourmap
    #mycmap = plt.cm.coolwarm
    #mycmap.set_bad(color = 'black')
    vals = [0,0,0,1]
    mycmap = ListedColormap(vals)
    mycmap.set_bad(color = 'white')
    
    # Plot matrix
    #plot = ax.matshow(Am, cmap=mycmap, **params)
    plot = ax.matshow(Am, cmap=mycmap, **params)
    
    # Remove axes
    ax.set_axis_off()
        
    return plot

## Add gridlines
def add_gridlines(ax, N, dimension, degree, hybridized=0, quads=False):
    ax.set_axis_on()
    
    sides, ncells, nfacets = get_sizes(N, degree, quads)
    
    # Major ticks
    linepositions = []
    
    # Add a line for each field (ndofs)
    for i in range(dimension):
        linepositions.append((i + 1)*ncells - 0.5)
    
    # Add a line for each field in hybridised section (nfacets)
    if hybridized > 0:
        start = linepositions[-1] + ncells
        for i in range(hybridized):
            linepositions.append(start + i*nfacets)
    
    ax.set_xticks(linepositions, minor=False)
    ax.set_yticks(linepositions, minor=False)
    
    # Labels for major ticks
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    
    # Remove 'tick marks' and border
    ax.tick_params(axis='both', which='both', length=0)
    #~ ax.spines['left'].set_visible(False)
    #~ ax.spines['right'].set_visible(False)
    #~ ax.spines['top'].set_visible(False)
    #~ ax.spines['bottom'].set_visible(False)
    
    # Gridlines based on minor ticks
    ax.grid(which='major', color='g', linestyle='-', linewidth=1)
    
    # Unsure why we need to rescale
    ax.relim()
    ax.autoscale_view()

## Add block labels
def add_block_labels(ax, labels, N, dimension, degree, hybridized=0, quads = False):
    sides, ncells, nfacets = get_sizes(N, degree, quads)
    
    pos = [ii*ncells + ncells//2 for ii in range(dimension+1)]
    pos += [(dimension+1)*ncells + ii*nfacets + nfacets//2 for ii in range(hybridized)]
    for ii, txty in enumerate(labels):
        for jj, txtx in enumerate(labels):
            ax.text(pos[ii], pos[jj], txtx+txty,
                    color='black', alpha=0.5, fontsize=12,
                    horizontalalignment='center',
                    verticalalignment='center')

## Add colourbar for sparsity patterns
def add_colorbar(fig, ax, plot, cb = None):
    if cb == None:
        if type(ax) == np.ndarray:
            cb = fig.colorbar(plot, ax=ax.ravel().tolist())
        else:
            cb = fig.colorbar(plot, ax=ax)
    
    # One sig fig
    onesf = lambda x : float(('%.0e')%x)
    top = onesf(np.max(np.abs(plot.get_clim())))
    plot.set_clim(vmin = -top, vmax = top)
    
    cb.set_clim(vmin = -top, vmax = top)
    cb.ax.plot([0,1], [0.5]*2, 'k', linewidth = 4)
    cb.draw_all()
    return cb

if __name__ == '__main__':
    from swehdg.core.shallow_water import *
    refine = 2
    N = 2**refine
    degree = 3
    mu = 0.5
    quads = True
    
    dim = 2
    M =  N**2
    
    sw_tmp = ShallowWaterHybridizedDG(  'flat',
                                        refine,
                                        degree)
    
    # Grid spacing
    h = sw_tmp.h

    # CFL scaling parameters. The explicit method is stable of approximately
    # nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
    # Here choose a time step size which is \alpha times larger than this, i.e.
    # dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g
    rho_CFL = 0.2

    # scaling factor alpha
    alpha = 10.0
    
    mesh = PeriodicUnitSquareMesh(N, N, quadrilateral=quads)
    dgswe = ShallowWaterDG( 'flat',
                             refine,
                             degree,
                             mesh=mesh)
    
    alpha = 0.5
    u, phi = TrialFunctions(dgswe._V_q)
    mass_integral = (dgswe._psi*phi + inner(dgswe._w, u))*dgswe._dx
    S = dgswe._resL(u, phi)
    bilinearform = mass_integral + alpha*dgswe._cg*S
    
    hdgswe = ShallowWaterHybridizedDG(  'flat',
                                        refine,
                                        degree,
                                        use_gtmg=True,
                                        mesh=mesh)
                                                                         
    u_, phi_, lmbda_ = TrialFunctions(hdgswe._V_q_hyb)
    mass_integral = hdgswe._psi*phi_*hdgswe._dx + inner(hdgswe._w, u_)*hdgswe._dx
    weak_derivative = hdgswe._hybridized_resL(u_, phi_, lmbda_)
    flux_condition = hdgswe._flux_condition(u_,phi_,lmbda_)

    hybridbilinearform = mass_integral + alpha*hdgswe._cg*(weak_derivative + flux_condition)
    
    L1 = get_np_matrix(bilinearform)
    L1 = swap_phiu(L1, M, dim, degree, quads=quads)
    L2 = get_np_matrix(hybridbilinearform)
    L2 = swap_phiu(L2, M, dim, degree, quads=quads)
    
    top = np.max(np.abs(np.hstack([L1.flatten(), L2.flatten()])))
    
    fig, ax = plt.subplots(1, 2, gridspec_kw = {'width_ratios':[L1.shape[0], L2.shape[0]]})
    fig.set_size_inches(4, 2.2)
    ### Fiddle to centre:
    fig.subplots_adjust(left = 0.05, right = 0.95, bottom = 0.05)
    
    p = plot_mat(ax[0], L1, vmin=-top, vmax=top)
    add_gridlines(ax[0], M, dim, degree, hybridized=0, quads=quads)
    add_block_labels(ax[0], [r'$\phi_S$', r'$U_1$', r'$U_2$'], M, dim, degree, hybridized=0, quads=quads)
    ax[0].set_title('DG matrix')
    
    p = plot_mat(ax[1], L2, vmin=-top, vmax=top)
    add_gridlines(ax[1], M, dim, degree, hybridized=1, quads=quads)
    add_block_labels(ax[1], [r'$\phi_S$', r'$U_1$', r'$U_2$', r'$\lambda$'], M, dim, degree, hybridized=1, quads=quads)
    ax[1].set_title('Hybridised DG matrix')
    
    #cb = add_colorbar(fig, ax, p)
    
    #fig.suptitle('Magic matrix sparsity plot')
    fig.savefig('corrected_sparsity.png', dpi=1200, bbox_inches='tight')
    plt.show()
    
    # Can check sparsity of facet block
    #sp_check(L2, M, dim, degree, quads=quads)
    
