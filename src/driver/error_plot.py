import argparse
import os

import matplotlib.pyplot as plt
import pickle as pkl
import swehdg.auxilliary.plotting as stdplot
import swehdg.auxilliary.results_data as resdat

from numpy import sqrt

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--directory',
                        default='.',
                        type=str,
                        help='directory to save or retrieve results from')
    args, unknown = parser.parse_known_args()
    directory = args.directory
    
    # Read in data
    raw_data = resdat.read_results_data(directory)
    # Add time per dof and total error fields
    raw_data['tperdof'] = raw_data['runtime']/(raw_data['ndof']*raw_data['num_ts'])
    raw_data['total_error'] = sqrt(raw_data['phi_error']**2 + raw_data['u_error']**2)
    # Get all methods
    all_methods = resdat.get_methods(raw_data)
    # Rearrange data
    data = raw_data.groupby(['method', 'refinement', 'degree']).mean()
    
    # Get all required degrees and refinements
    select_degree = list(set(data.index.get_level_values('degree')))
    select_ref = list(set(data.index.get_level_values('refinement')))
    
    # Global figure size
    fig_size = (4, 4)
    DPI = 300

    ######
    # PLOT
    ######
    plot_list = []

    for plt_type in ['refinement', 'degree']:
        for field in ['phi_error', 'u_error', 'total_error']:
            for method in all_methods:
                fig, ax = plt.subplots(1, 1)
                fig.set_size_inches(fig_size)
                
                if plt_type == 'degree':
                    stdplot.degree_error_plot(  ax, data,
                                                method=method,
                                                field=field,
                                                select_degree=select_degree,
                                                legends=True)
                else:
                    stdplot.refinement_error_plot(  ax, data,
                                                    method=method,
                                                    field=field,
                                                    select_refinement=select_ref,
                                                    legends=True,
                                                    gradients=True)
                
                plt_name = os.path.join(directory, field+method+plt_type+'.png')
                fig.savefig(plt_name, bbox_inches='tight', dpi=DPI)
                plot_list.append(fig)
                
    with open(os.path.join(directory, 'error_plots.pickle'), 'wb') as fh:
        pkl.dump(plot_list, fh)
