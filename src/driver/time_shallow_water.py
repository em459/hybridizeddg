from firedrake import *
from firedrake.petsc import PETSc
import numpy as np
import time
import sys, os
import argparse
from pyop2.profiling import timed_stage
from swehdg.core.shallow_water import *
from swehdg.core.ksp_monitor import *
from swehdg.core.timestepper import *
from swehdg.auxilliary.balanced_vortex import *
from swehdg.core.periodic_square import PeriodicUnitSquareMeshHierarchy

class NormMonitor(TimeStepperMonitor):
    '''Class for printing out the norm of the potential in each iteration
    of the timestepper'''
    def __init__(self):
        pass
    
    def __enter__(self):
        pass
    
    def __call__(self,t,q):
        phi = q.split()[1]
        print (('time = %8.4f' % t), (' ||phi|| = %12.6e' % math.sqrt(assemble(phi*phi*dx))))


parameters["pyop2_options"]["lazy_evaluation"] = False
# Parse command line options
parser = argparse.ArgumentParser(allow_abbrev=False)
# Mesh geometry
parser.add_argument('--geometry',
                    choices=('spherical','flat'),
                    default='flat',
                    help='mesh geometry')

parser.add_argument('--cell_type',
                    choices=('quads','triangles'),
                    default='triangles',
                    help='cell type (quadrilaterals or triangles')

parser.add_argument('--flux',
                    choices=('upwind','Lax-Friedrichs'),
                    default='Lax-Friedrichs',
                    help='flux to use for the implicit, linear part of operator')

parser.add_argument('--degree',
                    type=int,
                    default=2,
                    help='polynomial degree of DG space')

parser.add_argument('--nrefine',
                    type=int,
                    default=3,
                    help='mesh refinement level, has to be > 1 for flat geometry')

parser.add_argument('--hybrid_multigrid',
                    action='store_true',
                    default=False,
                    help='use hybrid multigrid instead of GAMG for solving hybridized system')
                    
parser.add_argument('--mg_type',
                    choices=('AMG', 'GMG'),
                    default='AMG',
                    help='coarse space multigrid type')
                    
parser.add_argument('--nonlinear',
                    action='store_true',
                    default=False,
                    help='solve non-linear SWEs?')

parser.add_argument('--bathymetry',
                    action='store_true',
                    default=False,
                    help='include non-trivial bathymetry?')

parser.add_argument('--ksp_verbosity',
                    type=int,
                    default=0,
                    help='verbosity of KSP solvers. 0: no output, 1: summary, 2: full output')

parser.add_argument('--initial_condition',
                    choices=('gaussian', 'balanced_vortex', 'flat','solidbodyrotation'),
                    default='balanced_vortex',
                    help='initial condition; can be Gaussian bump (\'gaussian\') with zero momentum, balanced stationary vortex (\'balanced_vortex\'), flat surface with zero velocity (\'flat\') or solid body rotation (\'solidbodyrotation\')')

args, unknown = parser.parse_known_args()

# Off-centering parameter mu (1.0=fully implicit, 0.0=fully explicit)
mu = 0.5

# Final time 
tfinal = 0.5

ref = args.nrefine
if args.mg_type == 'GMG':
    # Mesh hierarchy for transfering from trace to p1.
    # NOTE: For gmg, may need to create a full mesh hierarchy
    # that includes the coarsened spaces for the p1 solver.
    mesh_hierarchy = PeriodicUnitSquareMeshHierarchy(2**ref, ref//2)
    mesh = mesh_hierarchy.mesh()
else:
    if (args.geometry == 'flat'):
        mesh = PeriodicUnitSquareMesh(2**ref, 2**ref)
    else:
        mesh = UnitIcosahedralSphereMesh(2**ref)
        x_mesh = SpatialCoordinate(mesh)
        mesh.init_cell_orientations(x_mesh)

sw_tmp = ShallowWaterHybridizedDG(args.geometry,
                                  args.nrefine,
                                  args.degree,
                                  mesh=mesh, flux='Lax-Friedrichs')

def pprint(*s):
    '''Print string only on master process

    :arg s: Stuff to print
    '''
    if (mesh.comm.rank == 0):
        print (*s)

# Number of cells and facets
ncell = sw_tmp.ncell
nfacet = sw_tmp.nfacet

# Number of DG unknowns per cell 
ndof_cell = ncell * sw_tmp.ndof_per_cell
ndof_facet = nfacet * sw_tmp.ndof_per_facet

# Grid spacing
h = sw_tmp.h

# CFL scaling parameters. The explicit method is stable of approximately
# nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
# Here choose a time step size which is \alpha times larger than this, i.e.
# dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g

rho_CFL = 0.2

# scaling factor alpha
alpha = 10.0

# Time step size
dt = alpha*rho_CFL*h/(2.*args.degree+1)/sw_tmp.cg

# Print out parameters of run
pprint (" geometry          = ", args.geometry)
pprint (" cell type         = ", args.cell_type)
pprint (" initial condition = ", args.initial_condition)
pprint (" flux              = ", args.flux)
pprint (" nrefine           = ", args.nrefine)
pprint (" degree p          = ", args.degree)
pprint (" h                 = ", h)
pprint (" dt                = ", dt)
pprint (" mu                = ", mu)
pprint (" alpha             = ", alpha)
pprint (" T                 = ", tfinal)
pprint (" #cells            = ", ncell)
pprint (" #facets           = ", nfacet)
pprint (" non-linear?       = ", args.nonlinear)      
pprint (" hybrid MG?        = ", args.hybrid_multigrid)

# Print out number of processors
pprint(" Number of processes = ",mesh.comm.size)

# Print out numbers of unknowns
pprint (" total number of DG unknowns   [ndof_cell]  = ",ndof_cell)
pprint (" total number of flux unknowns [ndof_facet] = ",ndof_facet)
pprint (" ndof_facet/ndof_cell = ",( '%6.4f' % (ndof_facet/(1.0*ndof_cell)) ))

filename0 = 'test_output/write_test/test0.pvd'
filename1 = 'test_output/write_test/test1.pvd'
filename2 = 'test_output/write_test/test2.pvd'

deltaNL = int(args.nonlinear)

# Need vortex earlier to specify bathymetry
f_over_cg = 2.0*sw_tmp._Omega/sw_tmp._cg
non_linear = (deltaNL==1)
dphi = 0.1
vortex = BalancedVortex(dphi,f_over_cg,non_linear,bathymetry=True)

# Construct bathymetry
V_bath = FunctionSpace(mesh, 'CG', 1)
if args.bathymetry:
    x, y = SpatialCoordinate(mesh)
    bathymetry = Function(V_bath, name='bathymetry')
    dphi = 0.1
    if (args.initial_condition=='balanced_vortex'):
        f_bathymetry = vortex.bath_ufl(mesh)
    else:
        f_bathymetry = 1.0+dphi*cos(2*math.pi*x)*cos(2*math.pi*y)
        
    bathymetry.interpolate(f_bathymetry)
    outfile = File('test_output/write_test/bathymetry.pvd')
    outfile.write(bathymetry)
else:
    f_bathymetry = None

if (args.initial_condition == 'solidbodyrotation'):
    coriolis = False
else:
    coriolis = True
    
# Dictionary with shallow water solvers
sw_dict = [
            ('native_exp', ShallowWaterDG(args.geometry,
                                          args.nrefine,
                                          args.degree,
                                          mesh=mesh,
                                          bathymetry=f_bathymetry,
                                          deltaNL=deltaNL,
                                          coriolis=coriolis,
                                          output_filename=filename0,
                                          flux=args.flux,
                                          ksp_verbose=args.ksp_verbosity)),
            ('native', ShallowWaterDG(args.geometry,
                                      args.nrefine,
                                      args.degree,
                                      mesh=mesh,
                                      bathymetry=f_bathymetry,
                                      deltaNL=deltaNL,
                                      coriolis=coriolis,
                                      output_filename=filename1,
                                      flux=args.flux,
                                      ksp_verbose=args.ksp_verbosity)),
           ('hybridized', ShallowWaterHybridizedDG(args.geometry,
                                                   args.nrefine,
                                                   args.degree,
                                                   mesh=mesh,
                                                   bathymetry=f_bathymetry,
                                                   deltaNL=deltaNL,
                                                   coriolis=coriolis,
                                                   use_gtmg=args.hybrid_multigrid,
                                                   mg_type=args.mg_type,
                                                   output_filename=filename2,
                                                   flux=args.flux,
                                                   ksp_verbose=args.ksp_verbosity))
            ]

for sw_label, sw in sw_dict:
    pprint ('Running '+sw_label)
    V_q = sw.V_q
    
    # Construct initial condition
    q = Function(V_q)
    u, phi = q.split()
    u.assign(0.0)
    if (args.geometry == 'flat'):
        # width of initial peak
        if (args.initial_condition=='balanced_vortex'):
            phi.interpolate(vortex.phis_ufl(sw.mesh))
            phi_exact  = phi.copy()
            u.interpolate(vortex.u_ufl(sw.mesh))
        elif (args.initial_condition=='gaussian'):
            x,y = SpatialCoordinate(sw.mesh)
            sigma = 0.2
            phi.interpolate(exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2))
            u.interpolate(as_vector((x,y)))
        elif (args.initial_condition=='flat'):
            phi.assign(0.1)
        else:
            pprint ('Invalid initial condition')
            sys.exit()
    else:
        if (args.initial_condition=='solidbodyrotation'):
            # Solid body rotation test
            x,y,z = SpatialCoordinate(sw.mesh)
            # width of initial peak
            phi.assign(0.1)
            phi_exact  = phi.copy()
            u0 = 0.01
            u.interpolate(as_vector((-u0*y,u0*x,0)))
        else:
            pprint ('Invalid initial condition')
            sys.exit()
            
    pprint ('warmup runs...')
    # warmup run
    out = sys.stdout
    out_null = open(os.devnull, 'w')
    #sys.stdout = out_null
    monitor=NormMonitor()
    TSClass = TimeStepperTheta
    if sw_label == 'native_exp':
        timestepper = TSClass(sw,dt/10,0,monitor)
        print('New dt for explicit timestepping:', dt/10)
    else:
        timestepper = TSClass(sw,dt,mu,monitor)
    with monitor:
        timestepper.integrate(q,2.0*dt)
    sys.stdout = out
    
    pprint ('... finished')
    pprint ('')
    
    t_start = time.time()
    with timed_stage(sw_label + ' ' + str(args.degree)):
                    
        monitor=SaveToDiskMonitor(sw._output_filename, [0,1],['momentum','height'])
        if sw_label == 'native_exp':
            timestepper = TSClass(sw,dt/10,0,monitor)
            print('New dt for explicit timestepping:', dt/10)
        else:
            timestepper = TSClass(sw,dt,mu,monitor)
        with monitor:
            q_final = timestepper.integrate(q,tfinal)

    t_finish = time.time()
    
    pprint ('   elapsed time ['+sw_label+'] = ',t_finish-t_start,' s')
    pprint ()
    pprint ('Number of KSP iterations')
    for ksp_monitor in sw._ksp_monitors:
        s = '  '+ksp_monitor.label+': '
        for it in ksp_monitor.niter:
            s += str(it)+' '
        pprint(s)
        try:
            pprint(len(ksp_monitor.niter))
        except:
            pass
    pprint ('')
    # Work out error
    if (args.initial_condition=='balanced_vortex' or args.initial_condition=='solidbodyrotation'):
        phi_final = q_final.split()[1]
        phi_error = math.sqrt(assemble((phi_final-phi_exact)*(phi_final-phi_exact)*dx))
        pprint ('error in phi-field = '+('%12.4e' % phi_error))
        pprint ('')
    
