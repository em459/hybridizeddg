import math
from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.ksp_monitor import *
from swehdg.core.timestepper import *
import numpy as np
import time
import sys, os

''' 
=======================================================================
Find the largest stable time step size for a given mesh size and 
polynomial degree
=======================================================================
'''

class StableDtFinder(object):
    '''Find the stable time step for a given setup
    
    :arg geometry: mesh geometry
    :arg degree: polynomial degree
    :arg nrefine: refinement level of mesh
    '''
    def __init__(self,geometry,degree,nrefine):

        self._geometry = geometry
        self._degree = degree
        self._nrefine = nrefine
        mu = 0.0
        # Create temporary object to extract parameters
        shallowwater_tmp = ShallowWaterDG(self._geometry,
                                          self._nrefine,
                                          self._degree)
        self._h = shallowwater_tmp.h
        self._cg = shallowwater_tmp.cg
        self.comm = shallowwater_tmp.mesh.comm 

    def _set_stationary(self,sw,phi,u):
        '''Set initial conditions of a state in geometrophic balance (i.e.
        being a stationary solution of the shallow water equations.

        In the 2d case, set up a rotationally invariant smooth field
        :math:`\\phi(r) = \\exp\\left[-\\left(\frac{r}{r-\\sigma}\\right)^2\\right]`
        where :math:`r` is the distance from the centre of the unit square and
        :math:`\\sigma<1` is the diameter of the vortex. Then construct a
        purely tangential velocity field as
        :math:`\\vec{u} = -\\frac{c_g}{2\\Omega}\\phi'(r)\\hat{\\vec{r}}^{\\perp}`.
    
        In the 3d case, set of a smooth field :math:`\\phi(z)` which only
        varies with latitude :math:`\\theta` with :math:`\\sin(\\theta)=z` and
        is restricted to the range :math:`-\\sigma<z<\\sigma`. In 
        particular :math:`\\phi(z) = \\exp\\left[\\frac{\\sigma^2}{\\sigma^2-z^2}\\right]`.
        Then construct a purely longitudinal tangential velocity field
        with the longitudinal component given as :math:`u_T=-\\frac{c_g}{2\\Omega\\tan(\\theta)}\\phi'(z) = -\\frac{c_g\\sqrt{1-z^2}}{2\\Omega z}\\phi'(z)`.

        :arg sw: Instance of a shallow water class (to extract c_g and Omega)
        :arg phi: potential field to apply initial conditions to
        :arg u: momentum field to apply initial conditions to
        '''
        mesh = phi.function_space().mesh()
        dim = mesh.geometric_dimension()
        if (dim == 2):
            sigma = 0.4
            x,y = SpatialCoordinate(mesh)
            x -= 0.5
            y -= 0.5
            r = sqrt(x**2+y**2)
            f_phi = conditional(r<sigma,exp(-(r/(r-sigma))**2),0.0)
            f_u_t = conditional(r<sigma,2.*r*sigma/(r-sigma)**3*exp(-(r/(r-sigma))**2),0.0)
            cg_over_f = sw.cg/(2.*sw._Omega)
            f_u = (-y/sqrt(x**2+y**2)*f_u_t*(cg_over_f),
                   x/sqrt(x**2+y**2)*f_u_t*(cg_over_f))
            phi.project(f_phi)
            u.project(as_vector(f_u))
        else:
            sigma = 0.8
            x,y,z = SpatialCoordinate(mesh)
            f_phi = conditional(And((-sigma<z),(z<sigma)),
                                exp(-sigma**2/(sigma**2-z**2)),
                                0.0)
            f_u_t = conditional(And((-sigma<z),(z<sigma)),
                                -2.*sigma**2*z/(sigma**2-z**2)**2*exp(-sigma**2/(sigma**2-z**2)),
                                0.0)
            cg_over_f = sw.cg/(2.*sw._Omega)*sqrt(1.-z*z)/z
            f_u = (-y/sqrt(x**2+y**2)*f_u_t*(-cg_over_f),
                   x/sqrt(x**2+y**2)*f_u_t*(-cg_over_f),
                   0.0)
            phi.project(f_phi)
            u.project(as_vector(f_u))

        
    def _rel_norm(self,dt):
        '''Run with time step size dt and return ratio between
        ||phi|| and ||phi_0|| for a stationary vortex.

        :arg dt: Timestep size
        '''
        # Use explicit integrator (mu=0.0)
        mu = 0.0
        shallowwater = ShallowWaterDG(self._geometry,
                                      self._nrefine,
                                      self._degree)
    
        V_q = shallowwater.V_q
        q = Function(V_q)
        u, phi = q.split()
        self._set_stationary(shallowwater,phi,u)
        phi0_nrm = math.sqrt(assemble(phi*phi*dx))
        
        # Final time 
        tfinal = min(200*dt,1.0)

        timestepper = TimeStepperIMEXTheta(shallowwater,dt,mu)
        q_final = timestepper.integrate(q,tfinal)
        
        u_final, phi_final = q_final.split()
    
        phi_nrm = math.sqrt(assemble(phi_final*phi_final*dx))

        return phi_nrm/phi0_nrm

    def nu_scal_theor(self,dt):
        ''' Calculate theoretical scaled CFL number for a fixed timestep size,
        which is given by

        nu_{scal} = c_g*dt/dx*(2*p+1)
        
        :arg dt: time step size
        '''
        return self._cg*dt/self._h*(2.*self._degree+1.)

    def bisection_search(self):
        '''Use bisection seach in the range dt_{min},dt_{max} to find
        the largest possible dt. The range of possible considered dt is
        given by dt_{min} = 0.1*dx/(c_g*(2*p+1)) and 
        dt_{max} = 1.0*dx/(c_g*(2*p+1)).'''
        dt_theor = self._h/((2.*self._degree+1.)*self._cg)
        dt_min = 0.1*dt_theor
        dt_max = 1.0*dt_theor
        # Calculate the ratio at the boundary values
        ratio_min = self._rel_norm(dt_min)
        ratio_min = self._rel_norm(dt_max)
        # Bisection search the optimal value (to a relative tolerance of 1%)
        threshold=1.25
        while ((dt_max-dt_min)/dt_max > 0.01):
            dt = 0.5*(dt_min+dt_max)
            ratio = self._rel_norm(dt)
            if (ratio < threshold):
                dt_min = dt
                ratio_min = ratio
            else:
                dt_max = dt
                ratio_max = ratio
        return dt_min

# Grid geometry
geometry = 'flat'

# Loop over all polynomial degrees and mesh refinement levels and
# print out the largest stable time step size and the scaled CFL number
# nu_{scal} = c_g*dt/dx*(2*p+1)
for degree in (0,1,2,3):
    for nrefine in (2,3,4):
        dt_finder = StableDtFinder(geometry,degree,nrefine)
        comm = dt_finder.comm
        dt = dt_finder.bisection_search()
        nu_scal = dt_finder.nu_scal_theor(dt)
        if (comm.rank == 0):
            print ('degree = '+('%d' % degree)+'   nrefine = '+('%1d' % nrefine)+' nu_{scal} = '+('%5.2f' % nu_scal)+' dt = '+('%10.6f' % dt))
