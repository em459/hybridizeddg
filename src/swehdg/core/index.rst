.. Hybridised Shallow Water documentation master file, created by
   sphinx-quickstart on Tue Feb 27 08:48:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hybridised Shallow Water's documentation!
====================================================

This repository contains code for solving the linarised shallow water equations
on a sphere and in a flat domain. IMEX timestepping methods are used to propagate the state in time.
When solving the linear system in the implicit step, both a
native-fieldsplit solver and a hybridised solver can be used.

Contents:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
