==================================
Hybridized Shallow Water equations
==================================

Solver for linearised shallow water equations with Coriolis force on the sphere and in a flat geometry.

Mathematical formulation
------------------------

Fundamental equations
^^^^^^^^^^^^^^^^^^^^^

The equations to be solved are given by

  .. math::
    \partial_t \vec{u} + c_g\Pi\nabla\phi &= -f \vec{\hat{r}} \times \vec{u}\\
    \partial_t \phi + c_g \nabla\cdot \Pi\vec{u} &= 0

where :math:`\phi` is the (non-dimensionalised) geopotential and
:math:`\vec{u}` the (non-dimensionalised) momentum.
:math:`\Pi=1-\hat{\vec{r}}\hat{\vec{r}}^T` is the operator which projects onto
the tangential component of the velocity. We write
:math:`q = (\vec{u},\phi)` for the state of the system, so the equations can be expressed in conservative form as

  .. math::
    \partial_t q + \partial_i F_i(q) = \partial_t q + \partial_ i (A^{(i)} q) = R_q \qquad\text{with}\quad A^{(i)} = \begin{pmatrix}0&c_g\vec{\pi}^{(i)} \\ c_g \left(\vec{\pi}^{(i)}\right)^T&0 \end{pmatrix},\; R_q = \begin{pmatrix}-f \vec{\hat{r}} \times \vec{u}\\0\end{pmatrix} 

The entries of the vector :math:`\vec{\pi}^{(i)}` are defined as :math:`\pi^{(i)}_j = \Pi_{ij}`. :math:`c_g` and :math:`f` are the dimensionless gravity wave speed and
(spatially varying) Coriolis parameter given by

  .. math::
    c_g &= T_{ref}/R_{earth}\sqrt{gH} \\
    f &= 2\Omega T_{ref}\sin(\theta) = 2\Omega T_{ref} z

In this expression :math:`\theta` is the azimuthal angle (measured from the equator).
:math:`\hat{\vec{r}}` is the outward pointing normal vector. All distances are
measured in units of the Earth radius :math:`R_{earth}` and all times in units
of the reference time :math:`T_{ref}`.

We use the following parameters:

  * reference time :math:`T_{ref}`
  * Earth radius :math:`R_{earth}`
  * gravitational acceleration :math:`g`
  * water depth :math:`H`
  * angular frequency of the Earth's rotation :math:`\Omega`

Note that due to the non-dimensionalisation the computational domain is the
unit sphere, which is why :math:`\sin(\theta)=z` in the Coriolis parameter.

To apply IMEX timestepping, we rewrite the equations as

 .. math::
    q_t = \mathcal{L}q + \mathcal{N}(q)
    :label: general_time_dep

where :math:`\mathcal{L}` is a linear operator which encapsulates the fast modes and :math:`\mathcal{N}` is the non-linear part. Only the fast modes are treated implicitly.

Spatial discretisation
^^^^^^^^^^^^^^^^^^^^^^
A Discontinuous Galerkin (DG) discretisation is used in space, with
:math:`\phi\in V_h^k` and :math:`\vec{u}\in (V_h^k)^3` where :math:`V_h^k` is
the DG space of polynomial degree :math:`k`. Multiplying :eq:`general_time_dep`
by a test function :math:`p=(\vec{w},\psi)` and integrating over the domain this leads to
   
  .. math::
    \int p q_t \;dx = \int p \mathcal{L}q\;dx + \int p \mathcal{N}(q)\;dx

where :math:`\int p\mathcal{L}q\;dx =: -c_g S(\Pi\vec{w},\psi;\Pi\vec{u},\phi)` and :math:`\int p\mathcal{N}(q)\;dx=\sum_K (-f\vec{w},\vec{\hat{r}}\times\vec{u})_K`. For an upwind (Godunov) flux the weak-derivative term :math:`S(\vec{w},\psi;\vec{u},\phi)` is
given as

  .. math::
    S(\vec{w},\psi;\vec{u},\phi) &= - \sum_K \left( (\nabla\psi,\vec{u})_K + (\nabla\cdot\vec{w},\phi)_K\right) + \sum_{\partial K}\Big(([[\psi]],\{\{\vec{u}\}\})_{\partial K} + \frac{1}{2}(\delta \psi,\delta \phi)_{\partial K}\\
    &\quad +\;\;([[\vec{w}]],\{\{\phi\}\})_{\partial K} + \frac{1}{2}([[\vec{w}]],[[\vec{u}]])_{\partial K}\Big)
   :label: weakflux

See section ":ref:`sec_numerical_flux`" for a derivation.
Here :math:`\vec{n}` is the normal on a facet. We define the jump and
average as for scalar-valued functions :math:`\eta` and vector-valued
functions :math:`\vec{v}` as:

  * :math:`[[\eta]] = \eta_+\vec{n}_+ + \eta_-\vec{n}_-`
  * :math:`[[\vec{v}]] = \vec{v}_+\cdot\vec{n}_+ + \vec{v}_-\cdot\vec{n}_-`
  * :math:`\{\{\eta\}\} = \frac{1}{2}(\eta_++\eta_-)` and :math:`\{\{\vec{v}\}\} = \frac{1}{2}(\vec{v}_++\vec{v}_-)`
  * :math:`\delta \eta = \eta_+ - \eta_-` and :math:`\delta \vec{v} = \vec{v}_+ - \vec{v}_-`

Note that for a given mesh on the sphere in general :math:`\vec{n}_{-}+\vec{n}_{+}\ne 0` and the scheme will not me conservative (to see this, set :math:`(\psi,\vec{w})=(1,0)`). Hence in the code we replace :math:`\vec{n}_{-}\mapsto\frac{1}{2}(\vec{n}_{-}-\vec{n}_{+})` and :math:`\vec{n}_{+}\mapsto\frac{1}{2}(\vec{n}_{+}-\vec{n}_{-})`.

One problem with :eq:`weakflux` is that for a finite resolution :math:`\nabla\cdot \Pi\vec{w}\ne0` even if :math:`\Pi\vec{w}=0`. This would imply that :math:`S(\Pi\vec{w},\psi;\vec{u},\phi)\ne 0` and hence the radial component of the velocity would not remain zero throughout the simulation. To address this issue, use integration by parts

  .. math::
   (\nabla\vec{w},\phi)_K = -(\vec{w},\nabla\phi)_K + \sum_{\partial K, \partial K\cap K\ne 0} (\vec{n}\cdot \vec{w},\phi)_{\partial K}
   :label: integration_by_parts
      
and rewrite
  .. math::
    S(\vec{w},\psi;\vec{u},\phi) &= - \sum_K \left( (\nabla\psi,\vec{u})_K - (\vec{w},\nabla \phi)_K\right) + \sum_{\partial K}\Big(([[\psi]],\{\{\vec{u}\}\})_{\partial K} + \frac{1}{2}(\delta \psi,\delta \phi)_{\partial K}\\
    &\quad -\;\;\frac{1}{2}(\delta(\vec{w}\cdot\vec{n}),\delta\phi)_{\partial K} + \frac{1}{2}([[\vec{w}]],[[\vec{u}]])_{\partial K}\Big)

.. _sec_numerical_flux:
       
More on the numerical flux term 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Godunov (upwind) flux for the hyperbolic system is derived by considering

  .. math::
    F_i(q) = A^{(i)} q = \begin{pmatrix}0&c_g\vec{\pi}^{(i)}\\ c_g\left(\vec{\pi}^{(i)}\right)^T&0 \end{pmatrix}
     \begin{pmatrix}\vec{u}\\\psi\end{pmatrix}

and defining the numerical outward flux through a facet as
  .. math::
    F^*\cdot \vec{n}_- &= \frac{1}{2}A(q_-+q_+)\cdot\vec{n}_- + \frac{1}{2}|A| (q_- - q_+)\\
    &= \begin{pmatrix}
      \frac{c_g}{2}(\phi_-+\phi_+)\Pi\vec{n}_-
      +\frac{c_g}{2}(\vec{n}_{-}\cdot\Pi\vec{u}_-+\vec{n}_{+}\cdot\Pi\vec{u}_+)\Pi\vec{n}_-\\
      \frac{c_g}{2}\vec{n}_-\cdot \Pi(\vec{u}_-+\vec{u}_+)
      +\frac{c_g}{2}(\phi_--\phi_+)
     \end{pmatrix}

Here we use
  .. math::
     A = \sum_j n_i A^{(i)} = \begin{pmatrix} 0 & c_g \Pi \vec{n}\\c_g \vec{n}^T\Pi & 0\end{pmatrix} =: R S R^{-1},\qquad |A| := R |S| R^{-1} = \begin{pmatrix}c_g \Pi \vec{n}\vec{n}^T \Pi & 0\\ 0 & c_g \end{pmatrix}
     
By considering both :math:`F^*\cdot\vec{n}_-` and :math:`F^*\cdot\vec{n}_+`
this leads to the weak derivative given in :eq:`weakflux`.
The time stepping method for the vector of unknowns :math:`\vec{q}` is
discussed in :doc:`timestepper`.
      
Hybridization
^^^^^^^^^^^^^
To hybridize, introduce a trace space :math:`\Lambda_h`. :math:`q^H = (\vec{u},\phi,\lambda)` is the hybridized state function and :math:`p^H = (\vec{w},\psi,\tau)` is a test-function living in the same spaces :math:`V^H = (V_h^k)^3\otimes V_h^k\otimes \Lambda_h`.

When extending the space, the linear part of the weak form :math:`\int pLq\;dx=-c_g S(\Pi\vec{w},\psi;\Pi\vec{u},\phi)` is replaced by :math:`-c_g \left(S(\Pi\vec{w},\psi;\Pi\vec{u},\phi,\lambda)+C_F(\tau;\Pi\vec{u},\phi,\lambda) \right)`. :math:`S^H` is the hybridized version of the weak derivative and :math:`C_F` weakly enforces flux continuity on all facets. In one grid cell :math:`K` those forms are given as

  .. math::
     \begin{aligned}
     S^H(\vec{w},\psi;\vec{u},\phi,\lambda) &= - \sum_K\left((\nabla\psi,\vec{u})_K - (\vec{w},\nabla\phi)_K\right)\\
     &\quad+\;\;\sum_{\partial K}\Big((\psi_-,\vec{u}_-\cdot\vec{n}_-)_{\partial K} + (\psi_+,\vec{u}_+\cdot\vec{n}_+)_{\partial K}\\
     &\quad+\;\; (\psi_-,\phi_--\lambda)_{\partial K} + (\psi_+,\phi_+-\lambda)_{\partial K} + ([[\vec{w}]],\lambda)_{\partial K}\\
     &\quad-\;\; (\vec{w}_-\cdot\vec{n}_-,\phi_-)_{\partial K} - (\vec{w}_+\cdot\vec{n}_+,\phi_+)_{\partial K}\Big)\\
     C_F(\tau;\vec{u},\phi,\lambda) &=  (\tau,[[\vec{u}]])_{\partial K} + (\tau,\phi_--\lambda)_{\partial K} + (\tau,\phi_+-\lambda)_{\partial K}
     \end{aligned}
     :label: hybridized_derivative
             
Note that we only write down the contribution of the facet integral from this cell-there will be another contribution from the neighbouring cell.
As for the non-hybridized version we used the identity in Eq. :eq:`integration_by_parts` to rewrite the term :math:`(\nabla\cdot\vec{w},\phi)_K`.

Time discretisation of the hybridized system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
IMEX timestepping for the hybridized system is given by
  .. math::
     \begin{aligned}
     M \vec{Q}^{(i)} &= M\vec{q}^{(n)} + \Delta t \sum_{j=1}^{i-1} a_{ij} N(\vec{Q}^{(j)}) + \Delta t \sum_{j=1}^{i} \tilde{a}_{ij} (L^H \vec{Q}^{(j)}+\tilde{L}^H \tilde{Q}^{(j)})\\
     0 &= \Delta t \tilde{a}_{ii} (L^F \vec{Q}^{(i)} + \tilde{L}^F\tilde{Q}^{(i)})\\
     M \vec{q}^{(n+1)} &= M\vec{q}^{(n)} + \Delta t \sum_{i=1}^{s} b_{i} N(\vec{Q}^{(i)}) + \Delta t \sum_{i=1}^{s} \tilde{b}_{i} (L^H \vec{Q}^{(i)}+\tilde{L}^H\tilde{Q}^{(i)})
     \end{aligned}

Here :math:`\tilde{Q}^{(i)}` are the flux unknowns at the :math:`i`-th stage. :math:`L^H` and :math:`\tilde{L}^H` are the linear operators arising from the weak derivative :math:`S^H` (first term in Eq. :eq:`hybridized_derivative`). :math:`L^F` is the linear operator which weakly enforces the flux condition (second term in Eq. :eq:`hybridized_derivative`).
     
At the :math:`i`-th stage this requires the solution of the :math:`2\times 2` linear system
   .. math::
      \begin{pmatrix}
      M - \Delta t \tilde{a}_{ii} L^H & -\Delta t \tilde{a}_{ii} \tilde{L}^H\\
      - \Delta t \tilde{a}_{ii} L^F & -\Delta t \tilde{a}_{ii} \tilde{L}^F
      \end{pmatrix}
      \begin{pmatrix}
      \vec{Q}^{(i)}\\\tilde{Q}^{(i)}
      \end{pmatrix}
      =
      \begin{pmatrix}
      R_i \\ 0
      \end{pmatrix}
      :label: hybridized_solve
      
with the right hand side

   .. math::
      \vec{R}_i := M\vec{q}^{(n)}
        + \Delta t \sum_{j=1}^{i-1} \left(a_{ij} N(\vec{Q}^{(j)}) +
      \tilde{a}_{ij} (L^H \vec{Q}^{(j)}+\tilde{L}^H\tilde{Q}^{(i)})\right).

Since the scaling of the flux condition (i.e. the second equation in Eq. :eq:`hybridized_derivative`) does not matter, for a purely explicit stage with :math:`\tilde{a}_{ii}` we replace Eq. :eq:`hybridized_solve` by

   .. math::
      \begin{pmatrix}
      M  & 0\\
      L^F & \tilde{L}^F
      \end{pmatrix}
      \begin{pmatrix}
      \vec{Q}^{(i)}\\\tilde{Q}^{(i)}
      \end{pmatrix}
      =
      \begin{pmatrix}
      R_i \\ 0
      \end{pmatrix}

This equation can be solved with a lower-triangular Schur-complement approach. Note furthermore that the mass matrix :math:`M` is block-diagonal.
      
Solving the hybridized system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Starting from Eq. :eq:`hybridized_solve`, hybridization leads to the following :math:`3\times 3`-matrix system for the vector of unknowns :math:`(\vec{U},\vec{\Phi},\vec{\Lambda})`:
  .. math::
     \begin{pmatrix}
     A_{00} & A_{01} & A_{02}\\
     A_{10} & A_{11} & A_{12}\\
     A_{20} & A_{21} & A_{22}
     \end{pmatrix}
     \begin{pmatrix}
     \vec{U} \\ \vec{\Phi} \\ \vec{\Lambda}
     \end{pmatrix}
     =
     \begin{pmatrix}
     \vec{R}_u \\ \vec{R}_\phi \\ \vec{R}_{\lambda}
     \end{pmatrix}

Further define the following submatrices
  .. math::
     \tilde{A} := 
     \begin{pmatrix}
     A_{00} & A_{01}\\
     A_{10} & A_{11}\\
     \end{pmatrix},
     \qquad
     Q :=
     \begin{pmatrix}
     A_{20} & A_{21}
     \end{pmatrix},
     \qquad
     Q^T :=
     \begin{pmatrix}
     A_{02} \\ A_{12}
     \end{pmatrix},
     \qquad
     M_L := A_{22}

Note that :math:`A_{00}`, :math:`A_{01}`, :math:`A_{10}` and :math:`A_{11}` are block-diagonal in the sense the only unknowns within one cell couple to each other. This allows local inversion of those matrices and consequently local inversion of the :math:`2\times 2` matrix :math:`\tilde{A}`.

We can also define the Schur-complement in the flux-space as
   .. math::
      S := M_L - Q\tilde{A}^{-1}Q^T

Combing the potential- and momentum vectors into one vector :math:`\vec{Z}:=(\vec{U},\vec{\Phi})` (and also doing the same for the right hand side :math:`\vec{R}_z:=(\vec{R}_u,\vec{R}_\phi)`), we can write
  .. math::
    \begin{pmatrix}
    \tilde{A} & Q^T\\
    Q & M_L
    \end{pmatrix}
    \begin{pmatrix}
    \vec{Z}\\\vec{\Lambda}
    \end{pmatrix}
    =
    \begin{pmatrix}
    \vec{R}_z \\ \vec{R}_{\lambda}
    \end{pmatrix}

This system is solved in three steps:

1. Calculate modified right hand side :math:`\vec{R}^*_\lambda=\vec{R}_\lambda-Q\tilde{A}^{-1}\vec{R}_z`
2. Solve Schur-complement system :math:`S\vec{\Lambda} = \vec{R}^*_{\lambda}` for :math:`\vec{\Lambda}`
3. Back-substitute to recover potential and velocity :math:`\vec{Z}=\tilde{A}^{-1}(\vec{R}_z - Q^T\vec{\Lambda})`

Due to the block-diagonal structure of :math:`\tilde{A}` steps 1 and 3 are entirely local operations.

The reconstruction step 3 can also be further broken down by observing that
  .. math::
     \begin{pmatrix}
     A_{00} & A_{01}\\
     A_{10} & A_{11}
     \end{pmatrix}
     \begin{pmatrix}
     \vec{U}\\\vec{\Phi}
     \end{pmatrix}
     =
     \begin{pmatrix}
     \vec{R}_u - A_{02}\vec{\Lambda}\\
     \vec{R}_\phi - A_{12}\vec{\Lambda}
     \end{pmatrix}

To recover :math:`\vec{\Phi}` and :math:`\vec{U}` we use a Schur-complement approach again and solve in three steps:

1. Construct modified right hand side :math:`\vec{R}^*_u=\vec{R}_u - A_{01}A_{11}^{-1}\vec{R}_\phi - (A_{02}-A_{01}A_{11}^{-1}A_{12})\vec{\Lambda}`
2. Solve the system :math:`S_{u}\vec{\Phi}=\vec{R}_u^*` to recover the momentum :math:`\vec{U}`
3. Given the momentum :math:`\vec{U}` and :math:`\vec{\Lambda}`, recover the potential as :math:`\vec{\Phi}=A_{11}^{-1}(\vec{R}_{\phi}-A_{10}\vec{U}-A_{12}\vec{\Lambda})`
   
Class documentation
-------------------
    
.. automodule:: shallow_water
    :members:
    :special-members:
    :private-members:
    :show-inheritance:
