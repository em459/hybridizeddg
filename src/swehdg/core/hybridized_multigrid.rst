==========================
Hybrid multigrid algorithm
==========================

Function space hierarchy
------------------------
To solve the system

  .. math::
    a(\tau,\lambda) = b(\tau) \qquad\text{for all}\;\tau\in\Lambda_h
    :label: hybridizedeqn

in trace space, we use a multigrid algorithm, similar to the one described in `Gopalakrishnan and Tan (2009) <http://onlinelibrary.wiley.com/doi/10.1002/nla.636/abstract>`_. They define a (non-nested) hierarchy of function spaces :math:`M_1,\dots,M_J` where :math:`M_j=\Lambda_h` is the hybridized space above and :math:`M_{J-1}=P_{1,h}` is a conforming linear space on the same mesh. The spaces :math:`M_1,\dots,M_{J-2}` are obtained from :math:`M_{J-1}` via standard :math:`h`-coarsening.

Intergrid operators
^^^^^^^^^^^^^^^^^^^
Following Eq. (10) in `Gopalakrishnan and Tan (2009) <http://onlinelibrary.wiley.com/doi/10.1002/nla.636/abstract>`_, the **prolongation** :math:`I_J:M_{J-1}=P_{1,h}\rightarrow M_{J}=\Lambda_h` can be defined as follows

  .. math::
     I_J : P_{1,h} \ni v \mapsto I_J v = v|_{\varepsilon_h} = \lambda \in \Lambda_h

where :math:`\varepsilon_h` is the set of all facets of the mesh. Since on a particular facet :math:`F` the function spaces are nested, :math:`P_{1,h}|_{F}\subset \Lambda_h|_{F}`, this implies that the prolongated function :math:`\lambda` can be found by solving the following equation on each facet :math:`F`:

  .. math::
     (\tau,\lambda)_{F} = (\tau,v)_{F}\qquad\text{for all}\; \tau\in\Lambda_h|_{F}

Given :math:`v`, finding :math:`\lambda` effectively requires a (block-diagonal) mass matrix solve (depending on the choice of basis of :math:`\Lambda_h|_F`, there are probably more efficient ways of implementing this. For example, if the basis of :math:`\Lambda_h|_F` contains the linear functions with nodal points at the end of the interval which forms the facet in 2d, all we need to do is set the corresponding unknowns).

**Restriction** is carried out in dual space. For this, consider the fine-level residual functional :math:`r_J:M_J=\Lambda_h\rightarrow \mathbb{R}` with

  .. math::
   r_J(\tau) = b_J(\tau) - a_J(\tau,\lambda)\qquad\text{for all}\;\tau\in\Lambda_h

for a given function :math:`\lambda` where :math:`b_J=b` is the 1-form and :math:`a_J=a` is the bilinear form of the original hybridized system in Eq. :eq:`hybridizedeqn` which we wish to solve. We seek the corresponding coarse bilinear form :math:`r_{J-1}:M_{J-1}=P_{1,h}\rightarrow \mathbb{R}`. This can be defined as

  .. math::
     r_{J-1}(w) := r_J(I_Jw) = r_J(w|_{\varepsilon_h}) = b_J(w|_{\varepsilon_h})-a_J(\lambda,w|_{\varepsilon_h})

Hence, to obtain the coarse level residual :math:`r_{J-1}` for a given test function :math:`w\in P_{1,h}`, all we need to do is to evaluate the fine-level residual in trace space for the function :math:`w` restricted to the skeleton :math:`\varepsilon_h`.

In the code we do not have access to the bilinear form :math:`r_J`, but instead computed the components of the vector :math:`R` as :math:`R_j=r_J(\tau_j)` where :math:`\tau_j` are the basis functions of the flux space. To compute the corresponding components of the coarse space vector :math:`R^{(c)}` defined by :math:`R^{(c)}_i=r_{J-1}(w_i)=r_J(w_i|_{\varepsilon_h})` (where :math:`w_i` are the basis functions of :math:`P_{1,h}`) proceed as follows:
Note that since :math:`P_1|_{F}\subset \Lambda_h|_{F}` for all facets :math:`F` (see discussion above) :math:`w_i|_{\varepsilon_h}=\sum_j A_{ij}\tau_j` and hence due to the linearity of :math:`r_J` we have :math:`R^{(c)}=AR`. To compute the matrix :math:`A` write

  .. math::
     B_{ik} = (w_i|_{\varepsilon_h},\tau_k)_{\varepsilon_h} = \sum_j A_{ij}(\tau_j,\tau_k)_{\varepsilon_h} = \sum_{j} A_{ij} M^{(\Lambda_h)}_{jk}\qquad \Leftrightarrow\qquad B = A M^{(\Lambda_h)} 

where :math:`M^{(\Lambda_h)}` is the mass matrix in flux space. Hence we have that

  .. math::
     R^{(c)} = AR = B\left(M^{(\Lambda_h)}\right)^{-1}R

Coarse level equation
^^^^^^^^^^^^^^^^^^^^^
Define a bilinear form on the coarse :math:`P_{1,h}` space by discretising the sign-positive Helmholtz equation.

Eliminating velocity from the time-discretised continuous equations we get
  .. math::
     -\phi^{(t+\Delta t)} + \alpha^2 \Delta \phi^{(t+\Delta t)} = f

The weak form of this in :math:`P_{1,h}` space is
  
  .. math::
     a_{J-1}(u,w) = -(u,w) - \alpha^2\cdot(\nabla u,\nabla w) = r_{J-1}(w)\quad\text{for all}\;w\in P_{1,h}
     :label: coarsegridequation

Two-level algorithm
-------------------
A two-level algorithm for calculate an approximate to the hybridized solution :math:`\lambda` given a right-hand side :math:`b` can be defined as follows:

1. Set :math:`\lambda=0` and pre-smooth in trace-space :math:`\Lambda_h` : :math:`\lambda\mapsto S(\lambda,b;\nu_{\text{presmooth}})`
2. Restrict the residual to the lower-dimensional space :math:`P_{1,h}` by defining :math:`r_{J-1}(w)=r_{J}(w|_{\varepsilon_h})`
3. Solve the coarse level equation :eq:`coarsegridequation` to obtain a correction :math:`u`.
4. Prolongate and add the coarse level solution back to the hybridized space :math:`\lambda\mapsto\lambda+I_Ju=\lambda+u|_{\varepsilon_h}`.
5. Post-smooth in trace-space :math:`\Lambda_h` : :math:`\lambda\mapsto S(b,\lambda;\nu_{\text{postsmooth}})`
