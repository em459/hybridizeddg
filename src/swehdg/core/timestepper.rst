.. _timestepper:
====================
Timestepping methods
====================

Mathematical formulation
------------------------

We assume that the time evolution equation for the dof-vector of unknowns :math:`{q}` is given by

  .. math::
    M {q}_t = L{q} + N({q})

where :math:`M` is the mass matrix and the matrix :math:`L` arises in the
weak discretisation of the linear part of the continuous equations.

If we write :math:`{q}^{(n)}\approx {q}(n\Delta t)` for the approximate solution at time :math:`n\Delta t`, an IMEX timestepping method with
:math:`s` intermediate stages is then given by

  .. math::
     \begin{aligned}
     M {Q}^{(i)} &= M{q}^{(n)} + \Delta t \sum_{j=1}^{i-1} a_{ij} N({Q}^{(j)}) + \Delta t \sum_{j=1}^{i} \tilde{a}_{ij} L {Q}^{(j)}\\
     M {q}^{(n+1)} &= M{q}^{(n)} + \Delta t \sum_{i=1}^{s} b_{i} N({Q}^{(i)}) + \Delta t \sum_{i=1}^{s} \tilde{b}_{i} L {Q}^{(i)}
     \end{aligned}

The method is defined by the :math:`s\times s` matrices :math:`a` and :math:`\tilde{a}` and the :math:`s`-vectors :math:`b` and :math:`\tilde{b}`.

At the :math:`i`-th stage this requires the solution of the linear system
   .. math::
      (M-\Delta t \tilde{a}_{ii}L){Q}^{(i)} = {R}_i := M{q}^{(n)} +
      \Delta t \sum_{j=1}^{i-1} \left(a_{ij} N({Q}^{(j)}) +
      \tilde{a}_{ij} L {Q}^{(j)}\right).

Note that for a purely explicit stage with :math:`\tilde{a}_{ii}=0` this reduces to a mass-solve.

Implementation
--------------
      
This module provides timesteppers for general systems. Those systems are described by classes derived from the abstract base class :class:`timestepper.System`. A derived system class has to provide methods for
the following mathematical operations:

  * **Application** of the **linear operator** :math:`L` to a state :math:`{q}`
  * **Application** of the **non-linear operator** :math:`N` to a state :math:`{q}`
  * **Application** of the **mass matrix** :math:`M` to a state :math:`{q}`
  * **Solution of the shifted linear system** :math:`(M-\alpha L){q}={r}` for
    arbitrary :math:`\alpha`
  * **Mass solve**, i.e. solution of the linear system :math:`M{q}={r}`

In fact, the class has to provide methods which return solvers for the last two
operations.

Specific timesteppers
---------------------
:math:`\theta` method
^^^^^^^^^^^^^^^^^^^^^
The :math:`\theta` -method is given by

  .. math::
     (1-\mu \Delta t L) {q}^{(n+1)} = {q}^{(n)} 
        + \Delta t \left(N({q}^{(n)}) + (1-\mu) Lq^{(n)}\right)

and can be realised as a two-stage IMEX method with

  .. math::
     \begin{pmatrix}a\\\hline b\end{pmatrix}
       = \begin{pmatrix}0 & 0\\ 1 & 0\\\hline 1 & 0\end{pmatrix}\qquad
     \begin{pmatrix}\tilde{a}\\\hline\tilde{b}\end{pmatrix}
       &= \begin{pmatrix}0 & 0\\ 1-\mu & \mu\\\hline 1-\mu & \mu\end{pmatrix}

(see class :class:`timestepper.TimeStepperIMEXTheta`)

ARS methods
^^^^^^^^^^^
The ARS2(2,3,2) and ARS3(4,4,3) methods used in `Kang, Giraldo and Bui-Thanh (2017) <https://arxiv.org/pdf/1711.02751.pdf>`_ are developed in `Ascher, Ruuth, Wetton (1995) <https://epubs.siam.org/doi/abs/10.1137/0732037>`_. The matrices are written down explicitly in `Weller, Lock and Wood (2013) <https://doi.org/10.1016/j.jcp.2013.06.025>`_:

* **ARS2(2,3,2) scheme**:

  .. math::
     \begin{pmatrix}
     a \\\hline b
     \end{pmatrix} =
     \begin{pmatrix}
     0 & 0 & 0\\
     \gamma & 0 & 0\\
     \delta & 1-\delta & 0\\\hline
     0 & 1- \gamma & \gamma
     \end{pmatrix}
     \qquad
     \begin{pmatrix}
     \tilde{a} \\\hline \tilde{b}
     \end{pmatrix} =
     \begin{pmatrix}
     0 & 0 & 0\\
     0 & \gamma & 0\\
     0 & 1- \gamma & \gamma\\\hline
     0 & 1- \gamma & \gamma
     \end{pmatrix}

with :math:`\gamma = 1-\frac{1}{\sqrt{2}}` and :math:`\delta = -\frac{2}{3}\sqrt{2}`

* **ARS3(4,4,3) scheme**:

  .. math::
     \begin{pmatrix}
     a \\\hline b
     \end{pmatrix} =
     \begin{pmatrix}
     0 & 0 & 0 & 0 & 0 \\
     \frac{1}{2} & 0 & 0 & 0 & 0\\
     \frac{11}{18} & \frac{1}{18} & 0 & 0 & 0\\
     \frac{5}{6} & -\frac{5}{6} & \frac{1}{2} & 0 & 0\\
     \frac{1}{4} & \frac{7}{4} & \frac{3}{4} & -\frac{7}{4} & 0\\
     \hline
     \frac{1}{4} & \frac{7}{4} & \frac{3}{4} & -\frac{7}{4} & 0
     \end{pmatrix}
     \qquad
     \begin{pmatrix}
     \tilde{a} \\\hline \tilde{b}
     \end{pmatrix} =
     \begin{pmatrix}
     0 & 0 & 0 & 0 & 0\\
     0 & \frac{1}{2} & 0 & 0 & 0\\
     0 & \frac{1}{6} & \frac{1}{2} & 0 & 0\\
     0 & -\frac{1}{2} & \frac{1}{2} & \frac{1}{2} & 0\\
     0 & \frac{3}{2} & -\frac{3}{2} & \frac{1}{2} & \frac{1}{2}\\
     \hline
     0 & \frac{3}{2} & -\frac{3}{2} & \frac{1}{2} & \frac{1}{2}
     \end{pmatrix}

Class documentation
-------------------

.. automodule:: timestepper
    :members:
    :special-members:
    :private-members:
    :show-inheritance:
