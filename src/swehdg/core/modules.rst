source
======

.. toctree::
   :maxdepth: 4

   shallow_water
   hybridized_multigrid
   timestepper
