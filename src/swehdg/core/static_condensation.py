import firedrake
import ufl
import swehdg.core.transfer_kernels as tk

from firedrake.preconditioners import PCBase
from firedrake.matrix_free.operators import ImplicitMatrixContext
from firedrake.slate.slate import Tensor, AssembledVector
from firedrake.utils import cached_property
from pyop2.profiling import timed_region, timed_function

from firedrake.assemble import create_assembly_callable
from firedrake.function import Function
from firedrake.functionspace import FunctionSpace
from firedrake.ufl_expr import TestFunction, TrialFunction
from firedrake.mg import inject, prolong, restrict
from firedrake.mg.utils import get_level
from firedrake.mg.ufl_utils import coarsen as symbolic_coarsen
from firedrake.variational_solver import (LinearVariationalProblem,
                                          LinearVariationalSolver)

from functools import singledispatch, update_wrapper


def methoddispatch(func):
    dispatcher = singledispatch(func)

    def wrapper(*args, **kwargs):
        return dispatcher.dispatch(args[1].__class__)(*args, **kwargs)

    wrapper.register = dispatcher.register
    update_wrapper(wrapper, func)
    return wrapper


__all__ = ['SCPC']


class SCPC(PCBase):
    """A Slate-based python preconditioner implementation of
    static condensation for three-field problems. This also
    includes the local recovery of the eliminated unknowns.
    """

    def setTraceMonitor(self, monitor):
        self._trace_monitor = monitor

    @timed_function("SCPCInit")
    def initialize(self, pc):
        """Set up the problem context. This takes the incoming
        three-field system and constructs the static
        condensation operators using Slate expressions.

        A KSP is created for the reduced system. The eliminated
        variables are recovered via back-substitution.
        """

        from firedrake.bcs import DirichletBC
        from firedrake.function import Function
        from firedrake.functionspace import FunctionSpace
        from firedrake.interpolation import interpolate

        prefix = pc.getOptionsPrefix() + "trace_"
        A, P = pc.getOperators()
        self.cxt = A.getPythonContext()
        if not isinstance(self.cxt, ImplicitMatrixContext):
            raise ValueError("Context must be an ImplicitMatrixContext")

        # Retrieve the mixed function space
        W = self.cxt.a.arguments()[0].function_space()
        if len(W) != 3:
            raise NotImplementedError("Only supports three function spaces.")

        # Extract trace space
        T = W[2]

        # Need to duplicate a trace space which is NOT
        # associated with a subspace of a mixed space.
        Tr = FunctionSpace(T.mesh(), T.ufl_element())
        bcs = []
        cxt_bcs = self.cxt.row_bcs
        for bc in cxt_bcs:
            assert bc.function_space() == T, (
                "BCs should be imposing vanishing conditions on traces"
            )
            if isinstance(bc.function_arg, Function):
                bc_arg = interpolate(bc.function_arg, Tr)
            else:
                # Constants don't need to be interpolated
                bc_arg = bc.function_arg
            bcs.append(DirichletBC(Tr, bc_arg, bc.sub_domain))

        self._T = T
        self.residual = Function(W)
        self.solution = Function(W)

        # Perform symbolics only once
        S_expr, r_lambda_expr, u_h_expr, q_h_expr = self._slate_expressions_A

        # Find preconditioner expression
        if (not A == P):
            self.cxt_pc = P.getPythonContext()
            S_pc_expr = self._slate_expression_traceP
        else:
            self.cxt_pc = self.cxt
            S_pc_expr = S_expr

        q_h, u_h, lambda_h = self.solution.split()

        trace_lvp = LinearVariationalProblem(
            a=S_expr,
            L=r_lambda_expr,
            u=lambda_h,
            bcs=bcs,
            aP=S_pc_expr,
            form_compiler_parameters=self.cxt.fc_params)

        appctx = self.cxt.appctx
        trace_params = appctx.get("trace_params", {})
        self._alpha = appctx.get("alpha", 0.0)
        self._ksp_monitor = appctx.get("ksp_monitor", None)

        trace_solver = LinearVariationalSolver(
            trace_lvp,
            solver_parameters=trace_params,
            options_prefix=prefix)

        if self._ksp_monitor:
            trace_solver.snes.ksp.setMonitor(self._ksp_monitor)

        # Custom multigrid
        self._use_custom_mg = appctx.get("use_custom_mg", False)
        self._scalar_fs = (Tr.ufl_element().value_shape() == ())
        if (self._scalar_fs):
            coarse_fs = FunctionSpace(self._T.mesh(), "P", 1)
        else:
            coarse_fs = FunctionSpace(self._T.mesh(), "RT", 1)
        if self._use_custom_mg:
            transfer_operators = (
                firedrake.dmhooks.transfer_operators(
                    self._T,
                    restrict=self.my_restrict,
                    inject=self.my_inject),
                # Should do this in coarsen_fs probably, but this works.
                firedrake.dmhooks.transfer_operators(coarse_fs,
                                                     prolong=self.my_prolong))
            trace_solver.set_transfer_operators(*transfer_operators)

        # Inner trace solver
        self.trace_solver = trace_solver

        # Assemble u_h using lambda_h
        self._assemble_u = create_assembly_callable(
            u_h_expr,
            tensor=u_h,
            form_compiler_parameters=self.cxt.fc_params)

        # Recover q_h using both u_h and lambda_h
        self._assemble_q = create_assembly_callable(
            q_h_expr,
            tensor=q_h,
            form_compiler_parameters=self.cxt.fc_params)

    @cached_property
    def _slate_expressions_A(self):
        """Returns all the relevant Slate expressions
        for the static condensation and local recovery
        procedures for the bilinear form used by this PC.
        """

        return self._slate_expressions(self.cxt.a)

    @cached_property
    def _slate_expression_traceP(self):
        """Returns the Slate expression for the
        preconditioner used for the trace system

        """
        S_pc_expr, _, __, ___ = self._slate_expressions(self.cxt_pc.a)
        return S_pc_expr

    def _slate_expressions(self, a_form):
        """Returns all the relevant Slate expressions
        for the static condensation and local recovery
        procedures for a given bilinear form.

        :arg a_form: bilinear form to use
        """
        # This operator has the form:
        # | A  B  C |
        # | D  E  F |
        # | G  H  J |
        # NOTE: It is often the case that D = B.T,
        # G = C.T, H = F.T, and J = 0, but we're not making
        # that assumption here.
        _O = Tensor(a_form)
        Op = _O.blocks

        # Extract sub-block:
        # | A B |
        # | D E |
        # which has block row indices (0, 1) and block
        # column indices (0, 1) as well.
        M = Op[:2, :2]

        # Extract sub-block:
        # | C |
        # | F |
        # which has block row indices (0, 1) and block
        # column indices (2,)
        K = Op[:2, 2]

        # Extract sub-block:
        # | G H |
        # which has block row indices (2,) and block column
        # indices (0, 1)
        L = Op[2, :2]

        # And the final block J has block row-column
        # indices (2, 2)
        J = Op[2, 2]

        # Schur complement for traces
        S = J - L * M.inv * K

        # Create mixed function for residual computation.
        # This projects the non-trace residual bits into
        # the trace space:
        # -L * M.inv * | v1 v2 |^T
        _R = AssembledVector(self.residual)
        R = _R.blocks
        v1v2 = R[:2]
        v3 = R[2]
        r_lambda = v3 - L * M.inv * v1v2

        # Reconstruction expressions
        q_h, u_h, lambda_h = self.solution.split()

        # Local tensors needed for reconstruction
        A = Op[0, 0]
        B = Op[0, 1]
        C = Op[0, 2]
        D = Op[1, 0]
        E = Op[1, 1]
        F = Op[1, 2]
        Se = E - D * A.inv * B
        Sf = F - D * A.inv * C

        v1, v2, v3 = self.residual.split()

        # Solve locally using LU (with partial pivoting)
        u_h_expr = Se.solve(AssembledVector(v2) -
                            D * A.inv * AssembledVector(v1) -
                            Sf * AssembledVector(lambda_h),
                            decomposition="PartialPivLU")

        q_h_expr = A.solve(AssembledVector(v1) -
                           B * AssembledVector(u_h) -
                           C * AssembledVector(lambda_h),
                           decomposition="PartialPivLU")

        return (S, r_lambda, u_h_expr, q_h_expr)

    def update(self, pc):
        # No symbolics to reassemble, this is handled by the
        # inner trace problem this PC is wrapped around.
        return

        self._assemble_Sp()

    def apply(self, pc, x, y):
        """Solve the reduced system for the Lagrange multipliers.
        The system is assembled using operators constructed from
        the Slate expressions in the initialize method of this PC.
        Recovery of the scalar and flux fields are assembled cell-wise
        from Slate expressions describing the local problem.
        """

        with self.residual.dat.vec_wo as v:
            x.copy(v)

        with timed_region("SCPCSolve"):
            if self._use_custom_mg:
                with firedrake.dmhooks.ctx_coarsener(self._T,
                                                     self.my_coarsen):
                    with self._ksp_monitor:
                        self.trace_solver.solve()
            else:
                with self._ksp_monitor:
                    self.trace_solver.solve()

        with timed_region("SCPCBacksub"):
            # Recover eliminated unknowns
            self._assemble_u()
            self._assemble_q()

        with self.solution.dat.vec_ro as w:
            w.copy(y)

    def applyTranspose(self, pc, x, y):
        """Apply the transpose of the preconditioner."""

        raise NotImplementedError("Transpose application is not implemented.")

    def view(self, pc, viewer=None):

        viewer.printfASCII("Hybridized trace preconditioner\n")
        viewer.printfASCII("KSP to solve trace system:\n")
        self.trace_solver.snes.ksp.view(viewer=viewer)

    # Coarsening strategies for our custom multigrid procedure
    @methoddispatch
    def my_coarsen(self, expr, callback, coefficient_mapping=None):
        return symbolic_coarsen(expr,
                                callback,
                                coefficient_mapping=coefficient_mapping)

    @my_coarsen.register(firedrake.functionspaceimpl.FunctionSpace)
    @my_coarsen.register(firedrake.functionspaceimpl.WithGeometry)
    def _coarsen_fs(self, V, callback, coefficient_mapping=None):
        hierarchy, level = get_level(V.ufl_domain())
        if level == len(hierarchy) - 1:
            mesh = callback(V.ufl_domain(), callback)
            if (self._scalar_fs):
                return FunctionSpace(mesh, "P", 1)
            else:
                return FunctionSpace(mesh, "RT", 1)
        else:
            return symbolic_coarsen(V,
                                    callback,
                                    coefficient_mapping=coefficient_mapping)

    @my_coarsen.register(firedrake.LinearVariationalProblem)
    def _coarsen_nlvp(self, problem, callback, coefficient_mapping=None):
        hierarchy, level = get_level(problem.u.ufl_domain())
        if level != len(hierarchy) - 1:
            return symbolic_coarsen(problem,
                                    callback,
                                    coefficient_mapping=coefficient_mapping)

        if coefficient_mapping is None:
            coefficient_mapping = {}

        V = callback(problem.u.function_space(), callback)
        uh = Function(V)

        beta = firedrake.Constant(self._alpha**2)

        u = TrialFunction(V)
        v = TestFunction(V)
        if (self._scalar_fs):
            op = ufl.grad
        else:
            op = ufl.div
        a = -(ufl.inner(u, v)*ufl.dx +
              beta*ufl.inner(op(u),op(v))*ufl.dx)
        return LinearVariationalProblem(a, 0, uh)

    # Transfer operators for the custom multigrid procedure
    @staticmethod
    def my_restrict(fine, coarse):
        hierarchy, level = get_level(fine.ufl_domain())
        if level == len(hierarchy) - 1:
            tk.restrict(fine, coarse)
        else:
            restrict(fine, coarse)

    @staticmethod
    def my_inject(fine, coarse):
        hierarchy, level = get_level(fine.ufl_domain())
        if level == len(hierarchy) - 1:
            # TODO: implement injection
            pass
        else:
            inject(fine, coarse)

    @staticmethod
    def my_prolong(coarse, fine):
        hierarchy, level = get_level(fine.ufl_domain())
        if level == len(hierarchy) - 1:
            tk.prolong(coarse, fine)
        else:
            prolong(fine, coarse)
