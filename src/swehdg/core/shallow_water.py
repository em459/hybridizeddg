from firedrake import *
from firedrake.utils import cached_property
from firedrake import NonNestedHierarchy
from pyop2.profiling import timed_function
import math
from swehdg.core.ksp_monitor import *
from swehdg.core.timestepper import *
from swehdg.core.solver_wrapper import WrappedSolver
from swehdg.core.transfer_kernels import prolongation_matrix

class ShallowWaterDGParam(object):
    '''Class for holding constant parameters about the Earth
    Allows retrieving parameters without instantiating rest of
    shallow water object
    '''
    def __init__(self):
        # Radius of Earth = 6.370E6 [m]
        self._Rearth = 6.370E6

        # Gravitational acceleration g_grav = 9.81 [ms^{-2}]
        self._g_grav = 9.81

        # Depth of fluid = 2.0E3 [m]
        self._H = 2.0E3

        # Reference time = 1 day = 24*3600 [s]
        day = 24*3600
        self._day = day
        self._Tref = day
        self._cg = self._Tref / self._Rearth * math.sqrt(self._g_grav*self._H)

        # angular frequency of Earth rotation
        self._Omega = (2.0*math.pi/day)*self._Tref

    @property
    def cg(self):
        return self._cg

    @property
    def omega(self):
        return self._Omega


class ShallowWaterDG(ShallowWaterDGParam, System):
    '''Native DG linear shallow water solver with Coriolis force on a unit
    sphere or a biperiodic unit square.

    We use the following parameters
      * reference time :math:`T_{ref} = 1 day = 24\\cdot 3600 s`
      * Earth radius :math:`R_{earth} = 6.370\\cdot10^6 m`
      * gravitational acceleration :math:`g = 9.81 ms^{-2}`
      * water depth :math:`H = 2.0\\cdot 10^3 m`
      * angular frequency of the Earth's rotation :math:`\\Omega = 2\\pi / day`

    :arg geometry: Mesh geometry, can be 'spherical' or 'flat'
    :arg nrefine: Number of refinement steps for spherical icosahedral mesh
    :arg degree: Polynomial degree of DG space
    :arg mesh: Mesh object to use. If this is not passed, then a mesh in
         created in the constructor
    :arg bathymetry: UFL expression for bathymetry. If this is None, a flat
         bathymetry (:math:`\Phi_b=1` is used).
    :arg deltaNL: Solve non-linear equations (deltaNL=1) or linear (deltaNL=0)
         equations
    :arg flux: Numerical flux to use for linear part. Allowed values are
         'upwind' or 'Lax-Friedrichs'
    :arg output_filename: Name of output file (None for no output)
    :arg solver_rtol: Relative tolerance to be used in linear solve
    :arg ksp_verbose: KSP verbosity level
    '''
    def __init__(self,
                 geometry,
                 nrefine,
                 degree,
                 mesh=None,
                 bathymetry=None,
                 deltaNL=0,
                 coriolis=True,
                 flux="upwind",
                 output_filename=None,
                 solver_rtol=1.0E-8,
                 ksp_verbose=0):
        super(ShallowWaterDG, self).__init__()
        # Check geometry for validity
        assert ((geometry == 'spherical') or (geometry == 'flat'))
        self._geometry = geometry
        # quadrature degree (degree will be automatically set if None)
        self._quad_degree = None
        # mesh degree (degree will be automatically set if None)
        self._mesh_degree = None
        self._nrefine = nrefine
        self._degree = degree
        if mesh is None:
            if (self._geometry == 'spherical'):
                if self._mesh_degree is None:
                    self._mesh = UnitIcosahedralSphereMesh(self._nrefine)
                else:
                    self._mesh = UnitIcosahedralSphereMesh(self._nrefine,
                                                           degree=self._mesh_degree)
                self._mesh.init_cell_orientations(SpatialCoordinate(self._mesh))
            else:
                self._mesh = PeriodicUnitSquareMesh(2**self._nrefine,
                                                    2**self._nrefine)
        else:
            self._mesh = mesh

        # Check flux for validity
        assert ((flux == 'upwind') or (flux == 'Lax-Friedrichs'))
        self._flux = flux

        # Set bathymetry
        self._constant_bathymetry = (bathymetry is None)
        if self._constant_bathymetry:
            self._bath = Constant(1.0, domain=self._mesh)
        else:
            self._bath = bathymetry

        self._coriolis = coriolis
        
        # Communicator
        self._comm = self._mesh.comm
        # Construct function spaces
        self._V_phi = FunctionSpace(self._mesh, "DG", self._degree)
        if (self._geometry == 'spherical'):
            Ve = BrokenElement(FiniteElement("BDM",
                                             self._mesh.topological.ufl_cell(),
                                             self._degree))
            self._V_u = FunctionSpace(self._mesh,Ve)
        else:
            self._V_u = VectorFunctionSpace(self._mesh, "DG", self._degree)

        self._V_q = self._V_u * self._V_phi
        self._w, self._psi = TestFunctions(self._V_q)
        # Function for the current state
        self.q = Function(self._V_q, name="State")
        # Function for the state at the previous time-step
        self.qold = Function(self._V_q, name="Old state")
        # Cache for tangential normal
        self._n_T_cache = None
        # Name of output file
        self._output_filename = output_filename
        # Solver tolerance
        self._solver_rtol = solver_rtol
        # Turn off non-linear terms
        self._deltaNL = deltaNL
        # higher order qudrature rules
        if self._quad_degree is None:
            self._dS = dS(domain=self._mesh)
            self._dx = dx(domain=self._mesh)
        else:
            self._dS = dS(degree=self._quad_degree, domain=self._mesh)
            self._dx = dx(degree=self._quad_degree, domain=self._mesh)
        self._w, self._psi = TestFunctions(self._V_q)
        self._ksp_verbose = ksp_verbose
        self.reset_ksp_monitors()

    def reset_ksp_monitors(self):
        '''Reset at beginning of new integration'''
        self._ksp_monitors = []

    @cached_property
    def h(self):
        '''Return grid spacing'''
        if (self._geometry == 'flat'):
            return 0.5**self._nrefine
        else:
            return math.sqrt(2.*math.pi/(5.*math.sqrt(3.)))*0.5**self._nrefine

    @cached_property
    def ncell(self):
        '''Return the number of cells'''
        if (self._geometry == 'flat'):
            return 2**(2*self._nrefine+1)
        else:
            return 20*4**self._nrefine

    @cached_property
    def nfacet(self):
        '''Return the number of facets'''
        return int(3*self.ncell/2)

    @cached_property
    def ndof_per_cell(self):
        '''Return number of unknowns per cell (potential and momentum)'''
        element = self._V_phi.finat_element
        tdim = element.cell.get_dimension()
        ndof_cell = len(element.entity_dofs()[tdim][0])
        return (tdim+1)*ndof_cell

    @property
    def cg(self):
        '''Non-dimensionalised gravity wave speed'''
        return self._cg

    @property
    def V_q(self):
        '''Mixed function space :math:`V_q = V_h^k\\otimes (V_h^k)^2`'''
        return self._V_q

    def V(self):
        '''Return state function space'''
        return self.V_q

    @property
    def mesh(self):
        '''Return underlying mesh'''
        return self._mesh

    def n_T(self, sgn):
        '''Tangential normal vector for a facet.

        In the 2d model with flat geometry this is simply the facet
        normal n('+') or n('-'). In the 3d spherical model this is
        +/- (n_{+} - n_{-}) projected onto the trace space.
        Note that we always return the '+' value, since the '-' value is
        not available to SLATE.

        :arg sgn: Sign, has to be '+' or '-'
        '''
        assert ((sgn == '+') or (sgn == '-'))
        if (sgn == '-'):
            return -self.n_T('+')

        n = FacetNormal(self._mesh)
        if (self._geometry == 'flat'):
            return n('+')
        else:
            if (self._n_T_cache is None):
                V_trace = VectorFunctionSpace(self.mesh, "DGT", self._degree)
                self._n_T_cache = Function(V_trace)
                n_trial = TrialFunction(V_trace)
                n_test = TestFunction(V_trace)
                a = (dot(n_trial('+'), n_test('+')) +
                     dot(n_trial('-'), n_test('-')))*self._dS
                L = (dot(0.5*(n('+') - n('-')), n_test('+')) +
                     dot(0.5*(n('+') - n('-')), n_test('-')))*self._dS
                solve(a == L, self._n_T_cache,
                      solver_parameters={'ksp_type': 'cg',
                                         'pc_type': 'bjacobi',
                                         'sub_pc_type': 'ilu'})
            return self._n_T_cache('+')

    def _linear_upwind(self, u, phi, w=None, psi=None):
        '''Upwinding flux. Return either a one-form or a bilinear form,
        depending on the choice of u, phi.

        :arg u: momentum function or trial-function
        :arg phi: potential function or trial-function
        :arg w: momentum test-function or default test-function :
        :arg psi: potential test-function or default test-function
        '''

        if psi is None:
            psi = self._psi
        if w is None:
            w = self._w

        # Project out tangential part of velocity test function in spherical
        # geometry and use improve n_{+} and n_{-}
        if (self._geometry == 'spherical'):
            X = SpatialCoordinate(self._mesh)
            x, y, z = X
            r_hat = X/sqrt(x*x+y*y+z*z)

            w = w - r_hat*inner(r_hat, w)
            u = u - r_hat*inner(r_hat, u)

        # Define delta across facet
        def delta(a):
            return a('-')-a('+')

        # Define average across facet
        def avg(a):
            return 0.5*(a('-')+a('+'))

        jump_psi = psi('-')*self.n_T('-') + psi('+')*self.n_T('+')
        jump_w = inner(w('-'), self.n_T('-')) + inner(w('+'), self.n_T('+'))
        jump_u = inner(u('-'), self.n_T('-')) + inner(u('+'), self.n_T('+'))

        flux_phi = (inner(jump_psi, avg(u))*self._dS +
                    0.5*delta(psi)*sqrt(self._bath)*delta(phi)*self._dS)
        flux_u = (jump_w*avg(self._bath*phi)*self._dS
                  + 0.5*jump_w*sqrt(self._bath)*jump_u*self._dS)

        weakderivative = flux_phi + flux_u

        if (self._degree > 0):
            # phi
            weakderivative += -inner(grad(psi), u)*self._dx
            # u
            weakderivative += -inner(self._bath*phi, div(w))*self._dx

        return weakderivative

    def _linear_lax(self, u, phi, w=None, psi=None):
        '''Linear Lax-Friedrichs flux. Return either a one-form or a
        bilinear form, depending on the choice of u, phi.

        :arg u: momentum function or trial-function
        :arg phi: potential function or trial-function
        :arg w: momentum test-function or default test-function
        :arg psi: potential test-function or default test-function
        '''
        if psi is None:
            psi = self._psi
        if w is None:
            w = self._w

        # Project out tangential part of velocity test function in spherical
        # geometry and use improve n_{+} and n_{-}
        if (self._geometry == 'spherical'):
            X = SpatialCoordinate(self._mesh)
            x, y, z = X
            r_hat = X/sqrt(x*x+y*y+z*z)

            w = w - r_hat*inner(r_hat, w)
            u = u - r_hat*inner(r_hat, u)

        # Stability parameter
        tau = sqrt(self._bath('+'))

        # Flux part
        n = self.n_T
        b = self._bath
        flux_u = 0.5*(dot((b('+')*phi('+') + b('-')*phi('-'))*n('+'), w('+'))
                      + tau*dot((u('+')*dot(n('+'), n('+')) + u('-')*dot(n('-'), n('+'))), w('+'))
                      + dot((b('+')*phi('+') + b('-')*phi('-'))*n('-'), w('-'))
                      + tau*dot((u('+')*dot(n('+'), n('-')) + u('-')*dot(n('-'), n('-'))), w('-')))*self._dS

        flux_phi = 0.5*(dot((u('+') + u('-')), n('+'))*psi('+')
                        + tau*dot((phi('+')*dot(n('+'), n('+')) + phi('-')*dot(n('-'), n('+'))), psi('+'))
                        + dot((u('+') + u('-')), n('-'))*psi('-')
                        + tau*dot((phi('+')*dot(n('+'), n('-')) + phi('-')*dot(n('-'), n('-'))), psi('-')))*self._dS

        weakderivative = flux_phi + flux_u

        if (self._degree > 0):
            # phi
            weakderivative += -inner(grad(psi), u)*self._dx
            # u
            weakderivative += -inner(self._bath*phi, div(w))*self._dx

        return weakderivative

    @timed_function("EvalDGWeakDerivative")
    def _resL(self, u, phi, w=None, psi=None, name=None):
        '''Given :math:`(\\vec{u},\\phi)`, build the weak derivative
        :math:`S(\\vec{w},\\psi;\\vec{u},\\phi)` where :math:`\\psi` and
        :math:`\\vec{w}` are test-functions. Depending on the value of
        name, this calls _linear_upwind() or _linear_lax().
        Return either a one-form or a bilinear form, depending on the choice
        of u, phi.

        :arg u: velocity state function or trial-function :math:`\\vec{u}`
        :arg phi: potential function or trial-function :math:`\\phi`
        :arg w: testfunction (self._w if None)
        :arg psi: testfunction (self._psi if None)
        :arg name: Type of weak derivative to use ('upwind' or
        ''Lax-Friedrichs')
        '''
        if name is None:
            name = self._flux

        # This function is now just a wrapper for a choice of flux
        if name == 'upwind':
            weakderivative = self._linear_upwind(u, phi, w, psi)
        if name == 'Lax-Friedrichs':
            weakderivative = self._linear_lax(u, phi, w, psi)

        return -self._cg*weakderivative

    def resL(self, q):
        '''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        return self._resL(u, phi)

    def resLnative(self, q):
        '''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.
        This always uses the non-hybridized version.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        # Do NOT use self._w, self._psi, since they will not be in
        # the correct function space for the derived HDG class!
        w, psi = TestFunctions(self._V_q)
        return self._resL(u, phi, w, psi)

    def _full_nonlinear_form(self, u, phi, w=None, psi=None):
        '''Returns the nonlinear form :math:`N(p,q)`
        Return either a one-form or a bilinear form, depending on the choice
        of u, phi.

        :arg u: momentum function or trial-function
        :arg phi: potential function or trial-function
        :arg w: momentum test-function or default test-function
        :arg psi: potential test-function or default test-function
        '''

        if psi is None:
            psi = self._psi
        if w is None:
            w = self._w

        if (self._geometry == 'flat'):
            # Top 2x2 entries of F (the bottom 1x2 matrix is just U^T)
            Fnl = as_matrix([[u[0]**2/(phi + self._bath) + 0.5*phi**2 + self._bath*phi, u[0]*u[1]/(phi+self._bath)],
                            [u[0]*u[1]/(phi + self._bath), u[1]**2/(phi + self._bath) + 0.5*phi**2 + self._bath*phi]])

        else:
            X = SpatialCoordinate(self._mesh)
            x, y, z = X
            r_hat = X/sqrt(x*x+y*y+z*z)

            w = w - r_hat*inner(r_hat, w)
            u = u - r_hat*inner(r_hat, u)

            Fnl = as_matrix([[u[0]**2/(phi + self._bath) + 0.5*phi**2 + self._bath*phi, u[0]*u[1]/(phi+self._bath),u[0]*u[2]/(phi+self._bath)],
                             [u[0]*u[1]/(phi + self._bath), u[1]**2/(phi + self._bath) + 0.5*phi**2 + self._bath*phi,u[2]*u[1]/(phi+self._bath)],
                             [u[0]*u[2]/(phi + self._bath),  u[2]*u[1]/(phi+self._bath), u[2]**2/(phi + self._bath) + 0.5*phi**2 + self._bath*phi]])
        
        tau = Max(abs(inner(u('+'), self.n_T('+'))) +
                  sqrt(phi('+') + self._bath('+')),
                  abs(inner(u('-'), self.n_T('-'))) +
                  sqrt(phi('-') + self._bath('-')))

        # Weak derivative
        # U part
        a1 = -inner(Fnl, grad(w))*self._dx
        # phi part
        a2 = -inner(u, grad(psi))*self._dx

        # Flux
        n = self.n_T
        # U part
        a3 = 0.5*(dot((Fnl('+') + Fnl('-'))*n('+'), w('+'))
                  + tau*dot((u('+')*dot(n('+'), n('+')) + u('-')*dot(n('-'), n('+'))), w('+'))
                  + dot((Fnl('+') + Fnl('-'))*n('-'), w('-'))
                  + tau*dot((u('+')*dot(n('+'), n('-')) + u('-')*dot(n('-'), n('-'))), w('-'))
                  )*self._dS
        # phi part
        a4 = 0.5*(dot(u('+') + u('-'), n('+'))*psi('+')
                  + tau*dot((phi('+')*dot(n('+'), n('+')) + phi('-')*dot(n('-'), n('+'))), psi('+'))
                  + dot(u('+') + u('-'), n('-'))*psi('-')
                  + tau*dot((phi('+')*dot(n('+'), n('-')) + phi('-')*dot(n('-'), n('-'))), psi('-'))
                  )*self._dS

        fullnonlinear = a1 + a2 + a3 + a4
        return fullnonlinear

    def _resN(self, u, phi):
        '''Calculate the slow part of the operator, i.e.
        :math:`N(u,\\phi)-L(u,\\phi)` for given momentum and potential
        functions :math:`u` and :math:`\\phi'. This is an internal method
        which gets called by resN().

        :math u: Momentum function :math:`u`
        :math phi: Potential function :math:`\\phi`
        '''
        if (self._coriolis):
            if (self._geometry == 'spherical'):
                X = SpatialCoordinate(self._mesh)
                x, y, z = X
                r_hat = X/sqrt(x*x+y*y+z*z)
                coriolis_integral = z*dot(self._w, cross(r_hat, u))*self._dx
            else:
                # f-plane approximation at 90 degrees of latitude : math.sin(math.pi/2)==1
                coriolis_integral = dot(self._w, perp(u))*self._dx

            coriolis_integral = -2.0*self._Omega*coriolis_integral
        else:
            coriolis_integral = 0
        # Most general way of handling flux
        nonlinear_integral = -self._cg*self._full_nonlinear_form(u, phi) - self._resL(u, phi)

        bathymetry_integral = (-self._bath*(inner(self._w, grad(phi)) + phi*div(self._w))*self._dx
                               + inner(self._w('+'), self.n_T('+'))*self._bath*phi('+')*self._dS
                               + inner(self._w('-'), self.n_T('-'))*self._bath*phi('-')*self._dS)

        return self._deltaNL*nonlinear_integral + coriolis_integral + self._cg*bathymetry_integral

    def resN(self, q):
        '''Calculate the slow part of the operator, i.e.
        :math:`N(q)-L(q)` for a given state :math:`q=(u,\\phi).`
        This is an outward facing method which gets called for example by the
        timestpper algorithm. It calls the internal method _resN().

        :math q: State function :math:`q=(u,\\phi)`
        '''
        u, phi = q.split()
        return self._resN(u, phi)

    def _resM(self, u, phi):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        return (self._psi*phi + inner(u, self._w))*self._dx

    def resM(self, q):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        return self._resM(u, phi)

    def resMnative(self, q):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.
        This always uses the non-hybridized version.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        w, psi = TestFunctions(self._V_q)
        return (psi*phi + inner(u, w))*self._dx

    def linear_solver(self, r, q, alpha):
        '''Solver for the linear system :math:`(M-\\alpha L)q=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        :arg alpha: scaling parameter
        '''
        # Set up solver
        solver_param = {'ksp_type': 'gmres',
                        'ksp_rtol': self._solver_rtol,
                        'pc_type': 'fieldsplit',
                        'pc_fieldsplit_type': 'schur',
                        'pc_fieldsplit_schur_fact_type': 'FULL',
                        'pc_fieldsplit_schur_precondition': 'selfp',
                        'fieldsplit_0': {'ksp_type': 'preonly',
                                         'pc_type': 'bjacobi',
                                         'sub_pc_type': 'ilu'},
                        'fieldsplit_1': {'ksp_type': 'preonly',
                                         'pc_type': 'gamg',
                                         'pc_mg_log': None,
                                         'mg_levels': {'ksp_type': 'chebyshev',
                                                       'ksp_max_it': 2,
                                                       'pc_type': 'bjacobi',
                                                       'sub_pc_type': 'sor'}}}
        # Initialize linear solver
        u, phi = TrialFunctions(self._V_q)
        mass_integral = self._resM(u, phi)
        S = self._resL(u, phi)
        bilinear = mass_integral - alpha*S
        lvp = LinearVariationalProblem(bilinear, r, q)
        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=solver_param)
        ksp_label = 'nativeDG_stage_'+str(len(self._ksp_monitors))
        ksp_monitor = KSPMonitor(label=ksp_label,
                                 comm=self._mesh.comm,
                                 verbose=self._ksp_verbose)
        self._ksp_monitors.append(ksp_monitor)

        lvs.snes.ksp.setMonitor(self._ksp_monitors[-1])

        return WrappedSolver(lvs, self._ksp_monitors[-1], "DG Linear Solve")

    def mass_solver(self, r, q):
        '''Solver for the mass matrix :math:`Mq=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        '''
        u, phi = TrialFunctions(self._V_q)
        # Solve using LU factorisation on the diagonal blocks
        mass_solver_param = {'ksp_type': 'preonly',
                             'pc_type': 'fieldsplit',
                             'pc_fieldsplit_type': 'additive',
                             'fieldsplit': {'ksp_type': 'preonly',
                                            'pc_type': 'bjacobi',
                                            'sub_pc_type': 'lu'}}

        S = self._resM(u, phi)
        lvp = LinearVariationalProblem(S, r, q)
        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=mass_solver_param)

        ksp_label = 'mass_solve'
        ksp_monitor = KSPMonitor(label=ksp_label,
                                 comm=self._mesh.comm,
                                 verbose=self._ksp_verbose)
        self._ksp_monitors.append(ksp_monitor)

        return WrappedSolver(lvs, self._ksp_monitors[-1], 'DG Mass Solve')


class ShallowWaterHybridizedDG(ShallowWaterDG):
    '''Hybridized DG discretisation of shallow water solver.

    :arg geometry: Mesh geometry, can be 'spherical' or 'flat'
    :arg nrefine: Number of refinement steps for spherical icosahedral mesh
    :arg degree: Polynomial degree of DG space
    :arg mesh: Mesh object to use. If this is not passed, then a mesh in
         created in the constructor
    :arg bathymetry: UFL expression for bathymetry. If this is None, a flat
         bathymetry (:math:`\Phi_b=1` is used).
    :arg prec_const_bathymetry: Use constant bathymetry (:math:`\Phi_b=1`) in
         the preconditioner for the flux system. This is really only important
         if the hybrid geometric multigrid preconditioner is used, since the
         hybrid Schur-complement is only symmetric is :math:`\Phi_b=1`.
    :arg deltaNL: Solve non-linear equations (deltaNL=1) or linear (deltaNL=0)
         equations
    :arg flux: Numerical flux to use for linear part. Allowed values are
         'upwind' or 'Lax-Friedrichs'
    :arg output_filename: Name of output file (None for no output)
    :arg solver_rtol: Relative tolerance to be used in linear solve
    :arg use_gtmg: Turns on non-nested multigrid on the trace space solve.
    :arg mg_type: Use AMG or GMG on coarse solve
    :arg ksp_verbose: KSP verbosity level
    '''

    def __init__(self, geometry,
                 nrefine,
                 degree,
                 mesh=None,
                 bathymetry=None,
                 prec_const_bathymetry=True,
                 deltaNL=0,
                 coriolis=True,
                 flux='upwind',
                 output_filename=None,
                 solver_rtol=1.0E-8,
                 use_gtmg=False,
                 mg_type='AMG',
                 ksp_verbose=0):

        super(ShallowWaterHybridizedDG, self).__init__(geometry,
                                                       nrefine,
                                                       degree,
                                                       mesh,
                                                       bathymetry,
                                                       deltaNL,
                                                       coriolis,
                                                       flux,
                                                       output_filename,
                                                       solver_rtol,
                                                       ksp_verbose)
        self._prec_const_bathymetry = prec_const_bathymetry
        if self._flux == 'upwind':
            self._V_flux = FunctionSpace(self.mesh, "HDiv Trace", self._degree)
        if self._flux == 'Lax-Friedrichs':
            self._V_flux = VectorFunctionSpace(self.mesh, "HDiv Trace", self._degree)
        self._V_q_hyb = self._V_u * self._V_phi * self._V_flux
        self._use_gtmg = use_gtmg
        self._mg_type = mg_type
        self.lambda_h = Function(self._V_flux, name="Lagrange multiplier")
        self._w, self._psi, self._tau = TestFunctions(self._V_q_hyb)

    def V(self):
        '''Return state function space'''
        return self._V_q_hyb

    @cached_property
    def ndof_per_facet(self):
        '''Return number of flux unknowns per facet'''
        element = self._V_flux.finat_element
        tdim = element.cell.get_dimension()
        ndof_facet = len(element.entity_dofs()[tdim-1][0])
        return ndof_facet

    def _scalar_flux_condition(self, u, phi, lmbda, bathymetry=None):
        '''Given :math:`(\\vec{u},\\phi,\\lambda)`, build the weak form
        which weakly enforces the flux condition. If bathymetry
        is provided, the form is evaluated with this, otherwise
        the bathymetry stored in the class is used.

        :arg u: velocity function or trial-function :math:`\\vec{u}`
        :arg phi: potential function or trial-function :math:`\\phi`
        :arg lmbda: flux function or trial-function :math:`\\lambda`
        :arg bathymetry: Bathymetry to use
        '''
        if bathymetry is None:
            bath = self._bath
        else:
            bath = bathymetry

        def jump_n(a):
            return inner(a('+'), self.n_T('+')) + inner(a('-'), self.n_T('-'))

        def both(a):
            return 2*avg(a)

        return -self._tau('+') * (jump_n(u) + sqrt(bath)*both(phi) - sqrt(bath)*both(lmbda))*self._dS

    def _vector_flux_condition(self, u, phi, lmbda, bathymetry=None):
        '''Given :math:`(\\vec{u},\\phi,\\lambda)`, build the weak form
        which weakly enforces the flux condition. If a bathymetry
        is provided, the form is evaluated with this, otherwise
        the bathymetry stored in the class is used.

        :arg u: velocity function or trial-function :math:`\\vec{u}`
        :arg phi: potential function or trial-function :math:`\\phi`
        :arg lmbda: flux function or trial-function :math:`\\lambda`
        :arg bathymetry: bathymetry to use
        '''
        if bathymetry is None:
            bath = self._bath
        else:
            bath = bathymetry

        n = self.n_T
        fluxcondition = -inner(self._tau('+'),
                              bath*phi('-')*n('-') + bath*phi('+')*n('+')
                              + sqrt(bath)*(u('-') + u('+'))
                              - sqrt(bath)*lmbda('+')
                              - sqrt(bath)*lmbda('-'))*self._dS
        return fluxcondition

    def _hybridized_linear_upwind(self, u, phi, lmbda, bathymetry=None):
        '''Hybridized upwinding flux, returns a one-form. If constant
        bathymetry is provided, the form is evaluated with this,
        otherwise the bathymetry stored in the class is used.

        :arg u: momentum function
        :arg phi: potential function
        :arg lmbda: flux function
        :arg bathymetry: bathymetry to use
        '''
        if bathymetry is None:
            bath = self._bath
        else:
            bath = bathymetry

        w, psi, tau = TestFunctions(self._V_q_hyb)

        # Project out tangential part of velocity test function in spherical
        # geometry and use improve n_{+} and n_{-}
        if (self._geometry == 'spherical'):
            X = SpatialCoordinate(self._mesh)
            x, y, z = X
            r_hat = X/sqrt(x*x+y*y+z*z)

            w = w - r_hat*inner(r_hat, w)
            u = u - r_hat*inner(r_hat, u)

        lmbda = lmbda('+')

        def both(arg):
            return 2*avg(arg)

        def jump_n(a):
            return inner(a('+'), self.n_T('+')) + inner(a('-'), self.n_T('-'))

        tmp_phi = (psi('+')*dot(u('+'), self.n_T('+'))*self._dS +
                   psi('-')*dot(u('-'), self.n_T('-'))*self._dS +
                   psi('+')*sqrt(bath)*phi('+')*self._dS +
                   psi('-')*sqrt(bath)*phi('-')*self._dS -
                   both(psi)*sqrt(bath)*lmbda*self._dS)

        tmp_u = (bath*jump_n(w)*lmbda*self._dS)

        weakderivative = tmp_phi + tmp_u

        if (self._degree > 0):
            weakderivative += -inner(grad(psi), u)*self._dx
            weakderivative += -(div(w)*bath*phi)*self._dx

        # Set the flux condition
        self._flux_condition = self._scalar_flux_condition

        return weakderivative

    def _hybridized_linear_lax(self, u, phi, lmbda, bathymetry=None):
        '''Hybridized Lax-Friedrichs flux, returns a one-form. If a
        bathymetry is provided, the form is evaluated with this,
        otherwise the bathymetry stored in the class is used.

        :arg u: momentum function
        :arg phi: potential function
        :arg lmbda: flux function
        :arg bathymetry: bathymetry to use
        '''
        if bathymetry is None:
            bath = self._bath
        else:
            bath = bathymetry
        w, psi, tau = TestFunctions(self._V_q_hyb)

        # Project out tangential part of velocity test function in spherical
        # geometry and use improve n_{+} and n_{-}
        if (self._geometry == 'spherical'):
            X = SpatialCoordinate(self._mesh)
            x, y, z = X
            r_hat = X/sqrt(x*x+y*y+z*z)

            w = w - r_hat*inner(r_hat, w)
            u = u - r_hat*inner(r_hat, u)

        lmbda = lmbda('+')
        n = self.n_T

        tmp_phi = (inner(lmbda, n('+'))*psi('+')
                   + inner(lmbda, n('-'))*psi('-'))*self._dS

        tmp_u = (bath*phi('+')*inner(n('+'), w('+'))
                 + bath*phi('-')*inner(n('-'), w('-'))
                 + sqrt(bath)*inner(u('+'), w('+'))
                 + sqrt(bath)*inner(u('-'), w('-'))
                 - sqrt(bath)*inner(lmbda, w('+'))
                 - sqrt(bath)*inner(lmbda, w('-')))*self._dS

        weakderivative = tmp_phi + tmp_u

        if (self._degree > 0):
            weakderivative += -inner(grad(psi), u)*self._dx
            weakderivative += -(div(w)*bath*phi)*self._dx

        # Set the flux condition
        self._flux_condition = self._vector_flux_condition

        return weakderivative

    def _hybridized_resL(self, u, phi, lmbda, bathymetry=None, name=None):
        '''Given :math:`(\\vec{u},\\phi,\\lambda)`, build the weak derivative
        :math:`S^H(\\vec{w},\\psi;\\vec{u},\\phi,\\lambda)` where
        :math:`\\psi` and :math:`\\vec{w}` are test-functions.
        Note that this method only tests against potential and velocity
        test functions- the corresponding expression for the flux test
        function is given in the method _flux_condition().
        Depending on the value of the parameter name, this calls
        _hybridized_linear_upwind() or _hybridized_linear_lax().

        :arg u: velocity function or trial-function :math:`\\vec{u}`
        :arg phi: potential function or trial-function :math:`\\phi`
        :arg lmbda: flux function or trial-function :math:`\\lambda`
        :arg bathymetry: bathymetry to use
        :arg name: flux to use (upwind or Lax-Friedrichs)
        '''
        if name is None:
            name = self._flux

        if name == 'upwind':
            weakderivative = self._hybridized_linear_upwind(u, phi, lmbda,
                                                            bathymetry)
        if name == 'Lax-Friedrichs':
            weakderivative = self._hybridized_linear_lax(u, phi, lmbda,
                                                         bathymetry)

        return -self._cg*weakderivative

    def resL(self, q):
        '''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi, lmbda = q.split()
        return self._hybridized_resL(u, phi, lmbda)

    def resLnative(self, q):
        '''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.
        This always uses the non-hybridized version, and calls the method
        from the base class if necessary.

        :arg q: state to apply to
        '''
        try:
            u, phi, _ = q.split()
        except:
            return super().resLnative(q)
        return self._resL(u, phi, self._w, self._psi)

    def resN(self, q):
        '''Calculate the slow part of the operator, i.e.
        :math:`N(q)-L(q)` for a given state :math:`q=(u,\\phi).`
        This is an outward facing method which gets called for example by the
        timestpper algorithm. It calls the internal method _resN().

        :math q: State function :math:`q=(u,\\phi)`
        '''
        u, phi, lmbda = q.split()
        return self._resN(u, phi)

    def resM(self, q):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi, lmbda = q.split()
        return self._resM(u, phi)

    def linear_solver(self, r, q, alpha):
        '''Solver for the linear system :math:`(M-\\alpha L)q=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        :arg lmbda: flux part of solution
        :arg alpha: scaling parameter
        '''

        u_, phi_, lmbda_ = TrialFunctions(self._V_q_hyb)
        mass_integral = self._resM(u_, phi_)
        weak_derivative = self._hybridized_resL(u_, phi_, lmbda_)
        flux_condition = self._cg*self._flux_condition(u_, phi_, lmbda_)
        bilinear = mass_integral - alpha*weak_derivative + alpha*flux_condition

        # We only need to explicitly use the constant-bathymetry preconditioner
        # if the preconditioner is not already constant
        if (self._prec_const_bathymetry) and (not (self._constant_bathymetry)):
            constant_bath = Constant(1.0, domain=self._mesh)
            weak_derivative_P = self._hybridized_resL(u_, phi_, lmbda_,
                                                      bathymetry=constant_bath)
            flux_condition_P = self._cg*self._flux_condition(u_, phi_, lmbda_,
                                                             bathymetry=constant_bath)
            bilinear_P = mass_integral - alpha*weak_derivative_P + alpha*flux_condition_P
        else:
            bilinear_P = bilinear

        lvp = LinearVariationalProblem(bilinear, r, q,
                                       aP=bilinear_P)

        # HDG solver options:
        # We can use CG if the bathymetry is constant, since the flux system
        # is symmetric in this case.
        if (self._constant_bathymetry):
            inner_ksp_type = 'cg'
        else:
            inner_ksp_type = 'gmres'

        # Geometric Multigrid on coarse space depends on choice of flux
        if self._mg_type == 'GMG':
            # Set parameters for coarse level (P1 or RT1 solver)
            if self._flux == 'upwind':
                # We can just use standard P1 multigrid for the upwind flux
                coarse_param = {
                    'ksp_type': 'preonly',
                    'ksp_rtol': self._solver_rtol,
                    'pc_type': 'mg',
                    'pc_mg_levels':2,
                    'pc_mg_cycles': 'v',
                    'mg_levels': {'ksp_type': 'chebyshev',
                                  'ksp_max_it': 2,
                                  'pc_type': 'bjacobi',
                                  'sub_pc_type': 'sor'},
                    'mg_coarse': {'ksp_type': 'chebyshev',
                                  'ksp_max_it': 2,
                                  'pc_type': 'bjacobi',
                                  'sub_pc_type': 'sor'}}
            elif self._flux == 'Lax-Friedrichs':
                # Use multigrid with path smoother to solve the HDiv problem
                # for Lax-Friedrichs flux
                print('Lax-Friedrich + GTMG + GMG will probably fail')
                coarse_param = {
                    'mat_type':'aij',
                    'ksp_type':'cg',
                    'ksp_monitor_true_residual':None,
                    'ksp_rtol':self._solver_rtol,
                    'pc_type': 'mg',
                    'pc_mg_type': 'full',
                    'pc_mg_cycles': 'v',
                    'mg_levels_ksp_type': 'richardson',
                    'mg_levels_ksp_norm_type': 'unpreconditioned',
                    'mg_levels_ksp_richardson_scale': 0.5,
                    'mg_levels_ksp_max_it': 1,
                    'mg_levels_ksp_convergence_test': 'skip',
                    'mg_levels_pc_type': 'python',
                    'mg_levels_pc_python_type': 'firedrake.PatchPC',
                    'mg_levels_patch_pc_patch_save_operators': True,
                    'mg_levels_patch_pc_patch_partition_of_unity': False,
                    'mg_levels_patch_pc_patch_construct_type': 'star',
                    'mg_levels_patch_pc_patch_construct_dim': 0,
                    'mg_levels_patch_pc_patch_sub_mat_type': 'seqdense',
                    'mg_levels_patch_pc_patch_precompute_element_tensors': True,
                    'mg_levels_patch_sub_ksp_type': 'preonly',
                    'mg_levels_patch_sub_pc_type': 'lu',
                    'mg_coarse_pc_type': 'python',
                    'mg_coarse_pc_python_type': 'firedrake.AssembledPC',
                    'mg_coarse_assembled_pc_type': 'lu',
                    'mg_coarse_assembled_pc_factor_mat_solver_type': 'mumps'}
        # Algebraic Multigrid paramters can be used for either flux
        elif self._mg_type == 'AMG':
            coarse_param = {
                'ksp_type': 'preonly',
                'ksp_rtol': self._solver_rtol,
                'pc_type': 'gamg',
                'pc_mg_cycles': 'v',
                'mg_levels': {'ksp_type': 'chebyshev',
                              'ksp_max_it': 2,
                              'pc_type': 'bjacobi',
                              'sub_pc_type': 'sor'},
                'mg_coarse': {'ksp_type':'chebyshev',
                              'ksp_max_it': 2,
                              'pc_type': 'bjacobi',
                              'sub_pc_type': 'sor'}}
        else:
            print('Error: mg_type', self._mg_type, 'not supported')

        if self._use_gtmg:
            # GMG inner parameters
            inner_params = {
                'mat_type': 'aij',
                'ksp_type': inner_ksp_type,
                'ksp_rtol': self._solver_rtol,
                'pc_type': 'python',
                'pc_python_type': 'firedrake.GTMGPC',
                'gt': {'mat_type': 'aij',
                       'pc_mg_log': None,
                       'mg_levels': {'ksp_type': 'chebyshev',
                                     'ksp_max_it': 2,
                                     'pc_type': 'bjacobi',
                                     'sub_pc_type':'sor'},
                       'mg_coarse': coarse_param}}
        else:
            # AMG inner parameters
            inner_params = {
                'mat_type': 'aij',
                'ksp_type': inner_ksp_type,
                'ksp_rtol': self._solver_rtol,
                'pc_type': 'gamg',
                'pc_mg_log': None,
                'mg_levels': {'ksp_type': 'chebyshev',
                              'ksp_max_it': 2,
                              'pc_type': 'bjacobi',
                              'sub_pc_type': 'sor'}
                }


        # Use inbuilt hybridisation PC
        params = {'mat_type': 'matfree',
                  'ksp_type': 'preonly',
                  'pc_type': 'python',
                  'pc_python_type': 'firedrake.SCPC',
                  'pc_sc_eliminate_fields': '0, 1',
                  'condensed_field': inner_params}

        # Put monitor on solver
        ksp_label = 'hybridizedDG_stage_'+str(len(self._ksp_monitors))

        trace_ksp_label = ksp_label + '_trace'
        trace_ksp_monitor = KSPMonitor(label=trace_ksp_label,
                                         comm=self._mesh.comm,
                                         verbose=self._ksp_verbose)
        self._ksp_monitors.append(trace_ksp_monitor)

        if self._flux == 'upwind':
            def get_coarse_space():
                return FunctionSpace(self._mesh, 'CG', 1)
            def coarse_callback():
                P1 = get_coarse_space()
                q = TrialFunction(P1)
                r = TestFunction(P1)
                beta = Constant(alpha**2)
                return inner(q, r)*dx + beta*inner(grad(q), grad(r))*dx
        elif self._flux == 'Lax-Friedrichs':
            def get_coarse_space():
                return FunctionSpace(self._mesh, 'RT', 1)
            def coarse_callback():
                RT1 = get_coarse_space()
                q = TrialFunction(RT1)
                r = TestFunction(RT1)
                beta = Constant(alpha**2)
                return inner(q, r)*dx + beta*inner(div(q), div(r))*dx

        # Construct interpolation matrix
        interpolation_matrix = prolongation_matrix(self._V_flux,
                                                   get_coarse_space())

        # Set appctx to be passed to solver
        appctx = {'get_coarse_operator': coarse_callback,
                  'get_coarse_space': get_coarse_space,
                  'interpolation_matrix': interpolation_matrix}

        lvs = LinearVariationalSolver(
            lvp,
            options_prefix='hdg_implicit_solver',
            solver_parameters=params,
            appctx=appctx
        )

        # Call solve to initialise the condensed problem
        # There may be a better way
        lvs.solve()
        trace_ctx = lvs.snes.ksp.pc.getPythonContext()
        trace_ctx.condensed_ksp.setMonitor(self._ksp_monitors[-1])

        return WrappedSolver(lvs, self._ksp_monitors[-1], 'HDG Linear Solve')

    def mass_solver(self, r, q):
        '''Solve the mass problem :math:`(\\vec{u},\\vec{w})_K + (\\psi,\\phi)_K = r(\\vec{w},\\vec{\\psi})` constrained by the flux condition to obtain the
        state :math:`q=(\\vec{u},\\phi,\\lambda)`.

        :arg r: right hand side (one-form)
        :arg q: resulting state :math:`(\\vec{u},\\psi,\\lambda)`
        '''
        u, phi, lmbda = TrialFunctions(self._V_q_hyb)
        S_mass = self._resM(u, phi)

        lvp = LinearVariationalProblem(S_mass, r, q)

        mass_solver_param = {'ksp_type': 'preonly',
                             'pc_type': 'fieldsplit',
                             'pc_fieldsplit_type': 'additive',
                             'fieldsplit_0': {'ksp_type': 'preonly',
                                              'pc_type': 'bjacobi',
                                              'sub_pc_type': 'lu'},
                             'fieldsplit_1': {'ksp_type': 'preonly',
                                              'pc_type': 'bjacobi',
                                              'sub_pc_type': 'lu'},
                             'fieldsplit_2': {'ksp_type': 'preonly',
                                              'pc_type': 'none'}}

        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=mass_solver_param)

        ksp_label = 'mass_solve'
        ksp_monitor = KSPMonitor(label=ksp_label,
                                 comm=self._mesh.comm,
                                 verbose=self._ksp_verbose)
        self._ksp_monitors.append(ksp_monitor)

        return WrappedSolver(lvs, self._ksp_monitors[-1], 'HDG Mass Solve')
