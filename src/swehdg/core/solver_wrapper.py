from pyop2.profiling import timed_region

class WrappedSolver(object):
    ''' Wraps the solver routines so we can add custom monitors and timers
    '''
    def __init__(self, lvs, ksp_monitor, name):
        self._lvs = lvs
        self._inner_ksp_monitor = ksp_monitor
        self._name = name
    
    def solve(self):
        with timed_region(self._name):
            if self._inner_ksp_monitor is not None:
                with self._inner_ksp_monitor:
                    self._lvs.solve()
            else:
                self._lvs.solve()
