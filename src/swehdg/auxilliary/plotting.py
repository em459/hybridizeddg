import os

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.lines import Line2D
from matplotlib.ticker import ScalarFormatter
from pandas import DataFrame, read_csv

class PlotStyle(object):
    def __init__(self, colours=None, markers=None, linestyles=None):
        if colours is None:
            self.colours = ['C'+str(ii) for ii in range(10)]
        else:
            self.colours = colours
        
        if markers is None:
            self.markers= ['+', '^', 'd', '*', 's', 'x', 'o']
        else:
            self.markers = markers
            
        if linestyles is None:
            self.linestyles = ['-', '--', ':', '-.'] + [':']*5
        else:
            self.linestyles = linestyles

def log_interpolate(df):
    ''' Used for interpolating data on log plots
    '''
    # Take logs
    temp = np.log10(df)
    temp.index = np.log10(temp.index)
    
    # Interpolate
    temp = temp.interpolate(method='index', limit_area='inside')
    
    # Exponentiate
    interp = 10**temp
    interp.index = 10**interp.index
    
    return interp

def custom_legend(ax, style, methods):
    custom_lines = []
    for line in style.linestyles:
        custom_lines.append(Line2D([0], [0], color='black', linestyle=line, lw=1))

    custom = ax.legend(custom_lines,
                        methods,
                        title='method',
                        loc='lower left',
                        fontsize=10,
                        bbox_to_anchor=(1.05, 0),
                        borderaxespad=0.)
    
    ax.add_artist(custom)
    return ax

def data_legend(ax, select_degree=None, pos=None):
    legend_label = []
    for d in select_degree:
        legend_label.append('{}'.format(d))
    if pos is None:
        ax.legend(legend_label,
                    title='degree',
                    loc='upper left',
                    fontsize=10,
                    bbox_to_anchor=(1.05, 1),
                    borderaxespad=0.)
    else:
        ax.legend(legend_label,
                    title='degree',
                    loc=pos,
                    fontsize=10)
        
    return ax

def plot1(ax, data, style=None, select_method=None, select_degree=None, legends=True):
    ''' PLOT 1 runtime vs. refinement
    '''
    if style is None:
        style = PlotStyle()
    
    # Points
    for i, method in enumerate(select_method):
        temp = data.loc[method, 'runtime'].unstack('degree')
        for deg, col, marker in zip(temp, style.colours, style.markers):
            if deg in select_degree:
                fmt = col + marker + style.linestyles[i]
                temp[deg].plot(style=fmt, ax=ax, legend=False, logy=True, clip_on=False)
        
        plt.gca().set_prop_cycle(None)

    # First add custom legend
    if legends:
        ax = custom_legend(ax, style, select_method)
        # Then add "data" legend
        data_legend(ax, select_degree=select_degree)

    ax.set_xlabel('refinement', fontsize = 10)
    ax.set_ylabel('runtime', fontsize = 10)
    
    return ax
    
def plot2(ax, ndof_data, style=None, select_method=None, select_degree=None, legends=True):
    ''' PLOT 2 runtime vs. ndofs
    '''
    if style is None:
        style = PlotStyle()
    
    # Points
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'runtime'].unstack('degree')
        for deg, col, marker in zip(temp, style.colours, style.markers):
            if deg in select_degree:
                fmt = col + marker
                temp[deg].plot(style=fmt, ax=ax, legend=False, loglog=True, clip_on=False)
        
        ax.set_prop_cycle(None)
    
    # Lines
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'runtime'].unstack('degree')
        for deg, col in zip(temp, style.colours):
            if deg in select_degree:
                fmt = col + style.linestyles[i]
                log_interpolate(temp[deg]).plot(style=fmt, ax=ax, legend=False, loglog=True, clip_on=False)
        
        ax.set_prop_cycle(None)
    
    ### Legends
    if legends:
        ax = custom_legend(ax, style, select_method)
        # Then add "data" legend
        data_legend(ax, select_degree=select_degree)
    
    ax.set_xlabel('ndof', fontsize = 10)
    ax.set_ylabel('runtime', fontsize = 10)
    return ax

def plot3(ax, ndof_data, style=None, select_method=None, select_degree=None, legends=True):
    ''' PLOT 3 single timestep runtime per dof vs. ndof
    '''
    if style is None:
        style = PlotStyle()
    
    # Points
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'tperdof'].unstack('degree')
        if method == 'Explicit':
            temp *= 10
        for deg, col, marker in zip(temp, style.colours, style.markers):
            if deg in select_degree:
                fmt = col + marker
                temp[deg].plot(style=fmt, ax=ax, legend=False, loglog=True, clip_on=False)
        
        ax.set_prop_cycle(None)
    
    # Lines
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'tperdof'].unstack('degree')
        if method == 'Explicit':
            temp *= 10
        for deg, col in zip(temp, style.colours):
            if deg in select_degree:
                fmt = col + style.linestyles[i]
                log_interpolate(temp[deg]).plot(style=fmt, ax=ax, legend=False, loglog=True, clip_on=False)
        
        ax.set_prop_cycle(None)
    
    ### Legends
    if legends:
        ax = custom_legend(ax, style, select_method)
        # Then add "data" legend
        data_legend(ax, select_degree=select_degree)
    
    ax.set_xlabel('ndof', fontsize = 10)
    ax.set_ylabel('runtime/(ndof*ntimestep)', fontsize = 10)
    return ax

def plot4(ax, ndof_data, style=None, select_method=None, select_degree=None, legends=True):
    ''' PLOT 4 iteration vs. ndofs
    '''
    if style is None:
        style = PlotStyle()
        
    # Points
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'avg_iter'].unstack('degree')
        for deg, col, marker in zip(temp, style.colours, style.markers):
            if deg in select_degree:
                fmt = col + marker
                temp[deg].plot(style=fmt, ax=ax, legend=False, logx=True, clip_on=False)
        
        plt.gca().set_prop_cycle(None)
    
    # Lines
    for i, method in enumerate(select_method):
        temp = ndof_data.loc[method, 'avg_iter'].unstack('degree')
        for deg, col in zip(temp, style.colours):
            if deg in select_degree:
                fmt = col + style.linestyles[i]
                log_interpolate(temp[deg]).plot(style=fmt, ax=ax, legend=False, logx=True, clip_on=False)
        
        ax.set_prop_cycle(None)
    
    ### Legends
    if legends:
        ax = custom_legend(ax, style, select_method)
        # Then add "data" legend
        data_legend(ax, select_degree=select_degree)
    
    ax.set_xlabel('ndof', fontsize = 10)
    ax.set_ylabel('average number of iterations', fontsize = 10)
    return ax

def plot5(ax, data, style=None, select_method=None, select_degree=None, legends=True):
    ''' PLOT 5 speedup vs. refinement
    '''
    if style is None:
        style = PlotStyle()
        
    # Calculate speedup
    temp = data.loc['DG+ApproxSchur', 'runtime'].unstack('degree')
    hybrid_methods = [h for h in select_method if 'HDG' in h]
    
    if len(hybrid_methods) > 1:
        hyb = filter(lambda x : 'HDG+GTMG' in x, hybrid_methods).__next__()
    else:
        hyb = hybrid_methods[0]
        
    temp /= data.loc[hyb, 'runtime'].unstack('degree')
    
    # Points
    for deg, col, marker in zip(temp, style.colours, style.markers):
        if deg in select_degree:
            fmt = col + marker
            temp[deg].plot(style=fmt, ax=ax, legend=False, clip_on=False)
        
    ax.set_prop_cycle(None)
    
    # Lines
    for deg, col in zip(temp, style.colours):
        if deg in select_degree:
            fmt = col + style.linestyles[0]
            log_interpolate(temp[deg]).plot(style=fmt, ax=ax, legend=False, clip_on=False)
    
    # Line for no speedup
    ax.plot(ax.get_xlim(), [1, 1], 'k--')
    
    # Add "data" legend
    if legends:
        data_legend(ax, select_degree=select_degree)
    
    ax.set_xlabel('refinement', fontsize = 10)
    ax.set_ylabel('factor speedup', fontsize = 10)
    return ax

#####
# Scaling
#####
def _scaling_plot(ax, data, style=None, select_method=None, weak=False):
    if style is None:
        style = PlotStyle()
        
    for i, method in enumerate(select_method):
        temp = data.loc[method, 'tperts'].unstack('degree')
        for deg, col, marker in zip(temp, style.colours, style.markers):
            fmt = col + marker + style.linestyles[i]
            temp[deg].plot(style=fmt, ax=ax, logx=True, logy=(not weak), legend=False, clip_on=False)
    
    return ax
    
def weak_scaling_plot(ax, data, style=None, select_method=None, select_degree=None, legends=True):
    # Plot data
    _scaling_plot(ax, data, style=style, select_method=select_method, weak=True)
    
    # Format axes
    ax.set_xscale('log', basex=2)
    ax.xaxis.set_major_formatter(ScalarFormatter())
    ax.set_ylabel('runtime/timestep', fontsize=10)
    
    # Position of legend depends on strong/weak scaling
    pos = 'upper left'
    ax.axvspan(1, 16, facecolor='k', alpha=0.25)

    ax.yaxis.set_major_formatter(ScalarFormatter())
    
    # Add "data" legend
    if legends:
        data_legend(ax, select_degree=select_degree, pos=pos)
    return ax
    
def strong_scaling_plot(ax, data, style=None, select_method=None, select_degree=None, legends=True):
    # Plot data
    _scaling_plot(ax, data, style=style, select_method=select_method, weak=False)
    
    # Add comparison lines and format axes
    scaling_comparison_lines(ax, data, select_method=select_method, select_degree=select_degree)
    ax.set_xscale('log', basex=2)
    ax.xaxis.set_major_formatter(ScalarFormatter())
    ax.set_ylabel('runtime/timestep', fontsize=10)
    
    # Position of legend depends on strong/weak scaling
    pos = 'lower left'
    ax.set_yscale('log', basey=10)

    ax.yaxis.set_major_formatter(ScalarFormatter())

    # Add "data" legend
    if legends:
        data_legend(ax, select_degree=select_degree, pos=pos)
    return ax

def scaling_comparison_lines(ax, data, select_method=None, select_degree=None):
    ''' Add comparison lines
    '''
    eps = 0.9
    for i, method in enumerate(select_method):
        temp = data.loc[method, 'tperts'].unstack('degree')
        for deg in select_degree:
            # Line
            xa = temp.index.values[0]
            ya = temp[deg].loc[xa] * eps
            C = ya*xa
            xb = temp.index.values[-1]
            yb = C/xb
            ax.plot([xa, xb], [ya, yb], 'k--')
    return ax

#####
# Error
#####
def _error_plot(ax, data, style=None, method=None, field='', select_deg_ref=None, legends=True, plot_type=None):
    if style is None:
        style = PlotStyle()
    
    temp = data.loc[method, field].unstack(plot_type)
    for deg, col, marker in zip(temp, style.colours, style.markers):
        fmt = col + marker + '-'
        temp[deg].plot(style=fmt, ax=ax, logy=True, legend=False, clip_on=False)
    
    ax.set_ylabel(r'$L_2$ Error', fontsize=10)
    return ax

def degree_error_plot(ax, data, style=None, method=None, field='', select_degree=None, legends=True, gradients=True):
    _error_plot(ax, data, style, method, field, select_degree, legends, plot_type='degree')
    # Add degree legend
    if legends:
        data_legend(ax, select_degree, pos='lower left')
    if gradients:
        error_comparison_lines(ax, data, method, field, plot_type='degree')
    return ax
    

def refinement_error_plot(ax, data, style=None, method=None, field='', select_refinement=None, legends=True, gradients=False):
    _error_plot(ax, data, style, method, field, select_refinement, legends, plot_type='refinement')
    # Add refinement legend
    if legends:
        legend_label = []
        for r in select_refinement:
            legend_label.append('{}'.format(r))
        ax.legend(legend_label,
                    title='refinement',
                    loc='lower left',
                    fontsize=10)
    if gradients:
        error_comparison_lines(ax, data, method, field, plot_type='refinement')
    return ax

def error_comparison_lines(ax, data, method, field, plot_type='degree'):
    temp = data.loc[method, field].unstack(plot_type)
    if plot_type == 'degree':
        for extra, pos, eps in zip([0.5, 1], [(1, 1), (-10, -10)], [2, 0.5]):
            for deg in temp:
                # Line
                xa = temp.index.values[0]
                ya = temp[deg].loc[xa] * eps
                C = ya*2**(xa*(deg + extra))
                xb = temp.index.values[-1]
                yb = C/(2**(xb*(deg + extra)))
                ax.plot([xa, xb], [ya, yb], 'k--')
                # Annontate
                xm = xa + 0.5*(xb - xa)
                ym = C/(2**(xm*(deg + extra)))
                ax.annotate(r'$h^{{{}}}$'.format(deg + extra),
                             xy=(xm, ym),
                             xycoords='data',
                             xytext=pos,
                             textcoords='offset points',
                             fontsize=10)
    # These lines don't really conform with theory
    #~ elif plot_type == 'refinement':
        #~ s = 2
        #~ for extra, pos, eps in zip([0.5, 0], [(1, 1), (-10, -10)], [2, 0.5]):
            #~ for ref in temp:
                #~ # Line
                #~ xa = temp.index.values[0]
                #~ ya = temp[ref].loc[xa] * eps
                #~ C = ya*xa**(s - extra)
                #~ xb = temp.index.values[-1]
                #~ yb = C/(xb**(s - extra))
                #~ ax.plot([xa, xb], [ya, yb], 'k--')
                #~ # Annontate
                #~ xm = xa + 0.5*(xb - xa)
                #~ ym = C/(xm**(s - extra))
                #~ ax.annotate(r'$p^{{{}}}$'.format(-s + extra),
                             #~ xy=(xm, ym),
                             #~ xycoords='data',
                             #~ xytext=pos,
                             #~ textcoords='offset points',
                             #~ fontsize=10)
    return ax
