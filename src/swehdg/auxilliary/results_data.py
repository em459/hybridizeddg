import os

import numpy as np

from pandas import DataFrame, read_csv

# Sort so these always end up in the same order
SORT_ORDER = {
    'Explicit': 0,
    'DG+ApproxSchur': 1,
    'HDG+AMG': 2,
    'HDG+GTMG+AMG': 3,
    'HDG+GTMG+GMG': 4}

def method(ts, hyb, mg):
        ''' Determines method
        '''
        m = ''
        if ts=='IMEXTheta_mu0.000' or ts=='Theta_mu0.000':
            m = 'Explicit'
        elif hyb:
            m = 'HDG+'
            m += mg
        else:
            m = 'DG+ApproxSchur'
        
        return m

def add_methods_field(data):
    data['method'] = data.apply(lambda row: method(row['timestepper'], row['hybridised'], row['multigrid']), axis=1)
    return data

def get_methods(data):
    ''' Gets all methods used
    and sorts so always in the same order
    '''
    methods = list(set(data['method']))
    methods.sort(key=lambda val: SORT_ORDER[val])
    return methods

def read_results_data(directory):
    ''' Read in a data csv and return dataframe
    '''
    # Check directory exists
    if not os.path.isdir(directory):
        print('Directory', directory, 'does not exist')
        raise IOError('No such directory')
    
    csv_file = os.path.join(directory, 'results.csv')
    
    raw_data = read_csv(csv_file)
    raw_data['ndof'] = raw_data['ndof_cell'] ##+ raw_data['ndof_facet']
    raw_data['multigrid'] = raw_data['multigrid'].fillna('')
    
    raw_data = add_methods_field(raw_data)
    return raw_data
    
def read_scaling_results_data(directory):
    raw_data = DataFrame()
        
    for item in os.listdir(directory):
        folder = os.path.join(directory, item)
        if os.path.isdir(folder):
            kind, p = item.split('SCALING')
            csv_file = os.path.join(directory, item, 'results.csv')
            # Incase file issue
            try:
                read_data = read_csv(csv_file)
                read_data['procs'] = int(p)
                raw_data = raw_data.append(read_data)
            except:
                pass
                
    raw_data = add_methods_field(raw_data)
    return raw_data, kind
