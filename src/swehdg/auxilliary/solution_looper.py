from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.ksp_monitor import *
from swehdg.core.timestepper import *

from swehdg.core.periodic_square import PeriodicUnitSquareMeshHierarchy
from pyop2.profiling import timed_stage

import time
import os
import sys
import itertools
import numpy as np


def fprint(*s, file_handle=None, **kwargs):
    '''Print string only on master process, to a file

    :arg s: Stuff to print
    '''
    if (COMM_WORLD.rank == 0):
        print(*s, file=file_handle, flush=True, **kwargs)


# Parameters class
class P:
    def __init__(self, params):
        self.__dict__.update(params)

def gen_mesh(params):
    '''Generates mesh from partially populated dict of parameters
    '''
    if params['mesh_type'] == 'quads':
        quad = True
    else:
        quad = False

    if params['geometry'] == 'flat':
        N = 2**params['refinement']
        if params['mg_type'] == 'GMG':
            # Mesh hierarchy for transfering from trace to p1.
            # NOTE: For gmg, may need to create a full mesh hierarchy
            # that includes the coarsened spaces for the p1 solver.
            mesh_hierarchy = PeriodicUnitSquareMeshHierarchy(N, params['refinement']//2)
            mesh = mesh_hierarchy.mesh()
        else:
            mesh = PeriodicUnitSquareMesh(N, N, quadrilateral=quad)
    else:
        mesh = None

    return mesh

def default_param():
    '''Populates all parameters with sensible defaults
    '''

    simulation_parameters = dict()
    simulation_parameters['geometry'] = 'flat'
    simulation_parameters['mesh_type'] = 'tets'
    simulation_parameters['refinement'] = 5
    simulation_parameters['degree'] = 2
    simulation_parameters['gtmg'] = False
    simulation_parameters['mg_type'] = 'AMG'

    # Mesh NEEDS to be regenerated
    # along with bathymetry and initial conditions
    mesh =  gen_mesh(simulation_parameters)
    simulation_parameters['mesh'] = mesh

    simulation_parameters['alpha'] = 10.0
    simulation_parameters['rho'] = 0.2

    simulation_parameters['flux'] = 'Lax-Friedrichs'
    simulation_parameters['hybridised'] = True
    simulation_parameters['linear'] = True

    x, y = SpatialCoordinate(mesh)
    sigma = 0.2
    simulation_parameters['bathymetry'] = None
    simulation_parameters['initial_condition_phi'] = 0.5*exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2)
    simulation_parameters['initial_condition_u'] = 0.0

    simulation_parameters['solver_rtol'] = 1.0E-8

    simulation_parameters['timestepper'] = TimeStepperIMEXTheta
    simulation_parameters['timestep_parameter'] = 0.5
    simulation_parameters['total_time'] = 0.25 # Change back to 1

    simulation_parameters['save_output'] = False

    return simulation_parameters

def vary_param(fixed_param, key, values):
    '''Copy the fixed parameter and vary the key for the list of values
    '''
    if (not type(fixed_param) == list) and (not type(fixed_param) == tuple):
        fixed_param = [fixed_param]

    if (not type(values) == list) and (not type(values) == tuple):
        values = [values]

    product_list = []
    for fix in fixed_param:
        for val in values:
            old_val = fix[key] # Avoids modifying the original list
            fix[key] = val
            product_list.append(fix.copy())
            fix[key] = old_val

    return product_list

def meta_data(filename, params):
    '''Write all meta data to a file
    '''

    ## TODO: Replace this with config parser library

    with open(filename + '.meta', 'w') as fh:
        for key in params.keys():
            if key == 'mesh':
                value = repr(params[key])
            elif key == 'timestepper':
                value = params[key].__name__
            else:
                value = str(params[key])

            fh.write(key + ' : ' + value + '\n')


def pre_output(filename, sw, params):
    '''Write stdout info to a file for reference later
    '''

    p = P(params)

    fh = open(filename + '.output', 'w')

    # Number of cells and facets
    ncell = sw.ncell
    nfacet = sw.nfacet

    # Number of DG unknowns per cell
    ndof_cell = ncell * sw.ndof_per_cell
    if p.hybridised:
        ndof_facet = nfacet * sw.ndof_per_facet
    else:
        ndof_facet = 0

    # Print out parameters of run
    fprint ("h :", sw.h, file_handle=fh)
    dt = p.alpha*p.rho*sw.h/(2.0*p.degree+1)/sw.cg
    fprint("dt : ", dt, file_handle=fh)
    fprint("#cells : ", ncell, file_handle=fh)
    fprint("#facets : ", nfacet, file_handle=fh)

    # Print out number of processors
    fprint("Number of processes: ",
           sw.mesh.comm.size,
           file_handle=fh)

    # Print out numbers of unknowns
    fprint("total number of DG unknowns   [ndof_cell]  : ",
           ndof_cell,
           file_handle=fh)
    fprint("total number of flux unknowns [ndof_facet] : ",
           ndof_facet,
           file_handle=fh)
    fprint("ndof_facet/ndof_cell : ",
           ('%6.4f' % (ndof_facet/(1.0*ndof_cell))),
           file_handle=fh)

    fh.close()


def post_output(filename, sw, time):
    '''Write stdout info to a file for reference later
    '''

    fh = open(filename + '.output', 'a')

    fprint('elapsed time : ',
           time, 's',
           file_handle=fh)

    # In case we do explicit timestepping and _ksp_monitors is never initialised
    try:
        fprint('solver iterations:',
               ','.join(map(str, sw._ksp_monitors[0].niter)),
               file_handle=fh)
        fprint('total number of solves ',
               len(sw._ksp_monitors[0].niter),
               file_handle=fh)
        fprint('average number of KSP iterations',
               np.average(sw._ksp_monitors[0].niter),
               file_handle=fh)
    except Exception as e:
        pass

    fh.close()


def simulation_warmup(params):
    '''Use parameter dictionary to make and run a simulation.
    The simulation runs for 2 timesteps so timing is accurate
    on later runs
    '''
    p = P(params)

    # Turn on or off nonlinear terms
    deltaNL = 0 if p.linear else 1

    # Construct shallow water object
    if p.hybridised:
        sw = ShallowWaterHybridizedDG(p.geometry,
                                      p.refinement,
                                      p.degree,
                                      mesh=p.mesh,
                                      bathymetry=p.bathymetry,
                                      deltaNL=deltaNL,
                                      flux=p.flux,
                                      use_gtmg=p.gtmg,
                                      mg_type=p.mg_type,
                                      output_filename=None,
                                      solver_rtol=p.solver_rtol,
                                      ksp_verbose=0)

    else:
        sw = ShallowWaterDG(p.geometry,
                            p.refinement,
                            p.degree,
                            mesh=p.mesh,
                            bathymetry=p.bathymetry,
                            deltaNL=deltaNL,
                            flux=p.flux,
                            output_filename=None,
                            solver_rtol=p.solver_rtol,
                            ksp_verbose=0)

    # Set initial condition
    q = Function(sw.V_q)
    u, phi = q.split()

    try:
        phi.interpolate(p.initial_condition_phi)
    except Exception as e:
        phi.assign(p.initial_condition_phi)

    try:
        u.interpolate(p.initial_condition_u)
    except Exception as e:
        u.assign(p.initial_condition_u)

    # Setup timestepping
    dt = p.alpha*p.rho*sw.h/(2.0*p.degree+1)/sw.cg

    if 'Theta' in p.timestepper.__name__:
        mu = p.timestep_parameter
        timestepperinstance = p.timestepper(sw, dt, mu)
    else:
        timestepperinstance = p.timestepper(sw, dt)

    # Run simulation
    tstart = time.time()
    method_str = 'HDG' if p.hybridised else 'DG'
    with timed_stage(method_str
                    + '_ref=' + str(p.refinement)
                    + '_deg=' + str(p.degree)):
        q_final = timestepperinstance.integrate(q, 2*dt)
    tfinish = time.time()
    runtime = tfinish - tstart

    return runtime

def simulation(filename, params):
    '''Use parameter dictionary to make and run a simulation
    '''

    p = P(params)

    # Setup and output bathymetry on mesh
    if (p.bathymetry is not None) and p.save_output:
        B = FunctionSpace(p.mesh, 'CG', 1)

        bathymetry_out = Function(B, name='bathymetry')
        bathymetry_out.interpolate(p.bathymetry)
        outfile = File(filename + '_bath.pvd')
        outfile.write(bathymetry_out)

    # Turn on or off nonlinear terms
    deltaNL = 0 if p.linear else 1

    # Construct shallow water object
    if p.hybridised:
        sw = ShallowWaterHybridizedDG(p.geometry,
                                      p.refinement,
                                      p.degree,
                                      mesh=p.mesh,
                                      bathymetry=p.bathymetry,
                                      deltaNL=deltaNL,
                                      flux=p.flux,
                                      use_gtmg=p.gtmg,
                                      mg_type=p.mg_type,
                                      output_filename=filename + '.pvd',
                                      solver_rtol=p.solver_rtol,
                                      ksp_verbose=0)

    else:
        sw = ShallowWaterDG(p.geometry,
                            p.refinement,
                            p.degree,
                            mesh=p.mesh,
                            bathymetry=p.bathymetry,
                            deltaNL=deltaNL,
                            flux=p.flux,
                            output_filename=filename + '.pvd',
                            solver_rtol=p.solver_rtol,
                            ksp_verbose=0)

    if (COMM_WORLD.rank == 0):
        pre_output(filename, sw, params)

    # Set initial condition
    q = Function(sw.V_q)
    u, phi = q.split()

    try:
        phi.interpolate(p.initial_condition_phi)
    except Exception as e:
        phi.assign(p.initial_condition_phi)

    try:
        u.interpolate(p.initial_condition_u)
    except Exception as e:
        u.assign(p.initial_condition_u)

    # Setup timestepping
    dt = p.alpha*p.rho*sw.h/(2.0*p.degree+1)/sw.cg

    if p.save_output:
        monitor = SaveToDiskMonitor(sw._output_filename,
                                    [0, 1],
                                    ['momentum', 'height'])
    else:
        monitor = TimeStepperMonitorDummy()

    if 'Theta' in p.timestepper.__name__:
        mu = p.timestep_parameter
        timestepperinstance = p.timestepper(sw, dt, mu, monitor)
    else:
        timestepperinstance = p.timestepper(sw, dt, monitor)
    tsname = timestepperinstance.label

    # Redirect output to a file
    out = sys.stdout
    if (COMM_WORLD.rank == 0):
        out_file = open(filename + '.output', 'a')
    else:
        out_file = open(os.devnull, 'w')
    sys.stdout = out_file

    # Run simulation
    tstart = time.time()

    with monitor:
        method_str = 'HDG' if p.hybridised else 'DG'
        with timed_stage(method_str
                        + '_ref=' + str(p.refinement)
                        + '_deg=' + str(p.degree)):
            q_final = timestepperinstance.integrate(q, p.total_time)

    tfinish = time.time()
    runtime = tfinish - tstart

    print(flush=True)
    sys.stdout = out
    if (COMM_WORLD.rank == 0):
        post_output(filename, sw, runtime)

    return q_final, runtime, sw, tsname

def simple_run(fixture_a):
    '''Runs simulation with given fixture
    returns the final state and wall time
    '''
    outputdir = './test_output/'
    directory = outputdir + fixture_a.node.originalname + '/'
    filename = fixture_a.fixturename + str(fixture_a.param_index)
    test_parameters = fixture_a.param

    try:
        os.makedirs(directory)
    except Exception as e:
        pass

    meta_data(directory + filename, test_parameters)
    return simulation(directory + filename, test_parameters)[0:2]

if __name__ == '__main__':
    '''
    This once ran some important functionality tests
    TODO: Tidy this up or remove
    '''
    # IMPORTANT
    out = sys.stdout
    err = sys.stderr

    #####
    # Table 1
    #####
    print('=========================')
    print('Table 1')
    print('=========================')

    directory = 'test_output/table1/'
    try:
        os.makedirs(directory)
    except Exception as e:
        pass

    fixed_parameters = default_param()

    table1_list = vary_param(fixed_parameters, 'linear', [True, False])
    table1_list = vary_param(table1_list, 'degree', [0, 1, 2])
    x, y = SpatialCoordinate(fixed_parameters['mesh'])
    mybathymetry = 1.0 + 0.25*exp(-225.0*( (x - 0.75)**2))
    table1_list = vary_param(table1_list, 'bathymetry', [None, mybathymetry])
    table1_list = vary_param(table1_list, 'hybridised', [False, True])

    fields = ['linear', 'degree', 'bathymetry', 'hybridised']

    count = 0
    for item in table1_list:
        count += 1
        filename = 'simulation' + str(count)

        i = 0
        print(count, end=':: ')
        for f in fields:
            print(f, ': ', item[f], end=', ', flush=True)
            i += 1

        meta_data(directory + filename, item)
        try:
            simulation(directory + filename, item)
            print('DONE')
        except Exception as err:
            sys.stdout = out
            print('ERROR')
            store_error = err
            with open(directory + filename + '.output', 'a') as fh:
                fh.write(str(err.args))



    #####
    # Table 2
    #####
    print()
    print('=========================')
    print('Table 2')
    print('=========================')

    directory = 'test_output/table2/'
    try:
        os.makedirs(directory)
    except Exception as e:
        pass

    fixed_parameters = default_param()
    fixed_parameters['degree'] = 2
    fixed_parameters['linear'] = True
    fixed_parameters['bathymetry'] = None
    fixed_parameters['hybridised'] = False

    fixed_parameters['flux'] = 'upwind'

    table2_list = vary_param(fixed_parameters, 'mesh_type', ['tets', 'quads'])
    ## Regenerate all meshes
    for item in table2_list:
        item['mesh'] = gen_mesh(item)
        x, y = SpatialCoordinate(item['mesh'])
        sigma = 0.2
        item['initial_condition_phi'] = 0.5*exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2)

    table2_list = vary_param(table2_list, 'linear', [True, False])
    table2_list = vary_param(table2_list, 'degree', [0, 1, 2])

    fields = ['mesh_type', 'linear', 'degree']

    count = 0
    for item in table2_list:
        count += 1
        filename = 'simulation' + str(count)

        i = 0
        print(count, end=':: ')
        for f in fields:
            print(f, ': ', item[f], end=', ', flush=True)
            i += 1

        meta_data(directory + filename, item)
        try:
            simulation(directory + filename, item)
            print('DONE')
        except Exception as err:
            sys.stdout = out
            print('ERROR')
            store_error = err
            with open(directory + filename + '.output', 'a') as fh:
                fh.write(str(err.args))
