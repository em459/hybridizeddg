import sys
import math
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl

'''
*************************************************************************
* Fourier stability analysis on structured grid
*************************************************************************

This code can be used to carry out a Fourier stability analysis for the 
lowest order DG method on a structured grid. Write the 
SWEs as a linear system

.. math::
  \frac{d\vec{q}}{dt} = c_g A \vec{q}

with state vector :math:`\vec{q} = (u,v,\phi)` where :math:`u_{ij}` etc. is
the value in grid cell :math:`(i,j)`. Then :math:`A` is a :math:`3\times 3`
block matrix

.. math::
  A = \begin{pmatrix}
        A_{uu} & A_{uv} & A_{u\phi} \\
        A_{vu} & A_{vv} & A_{v\phi} \\
        A_{\phi u} & A_{\phi v} & A_{\phi\phi} \\
      \end{pmatrix}

Further, write in Fourier space:

.. math::
  `\vec{q}_{j\ell} = \hat{\vec}\exp[i h(j k_x+\ell k_y)]`

This allows writing the system matrix as :math:`A=A(\vec{k})` where
:math:`\vec{k}=(k_x,k_y)\in [-\pi/h,\pi/h]\times [-\pi/h,\pi/h]`.

This code calculates the matrix :math:`A^{Up}` for the Upwind flux and the
corresponding matrix :math:`A^{LF}` for the Lax-Friedrichs flux.

We get:

.. math::
  A^{Up}(\vec{k}) = 
  \begin{pmatrix}
    h/2 \hat{\Delta}_x & 0 & - \hat{D}_x \\
    0 & h/2 \hat{\Delta}_y & -\hat{D}_y \\
    -\hat{D}_x & - \hat{D}_y & h/2\hat{\Delta}
 \end{pmatrix}

.. math::
  A^{Up}(\vec{k}) = 
  \begin{pmatrix}
    h/2 \hat{\Delta} & 0 & - \hat{D}_x \\
    0 & h/2 \hat{\Delta} & -\hat{D}_y \\
    -\hat{D}_x & - \hat{D}_y & h/2\hat{\Delta}
 \end{pmatrix}

with 
:math:`\hat{D}_x = i/h*\sin(h k_x)`,
:math:`\hat{D}_y = i/h*\sin(h k_y)`,
:math:`\hat{\Delta}_y = -1/(4h^2)*\sin^2(h k_y/2)`, 
:math:`\hat{\Delta}_x = -1/(4h^2)*\sin^2(h k_x/2)` and
:math:`\hat{\Delta} = \hat{\Delta}_x+\hat{\Delta}_y`
  
The main question the code tries to answer is whether the following
semi-implicit method is stable:

.. math::
   \frac{\vec{q}^{(n+1)}-\vec{q}^{(n)}}{\Delta t} = 
   c_g(1-\mu)\left(A^{LF}-A^{Up}\right)\vec{q}^{(n)} + c_g \mu A^{Up}\vec{q}^{(n+1)}

This can be achieved by looking at the spectrum of the following transfer
matrix

.. math::
  T(\vec{k}) = \left(1-c_g\Delta t A^{Up}\right)^{-1}
               \left(1-c_g\Delta t (A^{LF}-A^{Up})\right)

'''

def Dx(k,h):
    '''Calculate :math:`\hat{D}_x(\vec{k}) = i/h*\sin(h k_x)`

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    return 1j/h*math.sin(h*k[0])

def Dy(k,h):
    '''Calculate :math:`\hat{D}_y(\vec{k}) = i/h*\sin(h k_y)`

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    return 1j/h*math.sin(h*k[1])

def Deltax(k,h):
    '''Calculate :math:`\hat{\Delta}_x(\vec{k}) = -1/(4h^2)*\sin^2(h k_x/2)`

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    return -4/h**2*math.sin(0.5*h*k[0])**2

def Deltay(k,h):
    '''Calculate :math:`\hat{\Delta}_x(\vec{k}) = -1/(4h^2)*\sin^2(h k_x/2)`

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    return -4/h**2*math.sin(0.5*h*k[1])**2

def A_LaxFriedrich(k,h):
    ''' Calculate the propagation matrix :math:`A^{LF}` for the Lax-Friedrich
        method

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    kx,ky = k
    A = np.zeros((3,3),dtype=complex)
    A[0,0] = 0.5*h*(Deltax(k,h)+Deltay(k,h))
    A[0,2] = -Dx(k,h)
    A[1,1] = 0.5*h*(Deltax(k,h)+Deltay(k,h))
    A[1,2] = -Dy(k,h)
    A[2,0] = -Dx(k,h)
    A[2,1] = -Dy(k,h)
    A[2,2] = 0.5*h*(Deltax(k,h)+Deltay(k,h))
    return A


def A_Upwind(k,h):
    ''' Calculate the propagation matrix A^{Up} for the Upwind method 

    :arg k: Wave vector :math:`\vec{k}`
    :arg h: Grid spacing h
    '''
    kx,ky = k
    k_sq = kx**2+ky**2
    A = np.zeros((3,3),dtype=complex)
    A[0,0] = 0.5*h*Deltax(k,h)
    A[0,2] = -Dx(k,h)
    A[1,1] = 0.5*h*Deltay(k,h)
    A[1,2] = -Dy(k,h)
    A[2,0] = -Dx(k,h)
    A[2,1] = -Dy(k,h)
    A[2,2] = 0.5*h*(Deltax(k,h)+Deltay(k,h))
    return A

def T(k,h,dt,cg,mu):
    ''' Calculate the propagation matrix :math:`T(\vec{k},h,\Delta t)` for
        the combined method
    
    :arg k: Two-component wave-vector :math:`\vec{k}`
    :arg h: Grid spacing h
    :arg dt: time step size :math:`\Delta t`
    :arg cg: Gravity wave speed :math:`c_g`
    :arg mu: Off-centering parameter :math:`\mu`
    '''
    A_implicit = np.linalg.inv(np.identity(3) - mu*dt*cg*A_Upwind(k,h))
    A_explicit = np.identity(3)+(1.-mu)*dt*cg*(A_LaxFriedrich(k,h)-A_Upwind(k,h))
    return np.matmul(A_implicit,A_explicit)
