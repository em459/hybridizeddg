from firedrake import *
from firedrake.petsc import PETSc
from swehdg.core.shallow_water import *
from swehdg.core.ksp_monitor import *
import numpy as np
import time
import sys, os
import argparse
from pyop2.profiling import timed_stage
from swehdg.core.timestepper import *
from swehdg.auxilliary.balanced_vortex import *
import save_mat
import matplotlib as mpl
import spectral_analysis
from matplotlib import pyplot as plt

parameters["pyop2_options"]["lazy_evaluation"] = False

'''Calculate eigenvalues of the transfor operator :math:`T` of the
theta-method given by

  ..math::
    \\frac{q^{(t+\\Delta t)}-q^{(t)}}{\\Delta t} = 
    (1-\\mu) (L_{LF}-L_{Up})(q^{(t)}) + \\mu L_{Up}(q^{(t+\\Delta t)})

If :math:`M` denotes the mass matrix for the state vector, and 
:math:`A_{Up}` and :math:`A_{LF}` denote the matrix form of the upwinding and
Lax-Friedrichs fluxes, then the transfer operator is given by

  ..math::
    T = (M+\\mu \\Delta t A_{Up})^{-1}(M+(1-\\mu)\\Delta t(A_{LF}-A_{Up}))

and it maps from the current state to the state at the next time step, i.e
:math:`q^{(t+\\Delta t)} = Tq^{(t)}`. 

In addition to plotting those eigenvalues, also plot the eigenvalues from the
theoretical Fourier analysis in XXX

''' 

geometry = 'flat'
nrefine = 5
degree = 0

# Off-centering parameter mu (1.0=fully implicit, 0.0=fully explicit)
mu = 0.5

# Final time
tfinal = 1.0

mesh = PeriodicUnitSquareMesh(2**nrefine, 2**nrefine, quadrilateral=True)

sw_tmp = ShallowWaterHybridizedDG(geometry,
                                  nrefine,
                                  degree,
                                  mesh=mesh,
                                  flux='Lax-Friedrichs')

# Grid spacing
h = sw_tmp.h

# CFL scaling parameters. The explicit method is stable of approximately
# nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
# Here choose a time step size which is \alpha times larger than this, i.e.
# dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g

rho_CFL = 0.2

# scaling factor alpha
alpha = 10.0

# Time step size
dt = alpha*rho_CFL*h/(2.*degree+1)/sw_tmp.cg

print ('h  = ',('%16.8e' % h))
print ('dt = ',('%16.8e' % dt))

filename='test_output/write_test/lowest_order.pvd'

sw = ShallowWaterDG(geometry,
                    nrefine,
                    degree,
                    mesh=mesh,
                    deltaNL=1,
                    output_filename=filename,
                    flux='upwind')

V_q = sw.V_q
    
# Construct initial condition
q = Function(V_q)
u, phi = q.split()
f_over_cg = 2.0*sw._Omega/sw._cg
non_linear = (sw._deltaNL==1)
dphi = 0.1
vortex = BalancedVortex(dphi,f_over_cg,non_linear)
phi.interpolate(vortex.phis_ufl(sw.mesh))
phi_exact  = phi.copy()
u.interpolate(vortex.u_ufl(sw.mesh))

# Extract matrices
w, psi = TrialFunctions(sw._V_q)
u, phi = TestFunctions(sw._V_q)
# Mass matrix
form_mass = inner(u,w)*dx + phi*psi*dx
mat_mass = save_mat.get_mat_from_bilinear(form_mass)
# Upwind flux
form_up = sw._linear_upwind(u,phi,w,psi)
mat_up = save_mat.get_mat_from_bilinear(form_up)
# Lax-Friedrichs
form_lax = sw._linear_lax(u,phi,w,psi)
mat_lax = save_mat.get_mat_from_bilinear(form_lax)

B = mu*dt*sw.cg*mat_up + mat_mass

T = np.matmul(np.linalg.inv(B),mat_mass - (1.-mu)*dt*sw.cg*(mat_lax-mat_up))
w,v = np.linalg.eig(T)
lmbda_re = [np.real(x) for x in w]
lmbda_im = [np.imag(x) for x in w]
plt.clf()
ax = plt.gca()

# Plot unit circle (stability boundary)                                     
X = np.arange(0,2.*math.pi,0.001)
circle = mpl.patches.Circle((0,0),1.0,
                            color='lightgray',
                            facecolor='lightgray',
                            fill=True)
ax.add_artist(circle)
ax.set_xlim(-1.1,1.1)
ax.set_ylim(-1.1,1.1)
ax.set_xlabel(r'Re($\lambda$)')
ax.set_ylabel(r'Im($\lambda$)')
ax.set_aspect('equal')

# Plot eigenvalues of matrices, as extracted from Firedrake
color = 'blue'
plt.plot(lmbda_re[:],lmbda_im[:],
         linewidth=0,
         marker='o',
         markersize=2,
         color=color,
         markeredgewidth=0,
         markerfacecolor=color,
         label='Firedrake')

# Eigenvalues from Fourier analysis

# Create a mesh in k-space with n points in each direction
# Number of k points
n =50
Ky = np.arange(-math.pi/h,math.pi/h,math.pi/(h*n))
Kx = np.arange(-math.pi/h,math.pi/h,math.pi/(h*n))

# Now for each wave vector k, work out the eigenvalues and
# plot them in the complex plane
label = 'Fourier'
for ky in Ky:
    lmbda_re = np.zeros((len(Kx),3))
    lmbda_im = np.zeros((len(Kx),3))
    for i,kx in enumerate(Kx):
        k = (kx,ky)
        w,v = np.linalg.eig(spectral_analysis.T(k,h,dt,sw.cg,mu))
        lmbda_re[i,:] = [np.real(x) for x in w]
        lmbda_im[i,:] = [np.imag(x) for x in w]
    for j in range(3):
        color = 'green'
        plt.plot(lmbda_re[:-1,j],lmbda_im[:-1,j],
                 linewidth=0,
                 marker='o',
                 markersize=2,
                 color=color,
                 markeredgewidth=0,
                 markerfacecolor=color,
                 label=label)
        if (label=='Fourier'):
            label = None

plt.legend()

plt.savefig('spectrum.pdf',bbox_inches='tight')

# Create a mesh in k-space with n points in each direction
# Number of k points
n =50
Ky = np.arange(-math.pi/h,math.pi/h,math.pi/(h*n))
Kx = np.arange(-math.pi/h,math.pi/h,math.pi/(h*n))

# Run the timestepping method
monitor=SaveToDiskMonitor(sw._output_filename, [0,1],['momentum','height'])
timestepper = TimeStepperIMEXTheta(sw,dt,mu,monitor)
with monitor:
    q_final = timestepper.integrate(q,tfinal)
phi_final = q_final.split()[1]
phi_error = math.sqrt(assemble((phi_final-phi_exact)*(phi_final-phi_exact)*dx))
print ('error in phi-field = '+('%16.8e' % phi_error))




