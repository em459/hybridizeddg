import numpy as np
from firedrake import *
from firedrake.petsc import PETSc
from matplotlib import pyplot as plt

def get_mat_from_bilinear(form):
    '''Extract the PETSc matrix from a bilinear form
    and return  it as a numpy array

    :arg form: (unassembled) bilinear form
    '''
    M_petsc = assemble(form).M.handle
    size = M_petsc.getSize()
    print ('size = ',size)
    Lij = PETSc.Mat()
    M_petsc.convert('aij', Lij)
    Lnp = np.array(Lij.getValues(range(size[0]), range(size[1])))
    return Lnp

def get_block_sizes(tensor):
    '''Work out the block sizes of a SLATE tensor

    :arg tensor: SLATE tensor object
    '''
    _tensor = tensor.blocks
    # Calculate sizes
    size = []
    for i in range(3):        
        M_petsc = assemble(_tensor[i,0]).M.handle
        size_block = M_petsc.getSize()
        size.append(size_block[0])
    return size

def get_mat_from_3x3tensor(tensor):
    '''Extract the PETSC matrix from a SLATE tensor and return it
    as a numpy array

    :arg tensor: SLATE tensor object
    '''
    _tensor = tensor.blocks
    size = get_block_sizes(tensor)
    Lnp = np.zeros((np.sum(size),np.sum(size)))
    offset_i = 0
    for i in range(3):
        offset_j = 0
        for j in range(3):
            M_petsc = assemble(_tensor[i,j]).M.handle
            Lij = PETSc.Mat()
            M_petsc.convert('aij', Lij)
            Lnp[offset_i:offset_i+size[i],
                offset_j:offset_j+size[j]] = Lij.getValues(range(size[i]),
                                                           range(size[j]))
            offset_j += size[j]
        offset_i += size[i]
    return Lnp

def show_mat(mat,filename,size=None):
    '''Visualise the matrix given by the numpy array mat. We assume that
    this comes from a 3x3 block matrix. Also
    show the block limits given by the size array. Save the plot to a 
    file given in filename
    
    :arg mat: Matrix given as a numpy array
    :arg size: Array with block sizes
    :arg filename: Name of file to save the plot to
    '''
    plt.clf()
    plt.imshow(mat)
    if (size is not None):
        total_size = np.sum(size)
        print (size)
        offset = size[0]
        for i in range(len(size)-1):
            local_size = size[i+1]
            plt.plot([0,total_size],[offset,offset],
                     color='white')
            plt.plot([offset,offset],[0,total_size],
                     color='white')
            offset += local_size
        offset_i = 0
        label = (r'$u$',r'$\phi$',r'$\lambda$')
        for i in range(len(size)):
            offset_j = 0
            for j in range(len(size)):
                plt.text(offset_j+10, offset_i+10,label[i]+label[j],
                         horizontalalignment='left',
                         verticalalignment='top',
                         color='white')
                offset_j += size[j]
            offset_i += size[i]
        
    plt.savefig(filename,bbox_inches='tight')
