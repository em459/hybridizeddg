from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.timestepper import *
from swehdg.auxilliary.solution_looper import *
import pytest

'''
This is a file outlining how to use some of the features of the
solution_looper module to generate tests for pytest. It is intended to
serve as an example and template for creating more tests.
'''

fixed_param = default_param()

# Replace all fixed_param with fixed_param=defaults()
# Functionality
super_list = []
super_list += vary_param(fixed_param, 'degree', [0, 1, 2])

#fixed_param['refinement'] = 3

# MUST REGENERATE MESH /IF/ REFINEMENT CHANGED!!!
#mesh = gen_mesh(fixed_param)
#fixed_param['mesh'] = mesh

# MUST REGENERATE INITIAL CONDITIONS /IF/ MESH CHANGED!!!
#simulation_parameters['initial_condition_phi'] = 0.5*exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2)
#simulation_parameters['initial_condition_u'] = 0.0

super_list += vary_param(fixed_param, 'degree', [0, 1])
super_list = vary_param(super_list, 'linear', [True, False])

@pytest.fixture(params=super_list)
def fixture_a(request):
    '''An example fixture
    '''
    return request

def test_gaussian_bump(fixture_a):
    '''An example test
    '''
    outputdir = './test_output/'
    directory = outputdir + fixture_a.node.originalname + '/'
    filename = fixture_a.fixturename + str(fixture_a.param_index)
    test_parameters = fixture_a.param
    
    try:
        os.makedirs(directory)
    except Exception as e:
        pass
    
    meta_data(directory + filename, test_parameters)
    simulation(directory + filename, test_parameters)

if __name__ == '__main__':
    '''
    This is an example of how you may use the testing framework
    '''
    pytest.main(['-v', __file__])
