from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.timestepper import *
from swehdg.auxilliary.solution_looper import *
from swehdg.auxilliary.balanced_vortex import *

import pytest

import numpy as np

'''
These tests check that the well balanced property holds for these solvers.
This involves checking for linear and non-linear, with and without
bathymetry, that a flat and constant stationary solution does not
spontaneously begin to move.
'''

# Set fixed parameters to be the "defaults"
fixed_param = default_param()
fixed_param['refinement'] = 4

mesh = gen_mesh(fixed_param)
fixed_param['mesh'] = mesh

fixed_param['initial_condition_phi'] = 0.2
fixed_param['initial_condition_u'] = 0.0
fixed_param['solver_rtol'] = 1.0E-16

# Construct a non-trivial bathymetry
x, y = SpatialCoordinate(mesh)
gaussian_hump = 1.0 + 0.2*exp(-25.0*((x-0.5)**2+(y-0.5)**2))

fixed_param['flux'] = 'upwind'
flat_upwind = vary_param(fixed_param, 'degree', [0, 1, 2])
flat_upwind = vary_param(flat_upwind, 'hybridised', [True, False])
flat_upwind = vary_param(flat_upwind, 'bathymetry', [None, gaussian_hump])

fixed_param['flux'] = 'Lax-Friedrichs'
flat_lax = vary_param(fixed_param, 'degree', [0, 1, 2])
flat_lax = vary_param(flat_lax, 'hybridised', [True, False])
flat_lax = vary_param(flat_lax, 'linear', [True, False])
flat_lax = vary_param(flat_lax, 'bathymetry', [None, gaussian_hump])

flat_list = flat_upwind + flat_lax
@pytest.fixture(params=flat_list)
def flat_fixture(request):
    '''An example fixture
    '''
    return request

def test_flat(flat_fixture):
    '''test_flat tests whether a lake at rest constant solution changes
    over time. This is now a replacement for test_wellbalanced
    '''
    phi_ic = flat_fixture.param['initial_condition_phi']
    
    q_final, t = simple_run(flat_fixture)
    
    u_final, phi_final = q_final.split()[0:2]
    phi_error = np.sqrt(assemble((phi_final - phi_ic)*(phi_final - phi_ic)*dx))
    u_error = np.sqrt(assemble(inner(u_final, u_final)*dx))
    
    # Tolerance
    if flat_fixture.param['bathymetry'] is None:
        tolerance = 1.0E-12
    else:
        # More lenient tolerance for bathymetry case
        tolerance = 1.0E-8
        
    assert np.allclose(phi_error, 0.0, atol=tolerance)
    assert np.allclose(u_error, 0.0, atol=tolerance)
    
if __name__ == '__main__':
    pytest.main(['-v', __file__])
