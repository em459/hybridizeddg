import math
from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.timestepper import *
import pytest
import numpy as np

'''
These tests are left over from the original test suite and many are
repeated elsewhere.
TODO: These tests will be removed later
'''

class ShallowWaterParam(object):
    def __init__(self, degree, mu, hybridized, flux):
        self.degree = degree
        self.mu = mu
        self.hybridized = hybridized
        self.flux = flux


class MeshParam(object):
    def __init__(self, geometry, nrefine):
        self.geometry = geometry
        self.nrefine = nrefine


@pytest.fixture
def mesh_param():
    '''Return mesh parameter object'''
    # Set geometry to either 'flat' or 'spherical'
    geometry = 'flat'
    # Number of grid refinement levels
    nrefine = 3
    return MeshParam(geometry, nrefine)


@pytest.fixture(params=(
    ShallowWaterParam(0, 0.0, False, 'upwind'),          # fully explicit timestepping, degree 0
    ShallowWaterParam(1, 0.0, False, 'upwind'),          # fully explicit timestepping, degree 1
    ShallowWaterParam(2, 0.0, False, 'upwind'),          # fully explicit timestepping, degree 2
    ShallowWaterParam(0, 0.5, False, 'upwind'),          # semi-implicit timestepping, degree 0
    ShallowWaterParam(1, 0.5, False, 'upwind'),          # semi-implicit timestepping, degree 1
    ShallowWaterParam(2, 0.5, False, 'upwind'),          # semi-implicit timestepping, degree 2
    ShallowWaterParam(0, 0.5, True, 'upwind'),           # semi-implicit timestepping, degree 0
    ShallowWaterParam(1, 0.5, True, 'upwind'),           # semi-implicit timestepping, degree 1
    ShallowWaterParam(2, 0.5, True, 'upwind'),           # semi-implicit timestepping, degree 2
    ShallowWaterParam(0, 0.5, False, 'Lax-Friedrichs'),  # semi-implicit timestepping, degree 0
    ShallowWaterParam(1, 0.5, False, 'Lax-Friedrichs'),  # semi-implicit timestepping, degree 1
    ShallowWaterParam(0, 0.5, True, 'Lax-Friedrichs'),   # semi-implicit timestepping, degree 0
    ShallowWaterParam(1, 0.5, True, 'Lax-Friedrichs'),   # semi-implicit timestepping, degree 1
    ShallowWaterParam(2, 0.5, True, 'Lax-Friedrichs')
))
def timestepper(request, mesh_param):
    '''Create timestepper object'''

    # Geometry
    geometry = mesh_param.geometry
    # Refinement level of spherical grid
    nrefine = mesh_param.nrefine
    # Polynomial degree
    degree = request.param.degree
    # Flux
    flux = request.param.flux
    if (request.param.hybridized):
        shallowwater = ShallowWaterHybridizedDG(geometry,
                                                nrefine,
                                                degree,
                                                flux=flux,
                                                solver_rtol=1.E-13)
    else:
        shallowwater = ShallowWaterDG(geometry,
                                      nrefine,
                                      degree,
                                      flux=flux,
                                      solver_rtol=1.E-13)

    # Off-centering parameter mu (1.0=fully implicit, 0.0=fully explicit)
    mu = request.param.mu
    # CFL scaling parameters. The explicit method is stable of approximately
    # nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
    # Here choose a time step size which is \alpha times this, i.e.
    # dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g
    alpha = 0.5
    rho_CFL = 0.2
    dt = alpha*rho_CFL*shallowwater.h/((2.*degree+1)*shallowwater.cg)
    return TimeStepperIMEXTheta(shallowwater, dt, mu)


@pytest.fixture(params=(
    ShallowWaterParam(0, 0.5, False, 'upwind'),  # semi-implicit timestepping, degree 0
    ShallowWaterParam(0, 0.5, True, 'upwind')    # semi-implicit timestepping, degree 0
))
def timestepper_vary_hp(request, mesh_param):
    '''Create list of shallow water timestepper objects with varying
    polynomial degree and varying mesh width (= refinement levels)

    :arg request: parameters
    :arg mesh_param: Mesh parameter fixture
    '''
    # Geometry
    geometry = mesh_param.geometry
    # Off-centering parameter mu (1.0=fully implicit, 0.0=fully explicit)
    mu = request.param.mu

    timestepper_list = []
    for degree in (0, 1, 2):
        for nrefine in (2, 3, 4):
            # CFL scaling parameters. The explicit method is stable for
            # approximately nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
            # Here choose a time step size which is \alpha times this, i.e.
            # dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g
            alpha = 0.5
            rho_CFL = 0.2
            if (request.param.hybridized):
                shallowwater = ShallowWaterHybridizedDG(geometry,
                                                        nrefine, degree,
                                                        solver_rtol=1.E-12)
            else:
                shallowwater = ShallowWaterDG(geometry,
                                              nrefine, degree,
                                              solver_rtol=1.E-12)
            dt = alpha*rho_CFL*shallowwater.h/((2.*degree+1)*shallowwater.cg)
            timestepper = TimeStepperIMEXTheta(shallowwater, dt, mu)
            timestepper_list.append(timestepper)
    return timestepper_list


def set_initial(phi):
    '''Set initial condition, a Gaussian peak of fixed width

    :arg phi: field to apply initial condition to
    '''
    mesh = phi.function_space().mesh()
    dim = mesh.geometric_dimension()
    # width of initial peak
    sigma = 0.4
    sigma_x = 0.2
    sigma_y = 0.4
    if (dim == 2):
        x, y = SpatialCoordinate(mesh)
        phi.interpolate(exp(-((x-0.5)**2/sigma_x**2+(y-0.5)**2/sigma_y**2)))
    else:
        x, y, z = SpatialCoordinate(mesh)
        phi.interpolate(exp(-(x**2+z**2)/sigma**2))


def set_stationary(sw, phi, u):
    '''Set initial conditions of a state in geostrophic balance (i.e. being
    a stationary solution of the shallow water equations.

    In the 2d case, set up a rotationally invariant smooth field
    :math:`\\phi(r) = \\exp\\left[-\\left(\frac{r}{r-\\sigma}\\right)^2\\right]`
    where :math:`r` is the distance from the centre of the unit square and
    :math:`\\sigma<1` is the diameter of the vortex. Then construct a purely
    tangential velocity field as
    :math:`\\vec{u} = -\\frac{c_g}{2\\Omega}\\phi'(r)\\hat{\\vec{r}}^{\\perp}`.

    In the 3d case, set of a smooth field :math:`\\phi(z)` which only varies
    with latitude :math:`\\theta` with :math:`\\sin(\\theta)=z` and is
    restricted to the range :math:`-\\sigma<z<\\sigma`. In
    particular :math:`\\phi(z) = \\exp\\left[\\frac{\\sigma^2}{\\sigma^2-z^2}\\right]`. Then construct a purely longitudinal tangential velocity field
    with the longitudinal component given as :math:`u_T=-\\frac{c_g}{2\\Omega\\tan(\\theta)}\\phi'(z) = -\\frac{c_g\\sqrt{1-z^2}}{2\\Omega z}\\phi'(z)`.

    :arg sw: Instance of a shallow water class (to extract c_g and Omega)
    :arg phi: potential field to apply initial conditions to
    :arg u: momentum field to apply initial conditions to
    '''
    mesh = phi.function_space().mesh()
    dim = mesh.geometric_dimension()
    if (dim == 2):
        sigma = 0.4
        x, y = SpatialCoordinate(mesh)
        x -= 0.5
        y -= 0.5
        r = sqrt(x**2+y**2)
        f_phi = conditional(r < sigma, exp(-(r/(r-sigma))**2), 0.0)
        f_u_t = conditional(r < sigma, 2.*r*sigma/(r-sigma)**3*exp(-(r/(r-sigma))**2), 0.0)
        cg_over_f = sw.cg/(2.*sw._Omega)
        f_u = (-y/sqrt(x**2+y**2)*f_u_t*(cg_over_f),
               x/sqrt(x**2+y**2)*f_u_t*(cg_over_f))
        phi.project(f_phi)
        u.project(as_vector(f_u))
    else:
        sigma = 0.8
        x, y, z = SpatialCoordinate(mesh)
        f_phi = conditional(And((-sigma < z), (z < sigma)),
                            exp(-sigma**2/(sigma**2-z**2)),
                            0.0)
        f_u_t = conditional(And((-sigma < z), (z < sigma)),
                            -2.*sigma**2*z/(sigma**2-z**2)**2*exp(-sigma**2/(sigma**2-z**2)),
                            0.0)
        cg_over_f = sw.cg/(2.*sw._Omega)*sqrt(1.-z*z)/z
        f_u = (-y/sqrt(x**2+y**2)*f_u_t*(-cg_over_f),
               x/sqrt(x**2+y**2)*f_u_t*(-cg_over_f),
               0.0)
        phi.project(f_phi)
        u.project(as_vector(f_u))


def test_linear_solve(timestepper):
    '''Check that the linear solve works, i.e. the residual is zero. This
    in particular tests the hybridized method since the non-hybridized
    residual is used for testing.

    :arg timestepper: shallow water timestepper fixture
    '''
    shallowwater = timestepper._system
    # Construct initial condition
    qold = Function(shallowwater.V())
    u_old, phi_old = qold.split()[0:2]
    u_old.assign(0.0)
    set_initial(phi_old)

    # Copy to native-DG state
    qold_nat = Function(shallowwater.V_q)
    u_old_nat, phi_old_nat = qold_nat.split()
    u_old_nat.assign(u_old)
    phi_old_nat.assign(phi_old)

    alpha = 1.0
    q = Function(shallowwater.V())
    res_old = shallowwater.resM(qold)
    solver = shallowwater.linear_solver(res_old, q, alpha)
    solver.solve()
    u, phi = q.split()[0:2]

    # Copy to native DG state
    q_nat = Function(shallowwater.V_q)
    u_nat, phi_nat = q_nat.split()
    u_nat.assign(u)
    phi_nat.assign(phi)
    # Calculate non-hybridized residual
    res = shallowwater.resMnative(qold_nat) \
        - (shallowwater.resMnative(q_nat)-alpha*shallowwater.resLnative(q_nat))
    assert np.allclose(norm(assemble(res)), 0.0, atol=1.E-12)


def test_conservative(timestepper):
    '''Check that the integrator is conservative, i.e. the integral over the
    potential is exactly conserved

    :arg timestepper: shallow water timestepper fixture
    '''
    shallowwater = timestepper._system
    V_q = shallowwater.V_q
    # Construct a constant potential-only function
    p_const = Function(V_q)
    w, psi = p_const.split()
    psi.assign(1.0)
    # Construct initial condition
    q = Function(V_q)
    u, phi = q.split()
    u.assign(0.0)
    set_initial(phi)
    tfinal = 1.0
    # Initial value
    phi_int_initial = assemble(psi*phi*dx)
    q_final = timestepper.integrate(q, tfinal)
    u_final, phi_final = q_final.split()[0:2]
    phi_int_final = assemble(psi*phi_final*dx)
    assert np.allclose(phi_int_initial, phi_int_final, rtol=1.E-12)


@pytest.mark.skip(reason="takes too long")
def test_convergence(timestepper_vary_hp):
    '''Test convergence with varying mesh width h and polynomial degree p.
    For this, run the code for all passed values of h and p and plot the
    difference :math:`||\\phi-\\phi_{exact}||_2`.

    By choosing the initial state to be in geostrophic balance the solver
    should simply leave it unchanged.

    :arg timestepper_vary_hp: Fixture with instances of the shallow water
    timestepper for different polynomial degrees and mesh widths
    '''
    import matplotlib
    matplotlib.use('Agg')
    from matplotlib import pyplot as plt

    h_dict = {}
    dphi_dict = {}
    for timestepper in timestepper_vary_hp:
        shallowwater = timestepper._system
        V_q = shallowwater.V_q
        # Construct initial condition
        q = Function(V_q)
        u, phi = q.split()
        set_stationary(shallowwater, phi, u)
        tfinal = 1.0
        q_final = timestepper.integrate(q, tfinal)
        u_final, phi_final = q_final.split()[0:2]
        degree = shallowwater._degree
        dphi = math.sqrt(assemble((phi_final-phi)**2*dx))
        if degree not in h_dict.keys():
            h_dict[degree] = []
            dphi_dict[degree] = []
        h_dict[degree].append(shallowwater.h)
        dphi_dict[degree].append(dphi)
        comm = shallowwater.mesh.comm
        if (comm.rank == 0):
            print (' p = '+('%2d' % degree)+' h = '+('%8.4e' % shallowwater.h)+' dphi = '+('%8.4e' % dphi))
        save_to_disk = False
        if (save_to_disk):
            #### Save to disk
            filename = 'initial_potential_'+str(shallowwater._nrefine)+'_'+str(shallowwater._degree)+'.pvd'
            outfile = File(filename)
            outfile.write(phi)
            filename = 'initial_momentum_'+str(shallowwater._nrefine)+'_'+str(shallowwater._degree)+'.pvd'
            outfile = File(filename)
            outfile.write(u)
            filename = 'final_potential_'+str(shallowwater._nrefine)+'_'+str(shallowwater._degree)+'.pvd'
            outfile = File(filename)
            outfile.write(phi_final)
            filename = 'final_momentum_'+str(shallowwater._nrefine)+'_'+str(shallowwater._degree)+'.pvd'
            outfile = File(filename)
            outfile.write(u_final)
    for degree in h_dict.keys():
        dphi_dict[degree] = np.array([x for _, x in sorted(zip(h_dict[degree],
                                                               dphi_dict[degree]),
                                                           key=lambda pair: pair[0])])
        h_dict[degree] = np.array(sorted(h_dict[degree]))

    geometry = shallowwater._geometry
    if (type(shallowwater) is ShallowWaterDG):
        discretisation = 'standardDG'
    elif (type(shallowwater) is ShallowWaterHybridizedDG):
        discretisation = 'hybridizedDG'
    else:
        discretisation = 'unknown'
    output_filename = 'convergence_'+geometry+'_'+discretisation+'.pdf'

    # Plot error ||phi-phi_{exact}|| for different polynomial degrees
    # and grid spacings
    plt.clf()
    ax = plt.gca()
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel(r'grid spacing $h$')
    ax.set_ylabel(r'error $||\phi-\phi_{\operatorname{exact}}||_2$')
    colors = {0: 'blue', 1: 'red', 2: 'green', 3: 'black'}
    plots = []
    labels = []
    for degree in h_dict.keys():
        plots.append(plt.plot(h_dict[degree],
                              dphi_dict[degree],
                              color=colors[degree],
                              markersize=8, marker='o',
                              markeredgewidth=0,
                              linewidth=2)[0])
        labels.append('$p = '+str(degree)+'$')
    plt.legend(plots, labels, loc='lower right')
    plt.savefig(output_filename, bbox_inches='tight')

    assert True


if __name__ == "__main__":
    pytest.main('-v', [__file__])
