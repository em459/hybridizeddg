from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.timestepper import *
from swehdg.auxilliary.solution_looper import *
from swehdg.auxilliary.balanced_vortex import *

import pytest

import numpy as np

'''
These tests check that the method matches up with the analytic solution
for the stationary vortex, linear and non-linear, with and without
bathymetry.
This test checks with low resolutions and low-order and mid-order
polynomials, which means we cannot check with very small tolerances.
The runtime is significantly shorter than "hires" so can be included in
the test suite.
'''

# Set fixed parameters to be the "defaults"
fixed_param = default_param()
fixed_param['refinement'] = 5
mesh = gen_mesh(fixed_param)
fixed_param['mesh'] = mesh

## Initial conditions:
# Vortex setup
SW_param = ShallowWaterDGParam()
f_over_cg = 2.0*SW_param.omega/SW_param.cg
dphi = 0.1
 
vortex_list = []
fixed_param['flux'] = 'Lax-Friedrichs'
base_list = vary_param(fixed_param, 'degree', [0, 1, 2])
base_list = vary_param(base_list, 'hybridised', [True, False])

for bathymetry in [False, True]:
    for nonlinear in [False, True]:
        vortex = BalancedVortex(dphi, f_over_cg, non_linear=nonlinear, bathymetry=bathymetry)
        temp_list = vary_param(base_list, 'linear', (not nonlinear))
        temp_list = vary_param(temp_list, 'initial_condition_phi', vortex.phis_ufl(mesh))
        if bathymetry:
            temp_list = vary_param(temp_list, 'bathymetry', vortex.bath_ufl(mesh))
        vortex_list += vary_param(temp_list, 'initial_condition_u', vortex.u_ufl(mesh))

fixed_param['flux'] = 'upwind'
base_list2 = vary_param(fixed_param, 'degree', [0, 1, 2])
base_list2 = vary_param(base_list2, 'hybridised', [True, False])

for bathymetry in [False, True]:
    vortex = BalancedVortex(dphi, f_over_cg, non_linear=False, bathymetry=bathymetry)
    temp_list = vary_param(base_list2, 'initial_condition_phi', vortex.phis_ufl(mesh))
    if bathymetry:
        temp_list = vary_param(temp_list, 'bathymetry', vortex.bath_ufl(mesh))
    vortex_list += vary_param(temp_list, 'initial_condition_u', vortex.u_ufl(mesh))
    
@pytest.fixture(params=vortex_list)
def vortex_fixture(request):
    return request

def test_lowres_vortex(vortex_fixture):
    '''test_vortex tests whether a stationary vortex solution changes
    over time.
    '''
    phi_ic = vortex_fixture.param['initial_condition_phi']
    u_ic = vortex_fixture.param['initial_condition_u']
    
    q_final, t = simple_run(vortex_fixture)
    
    u_final, phi_final = q_final.split()[0:2]
    phi_error = np.sqrt(assemble((phi_final - phi_ic)*(phi_final - phi_ic)*dx))
    u_error = np.sqrt(assemble(inner(u_final - u_ic, u_final - u_ic)*dx))
    
    # If there's a problem it's useful to see how big the error is
    print()
    print('Error in phi field :', phi_error)
    print('Error in u field   :', u_error)
    
    # Tolerance (low resolution, particularly bad for degree 0)
    if vortex_fixture.param['degree'] == 0:
        tolerance = 0.03
    else:
        tolerance = 0.01
    
    assert np.allclose(phi_error, 0.0, atol=tolerance)
    assert np.allclose(u_error, 0.0, atol=tolerance)

if __name__ == '__main__':
    pytest.main(['-v', __file__])
