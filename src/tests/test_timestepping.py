from firedrake import *
from swehdg.core.shallow_water import *
from swehdg.core.timestepper import *
from swehdg.auxilliary.solution_looper import *
from swehdg.auxilliary.balanced_vortex import *

import pytest

import numpy as np

'''
These tests solve the stationary vortex analytic solution, linear and
non-linear, with and without bathymetry.
The refinement and degree are fixed and only the timestepper is changed
in each case.
Both semi-implicit and explicit timesteppers are checked in this file as
two seperate cases, due to timestep restrictions.
'''

# Set fixed parameters to be the "defaults"
fixed_param = default_param()
fixed_param['refinement'] = 4
fixed_param['degree'] = 2
mesh = gen_mesh(fixed_param)
fixed_param['mesh'] = mesh

## Initial conditions:
SW_param = ShallowWaterDGParam()
f_over_cg = 2.0*SW_param.omega/SW_param.cg
dphi = 0.1
 
ts_list = []
base_list = []

for bathymetry in [False, True]:
    for nonlinear in [False, True]:
        vortex = BalancedVortex(dphi, f_over_cg, non_linear=nonlinear, bathymetry=bathymetry)
        temp_list = vary_param(fixed_param, 'linear', (not nonlinear))
        temp_list = vary_param(temp_list, 'initial_condition_phi', vortex.phis_ufl(mesh))
        if bathymetry:
            temp_list = vary_param(temp_list, 'bathymetry', vortex.bath_ufl(mesh))
        base_list += vary_param(temp_list, 'initial_condition_u', vortex.u_ufl(mesh))

tmp = vary_param(base_list, 'timestepper', [TimeStepperTheta,
                                            TimeStepperIMEXTheta])
ts_list = vary_param(tmp, 'timestep_parameter', 0.5)
ts_list += vary_param(base_list, 'timestepper',[TimeStepperIMEXThreeStage,
                                                TimeStepperIMEXARS2_2_3_2,
                                                TimeStepperIMEXARS3_4_4_3])
@pytest.fixture(params=ts_list)
def timestepper_fixture(request):
    return request

def test_timesteppers(timestepper_fixture):
    # We expect the ARS(3,4,4,3) tests to fail since that timestepper
    # has not been implemented correctly yet
    if (timestepper_fixture.param["timestepper"] is TimeStepperIMEXARS3_4_4_3):
        pytest.xfail("ARS(3,4,4,3) timestepper not correctly implemented yet")
    phi_ic = timestepper_fixture.param['initial_condition_phi']
    u_ic = timestepper_fixture.param['initial_condition_u']
    
    q_final, t = simple_run(timestepper_fixture)
    
    u_final, phi_final = q_final.split()[0:2]
    phi_error = np.sqrt(assemble((phi_final - phi_ic)*(phi_final - phi_ic)*dx))
    u_error = np.sqrt(assemble(inner(u_final - u_ic, u_final - u_ic)*dx))
    
    # If there's a problem it's useful to see how big the error is
    print()
    print('Error in phi field :', phi_error)
    print('Error in u field   :', u_error)
    
    # Tolerance (low resolution, particularly bad for degree 0)
    if timestepper_fixture.param['degree'] == 0:
        tolerance = 0.03
    else:
        tolerance = 0.01
    
    assert np.allclose(phi_error, 0.0, atol=tolerance)
    assert np.allclose(u_error, 0.0, atol=tolerance)

explicit_list = vary_param(base_list, 'alpha', 1.0)
explicit_list = vary_param(explicit_list, 'rho', 0.1)
explicit_list = vary_param(explicit_list, 'timestepper',[TimeStepperTheta,
                                                         TimeStepperIMEXTheta])
explicit_list = vary_param(explicit_list, 'timestep_parameter', 0.0)
@pytest.fixture(params=explicit_list)
def explicit_timestepper_fixture(request):
    return request

def test_explicit_timestepper(explicit_timestepper_fixture):
    
    phi_ic = explicit_timestepper_fixture.param['initial_condition_phi']
    u_ic = explicit_timestepper_fixture.param['initial_condition_u']
    
    q_final, t = simple_run(explicit_timestepper_fixture)
    
    u_final, phi_final = q_final.split()[0:2]
    phi_error = np.sqrt(assemble((phi_final - phi_ic)*(phi_final - phi_ic)*dx))
    u_error = np.sqrt(assemble(inner(u_final - u_ic, u_final - u_ic)*dx))
    
    # If there's a problem it's useful to see how big the error is
    print()
    print('Error in phi field :', phi_error)
    print('Error in u field   :', u_error)
    
    # Tolerance (low resolution, particularly bad for degree 0)
    if explicit_timestepper_fixture.param['degree'] == 0:
        tolerance = 0.03
    elif explicit_timestepper_fixture.param['linear'] is False:
        tolerance = 0.05
    else:
        tolerance = 0.01
    
    assert np.allclose(phi_error, 0.0, atol=tolerance)
    assert np.allclose(u_error, 0.0, atol=tolerance)

if __name__ == '__main__':
    pytest.main(['-v', __file__])
