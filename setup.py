import os
from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

# Collect all scripts from driver direcotry
scripts = []
expath = os.path.join('src', 'driver')
for pyfile in os.listdir(expath):
    if pyfile[0]!= '_' and pyfile[-3:]=='.py':
        scripts.append(os.path.join(expath, pyfile))

setup(  name='swehdg',
        version=1.0,
        author='Eike Müller, Jack Betteridge',
        author_email='jdb55@bath.ac.uk',
        description='Hybridised Discontinuous Galerkin solver for the Shallow Water Equations',
        long_description=long_description,
        long_description_content_type='text/markdown',
        url='https://bitbucket.org/em459/hybridizeddg',
        package_dir={'': 'src'},
        package=find_packages('src', exclude=['tests', 'tests_*']),
        scripts=scripts,
        classifiers=[   'Programming Language :: Python :: 3',
                        'License :: OSI Approved :: BSD License',
                        'Operating System :: POSIX :: Linux'
                    ]
        )
